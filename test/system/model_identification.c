
#include <string.h>
#include <unity.h>

#include "state.h"
#include "platform_state.h"
#include "platform_base.h"
#include "labirynth.h"
#include "pid.h"
#include "executor.h"
#include "logger.h"

void setUp(void)
{
  labResetToDefault();
  stateInitModule(configGet());

  state_t now = {0};
  stateInitialize(&now);
  now.x = 1;
  now.y = 1; // make sure that x & y will be positive
  statePlatformSet(now);
}

void tearDown(void)
{
  motorSetSpeed((motors_speed_t){0, 0});
  platformDelayMs(1000);
}

void GatherData(double step_time_s, double total_time_s, double speed_left, double speed_right)
{
  state_t st_now, st_next;
  motors_ticks_t ticks;

  platformDelayMs(5000); //prepare for test

  log_set_verbosity_level(LOGGER_LVL_INFO); // omit all debug output

  bool decelerate = false;
  log_info("Test params: step %f, total time %f, motor speed %f R %f", step_time_s, total_time_s, speed_left, speed_right);
  log_info("time_seconds, velocity_linear, velocity_angular");

  motorSetSpeed((motors_speed_t){speed_left, speed_right});

  for (double time = 0; time < total_time_s; time += step_time_s)
  {
    log_info("%2.4f, %2.4f, %2.4f", time, st_now.v, st_now.w);
    if (time >= total_time_s / 2 && decelerate == false)
    {
      decelerate = true;
      motorSetSpeed((motors_speed_t){0, 0});
      log_info("---------- decelerate");
    }
    platformDelayMs(step_time_s * 1000);
    motorGetTicks(&ticks);
    stateNextEncoders(step_time_s, st_now, &st_next, ticks);
    st_now = st_next;
  }
}

void Given_OnlyLinear_When_StepResponse_Then_GatherData(void)
{
  /**
   * COMMENT this line to run this test.
   * It is disabled by default, so that CI server does not have to waste time for it
   */
  TEST_IGNORE();
  /** */
  const double STEP_TIME_S = 0.01;
  const double TOTAL_TIME_S = 2;
  double speed_left;
  double speed_right;

  for(double speed = 0.1; speed < 0.8; speed += 0.1)
  {
    speed_left = speed;
    speed_right = speed;
    GatherData(STEP_TIME_S, TOTAL_TIME_S, speed_left, speed_right);

    // give time to move bot to begining
    motorSetSpeed((motors_speed_t){0, 0});
    platformDelayMs(2000);
  }
}

void Given_OnlyAngular_When_StepResponse_Then_GatherData(void)
{
  /**
   * COMMENT this line to run this test.
   * It is disabled by default, so that CI server does not have to waste time for it
   */
  TEST_IGNORE();
  /** */
  const double STEP_TIME_S = 0.01;
  const double TOTAL_TIME_S = 2;
  double speed_left;
  double speed_right;

  for(double speed = 0.1; speed < 0.8; speed += 0.1)
  {
    speed_left = speed;
    speed_right = -speed;
    GatherData(STEP_TIME_S, TOTAL_TIME_S, speed_left, speed_right);

    // give time to move bot to begining
    motorSetSpeed((motors_speed_t){0, 0});
    platformDelayMs(2000);
  }
}


int main(void)
{
  platformInit();

  RUN_TEST(Given_OnlyLinear_When_StepResponse_Then_GatherData);
  RUN_TEST(Given_OnlyAngular_When_StepResponse_Then_GatherData);

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}
