
#include <string.h>

#include "unity.h"
#include "unity_fixture.h"

#include "executor.h"
#include "useful.h"
#include "state.h"
#include "platform_state.h"
#include "platform_base.h"
#include "labirynth.h"
#include "labirynth_alter.h"
#include "labirynths.h"
#include "distance_sensors_hw.h"
#include "platform_labirynth.h"

const double OFFSET_ALLOWED_IN_STATE_POSITION_MM = 10;

point_t target;
state_t now, sim_state;
motors_speed_t mot;
motors_ticks_t ticks;
const double TIME_STEP_S = 0.01;
ret_code_t ret;

void setUpCommon(void)
{
  platformInit();
  labTotalReset();
  stateInitModule(configGet());
}

void tearDownCommon()
{
  motorSetSpeed((motors_speed_t){0, 0});
  platformDelayMs(1000);
}

TEST_GROUP(stateBasedOnMotorEncoder);
TEST_SETUP(stateBasedOnMotorEncoder) { setUpCommon(); }
TEST_TEAR_DOWN(stateBasedOnMotorEncoder) { tearDownCommon(); }

TEST(stateBasedOnMotorEncoder, When_DriveForwardHalfMeter_Then_AchieveTarget)
{
  const double MAX_TIME_SECONDS = 3;
  double time = 0;

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&now));
  now.x = ONE_METER;
  now.y = LAB_DIM_WALL + LAB_DIM_CORRIDOR_HALF;
  now.t = 0;
  target.x = now.x + METERS_TO_MILLIMETERS(0.5);
  target.y = now.y;
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  memset(&ticks, 0, sizeof(ticks));

  do
  {
    ret = executorGoToTargetAndStop(target, now, TIME_STEP_S, &mot);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(mot));
    platformDelayMs(TIME_STEP_S*1000);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(TIME_STEP_S, now, &now, ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformNotify(now));
    stateDump("now", now);
    time += TIME_STEP_S;
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, now.y);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, now.y);
  }

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, now.y);
  }
}

TEST(stateBasedOnMotorEncoder, When_DriveBackwardHalfMeter_Then_RotateAndGo)
{
  const double MAX_TIME_SECONDS = 5;
  double time = 0;

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&now));
  target.x = now.x;
  target.y = now.y;
  now.y = target.y + METERS_TO_MILLIMETERS(0.5);
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  memset(&ticks, 0, sizeof(ticks));

  do
  {
    ret = executorGoToTargetAndStop(target, now, TIME_STEP_S, &mot);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(mot));
    platformDelayMs(TIME_STEP_S*1000);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(TIME_STEP_S, now, &now, ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformNotify(now));
    stateDump("now", now);
    time += TIME_STEP_S;
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, now.y);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, now.y);
  }
}

TEST(stateBasedOnMotorEncoder, When_DriveLeftOneCell_Then_RotateLeftAndGo)
{
  const double MAX_TIME_SECONDS = 3;
  double time = 0;

  target = labCellToPoint((lab_cellxy_t){0, 1});

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&now));
  now.t = 0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  memset(&ticks, 0, sizeof(ticks));

  do
  {
    ret = executorGoToTargetAndStop(target, now, TIME_STEP_S, &mot);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(mot));
    platformDelayMs(TIME_STEP_S*1000);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(TIME_STEP_S, now, &now, ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformNotify(now));
    time += TIME_STEP_S;
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, now.y);
  }
}

TEST_GROUP_RUNNER(stateBasedOnMotorEncoder)
{
  RUN_TEST_CASE(stateBasedOnMotorEncoder, When_DriveForwardHalfMeter_Then_AchieveTarget);
  RUN_TEST_CASE(stateBasedOnMotorEncoder, When_DriveBackwardHalfMeter_Then_RotateAndGo);
  RUN_TEST_CASE(stateBasedOnMotorEncoder, When_DriveLeftOneCell_Then_RotateLeftAndGo);

}

TEST_GROUP(stateBasedOnEncodersAndDistanceSensors);
TEST_SETUP(stateBasedOnEncodersAndDistanceSensors) { setUpCommon(); }
TEST_TEAR_DOWN(stateBasedOnEncodersAndDistanceSensors) { tearDownCommon(); }

TEST(stateBasedOnEncodersAndDistanceSensors, Given_NoWallsAtAll_When_DriveForward5cells_Then_DoIt)
{
  const double MAX_TIME_SECONDS = 3;
  double time = 0;

  target = labCellToPoint((lab_cellxy_t){0, 4});
  double ds[3];
  state_t next_enc, next_ds;

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsInit(dsGetCfgPlatformDefault()));
  memset(&ticks, 0, sizeof(ticks));

  do
  {
    ret = executorGoToTargetAndStop(target, now, TIME_STEP_S, &mot);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(mot));
    platformDelayMs(TIME_STEP_S*1000);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(TIME_STEP_S, now, &next_enc, ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, dsGetDistances(ds, 3));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(next_enc, &next_ds, ds));
    now.x = (next_ds.x + next_enc.x)/2;
    now.y = (next_ds.y + next_enc.y)/2;
    now.t = (next_ds.t + next_enc.t)/2;
    now.v = next_enc.v;
    now.w = next_enc.w;
    stateDump("enc", next_enc);
    stateDump("ds ", next_ds);
    stateDump("now", now);

    TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformNotify(now));
    time += TIME_STEP_S;
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, now.y);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, now.y);
  }
}

TEST(stateBasedOnEncodersAndDistanceSensors, Given_WallsKnownOnLeftAndRight_When_DriveForward5cellsN_Then_DoIt)
{
  const double MAX_TIME_SECONDS = 4;
  double time = 0;

  labResetToDefault();
  labLoad(labirynths[LABIRYNTHS_TEST_STRAIGHT_NS]);
  labirynthSetup(LABIRYNTHS_TEST_STRAIGHT_NS);

  target = labCellToPoint((lab_cellxy_t){0, 4});
  double ds[3];
  state_t next_enc, next_ds;

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsInit(dsGetCfgPlatformDefault()));
  memset(&ticks, 0, sizeof(ticks));

  do
  {
    // usleep(100000);
    ret = executorGoToTargetAndStop(target, now, TIME_STEP_S, &mot);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(mot));
    platformDelayMs(TIME_STEP_S*1000);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(TIME_STEP_S, now, &next_enc, ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, dsGetDistances(ds, 3));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(next_enc, &next_ds, ds));
    now.x = (next_ds.x + next_enc.x)/2;
    now.y = (next_ds.y + next_enc.y)/2;
    now.t = (next_ds.t + next_enc.t)/2;
    now.v = next_enc.v;
    now.w = next_enc.w;
    stateDump("enc", next_enc);
    stateDump("ds ", next_ds);
    stateDump("now", now);

    TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformNotify(now));
    time += TIME_STEP_S;
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, now.y);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, now.y);
  }
}

TEST(stateBasedOnEncodersAndDistanceSensors, Given_WallsKnownOnLeftAndRight_When_DriveForwardE_Then_DoIt)
{
  const double MAX_TIME_SECONDS = 10;
  double time = 0;

  labResetToDefault();
  labLoad(labirynths[LABIRYNTHS_TEST_STRAIGHT_EW]);
  labirynthSetup(LABIRYNTHS_TEST_STRAIGHT_EW);

  target = labCellToPoint((lab_cellxy_t){LAB_N-1, 0});
  double ds[3];
  state_t next_enc, next_ds;

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&now));
  now.t = 0; // do not turn, go straight
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsInit(dsGetCfgPlatformDefault()));
  memset(&ticks, 0, sizeof(ticks));

  do
  {
    // usleep(100000);
    ret = executorGoToTargetAndStop(target, now, TIME_STEP_S, &mot);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(mot));
    platformDelayMs(TIME_STEP_S*1000);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(TIME_STEP_S, now, &next_enc, ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, dsGetDistances(ds, 3));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(next_enc, &next_ds, ds));
    now.x = (next_ds.x + next_enc.x)/2;
    now.y = (next_ds.y + next_enc.y)/2;
    now.t = (next_ds.t + next_enc.t)/2;
    now.v = next_enc.v;
    now.w = next_enc.w;
    stateDump("enc", next_enc);
    stateDump("ds ", next_ds);
    stateDump("now", now);

    TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformNotify(now));
    time += TIME_STEP_S;
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, now.y);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, now.y);
  }
}

TEST_GROUP_RUNNER(stateBasedOnEncodersAndDistanceSensors)
{
  RUN_TEST_CASE(stateBasedOnEncodersAndDistanceSensors, Given_NoWallsAtAll_When_DriveForward5cells_Then_DoIt);
  RUN_TEST_CASE(stateBasedOnEncodersAndDistanceSensors, Given_WallsKnownOnLeftAndRight_When_DriveForward5cellsN_Then_DoIt);
  RUN_TEST_CASE(stateBasedOnEncodersAndDistanceSensors, Given_WallsKnownOnLeftAndRight_When_DriveForwardE_Then_DoIt);
}

int main(void)
{
  UNITY_BEGIN();

  RUN_TEST_GROUP(stateBasedOnMotorEncoder);
  RUN_TEST_GROUP(stateBasedOnEncodersAndDistanceSensors);

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}

