
#include <string.h>

#include "unity.h"
#include "unity_fixture.h"

#include "executor.h"
#include "motor.h"
#include "state.h"
#include "platform_state.h"
#include "platform_base.h"
#include "labirynth.h"

state_t st_now;
state_t st_next;
motors_ticks_t ticks;
double time;


void setUpCommon()
{
  //in the middle of the maze
  st_now.x = 1;
  st_now.y = 1;
  st_now.t = 0;
  st_now.v = 0;
  st_now.w = 0;
  statePlatformSet(st_now);
  labResetToDefault();
  stateInitModule(configGet());
}
void tearDownCommon()
{
  motorSetSpeed((motors_speed_t){0, 0});
  platformDelayMs(1000);
}

TEST_GROUP(stateBasedOnMotorEncoder);
TEST_SETUP(stateBasedOnMotorEncoder) { setUpCommon(); }
TEST_TEAR_DOWN(stateBasedOnMotorEncoder) { tearDownCommon(); }

TEST(stateBasedOnMotorEncoder, When_DrivingForwardFor3sec_Then_BothEncodersShowEqualDistance)
{
  time = 3;

  motorSetSpeed((motors_speed_t){0.4, 0.4});
  platformDelayMs(time*1000);
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time, st_now, &st_next, ticks));

  TEST_ASSERT_GREATER_THAN(0, ticks.left);
  TEST_ASSERT_INT_WITHIN(ticks.left/20, ticks.left, ticks.right);
}

TEST(stateBasedOnMotorEncoder, When_DrivingForwardAndTurningLeft_Then_RightEncoderShowsMore)
{
  time = 3;

  motorSetSpeed((motors_speed_t){0.4, 0.5});
  platformDelayMs(time*1000);
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time, st_now, &st_next, ticks));

  TEST_ASSERT_GREATER_THAN(0, ticks.left);
  TEST_ASSERT_GREATER_THAN(ticks.left, ticks.right);
}

TEST(stateBasedOnMotorEncoder, When_OnlyTurningRight_Then_BothEncodersShowEqualDistanceButDifferentSign)
{
  time = 2.0;

  motorSetSpeed((motors_speed_t){0.3, -0.3});
  platformDelayMs(time*1000);

  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time, st_now, &st_next, ticks));

  TEST_ASSERT_GREATER_THAN(0, ticks.left);
  TEST_ASSERT_INT_WITHIN(ticks.left/20, ticks.left, -ticks.right);
}

TEST_GROUP_RUNNER(stateBasedOnMotorEncoder)
{
  RUN_TEST_CASE(stateBasedOnMotorEncoder, When_DrivingForwardFor3sec_Then_BothEncodersShowEqualDistance);
  RUN_TEST_CASE(stateBasedOnMotorEncoder, When_DrivingForwardAndTurningLeft_Then_RightEncoderShowsMore);
  RUN_TEST_CASE(stateBasedOnMotorEncoder, When_OnlyTurningRight_Then_BothEncodersShowEqualDistanceButDifferentSign);
}

int main(void)
{
  UNITY_BEGIN();
  platformInit();

  RUN_TEST_GROUP(stateBasedOnMotorEncoder);

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}

