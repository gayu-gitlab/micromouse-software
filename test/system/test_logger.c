
#include <string.h>

#include "unity.h"
#include "unity_fixture.h"

#include "platform_state.h"
#include "platform_base.h"
#include "useful.h"

void setUpCommon(void)
{
  log_raw("\n");
}

void tearDownCommon()
{
}

TEST_GROUP(loggingTimings);
TEST_SETUP(loggingTimings) { setUpCommon(); }
TEST_TEAR_DOWN(loggingTimings) { tearDownCommon(); }


TEST(loggingTimings, When_Waiting1s_Then_TimerPlatformReturns1s)
{
  platformStartTimingMeasurement();
  platformDelayMs(1000);

  uint32_t Duration_ms = platformStopTimingMeasurementUs()/1000;
  TEST_ASSERT_INT_WITHIN(0, 1000, Duration_ms);
}

/**
 * @brief UART is set to 115200 baud rate.
 * One char takes 8 bits + 1 start bit + 1 stopbit = 10bits.
 * So there should be up to 11 520 chars per second, 115 chars per 0.01 second
 *
 * The aim: make sure that logging does not halt CPU (it is done by HW)
 */
TEST(loggingTimings, When_Logging100charsWith100Hz_Then_NothingGetLostAndNoCpuOverhead)
{
  const double TEST_TIME_S = 10;
  const double LOGGING_FREQUENCY = 100;

  platformStartTimingMeasurement();
  for(int i = 0; i<TEST_TIME_S * LOGGING_FREQUENCY; i++)
  {
    log_raw("this is very long log that has 100 chars in it... "
            "%048d\r\n", i);
    platformDelayMs(SECONDS_TO_MILLISECONDS(1/LOGGING_FREQUENCY));
  }
  uint32_t duration_ms = platformStopTimingMeasurementUs()/1000;
  uint32_t difference_ms = duration_ms-(uint32_t)SECONDS_TO_MILLISECONDS(TEST_TIME_S);

  log_debug("TEST took              %u ms", duration_ms);
  log_debug("TEST should have taken %u ms", (uint32_t)SECONDS_TO_MILLISECONDS(TEST_TIME_S));
  log_debug("TEST difference        %u ms", difference_ms);
  log_debug("TEST meaning           %f ms per 100 chars log", difference_ms/(LOGGING_FREQUENCY*TEST_TIME_S));
  platformLoggerFlush();

  // allow 15% overhead
  TEST_ASSERT_INT_WITHIN(TEST_TIME_S*15, 0, difference_ms);
}

TEST_GROUP_RUNNER(loggingTimings)
{
  RUN_TEST_CASE(loggingTimings, When_Waiting1s_Then_TimerPlatformReturns1s);
  RUN_TEST_CASE(loggingTimings, When_Logging100charsWith100Hz_Then_NothingGetLostAndNoCpuOverhead);
}


int main(void)
{
  UNITY_BEGIN();
  platformInit();

  RUN_TEST_GROUP(loggingTimings);

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}

