
#include <string.h>
#include <math.h>

#include "unity.h"
#include "unity_fixture.h"

#include "state.h"
#include "platform_state.h"
#include "platform_base.h"
#include "labirynth.h"
#include "pid.h"
#include "executor.h"
#include "logger.h"
#include "useful.h"


static const double LINEAR_VELOCITY_OFFSET_ALLOWED = 10;
static const double ANGULAR_VELOCITY_OFFSET_ALLOWED = (10*M_PI)/180;
static const double LINEAR_VELOCITY_TYPICAL = 600;
static const double ANGULAR_VELOCITY_TYPICAL = M_PI;

state_t st_now;

void setUpCommon(void)
{
  labResetToDefault();
  stateInitModule(configGet());

  stateInitialize(&st_now);
  statePlatformSet(st_now);
}

void tearDownCommon()
{
  motorSetSpeed((motors_speed_t){0, 0});
  platformDelayMs(1000);
}

static void achieve_velocity( pid_controller_t pidController,
  bool linear, double target_speed, const double DRIVE_TIME, const double STEP_TIME);

TEST_GROUP(pidVelocityUsingEncoders);
TEST_SETUP(pidVelocityUsingEncoders) { setUpCommon(); }
TEST_TEAR_DOWN(pidVelocityUsingEncoders) { tearDownCommon(); }

void test_pid(bool linear, double target_speed)
{
  const double STEP_TIME = 0.01;
  const double DRIVE_TIME = 1.0;
  uint8_t pidMem[PID_CONTROLLER_STRUCT_SIZE_BYTES];
  pid_controller_t pidController = (pid_controller_t)pidMem;

  log_set_verbosity_level(LOGGER_LVL_INFO); // omit all debug output

  if(linear)
  {
    pidInit(pidController, configGet()->linearPIDp, configGet()->linearPIDi, configGet()->linearPIDd);
  }
  else
  {
    pidInit(pidController, configGet()->angularPIDp, configGet()->angularPIDi, configGet()->angularPIDd);
  }

  log_info("Test params: step %f, acceleration_time %f, target_velocity %f, linear %d",
    STEP_TIME, DRIVE_TIME, target_speed, (int)linear);


  log_info("\n\nACCELERATE");
  achieve_velocity(pidController, linear, target_speed, DRIVE_TIME, STEP_TIME);
  if(linear) { TEST_ASSERT_DOUBLE_WITHIN(LINEAR_VELOCITY_OFFSET_ALLOWED, target_speed, st_now.v); }
  else { TEST_ASSERT_DOUBLE_WITHIN(ANGULAR_VELOCITY_OFFSET_ALLOWED, target_speed, st_now.w); }

  state_t st;
  ret_code_t ret = statePlatformGet(&st);
  if(ret == RET_SUCCESS)
  {
    //only on simulator
    if(linear) { TEST_ASSERT_DOUBLE_WITHIN(LINEAR_VELOCITY_OFFSET_ALLOWED, st.v, st_now.v); }
    else { TEST_ASSERT_DOUBLE_WITHIN(ANGULAR_VELOCITY_OFFSET_ALLOWED, st.w, st_now.w); }
  }

  log_info("\n\nDECELERATE");
  target_speed = 0;
  achieve_velocity(pidController, linear, target_speed, DRIVE_TIME, STEP_TIME);
  if(linear) { TEST_ASSERT_DOUBLE_WITHIN(LINEAR_VELOCITY_OFFSET_ALLOWED, target_speed, st_now.v); }
  else { TEST_ASSERT_DOUBLE_WITHIN(ANGULAR_VELOCITY_OFFSET_ALLOWED, target_speed, st_now.w); }

  ret = statePlatformGet(&st);
  if(ret == RET_SUCCESS)
  {
    //only on simulator
    if(linear) { TEST_ASSERT_DOUBLE_WITHIN(LINEAR_VELOCITY_OFFSET_ALLOWED, st.v, st_now.v); }
    else { TEST_ASSERT_DOUBLE_WITHIN(ANGULAR_VELOCITY_OFFSET_ALLOWED, st.w, st_now.w); }
  }

}

void achieve_velocity( pid_controller_t pidController,
  bool linear, double target_speed, const double DRIVE_TIME, const double STEP_TIME)
{
  double pid_output;
  state_t st_next;
  ret_code_t ret;
  motors_ticks_t ticks;
  memset(&ticks, 0, sizeof(ticks));

  platformStartTimingMeasurement();
  for (double time = DRIVE_TIME; time > 0; time -= STEP_TIME)
  {
    double velocity;
    if(linear) { velocity = st_now.v; }
    else{ velocity = st_now.w; }

    double error = target_speed - velocity;

    ret = pidStep(pidController, STEP_TIME, error, &pid_output);
    TEST_ASSERT_EQUAL(ret, RET_SUCCESS);

    if(linear) { TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(executorSetRawVelocities(pid_output, 0))); }
    else{ TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(executorSetRawVelocities(0, pid_output))); }

    // TODO should be 1 here, but simulator does not support this ...
    // more acurate wait than just "delay"
    // while( platformStopTimingMeasurementUs() < STEP_TIME*1000000) {
    platformDelayMs(10);
    // };
    platformStartTimingMeasurement();

    TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(STEP_TIME, st_now, &st_next, ticks));
    st_now = st_next;
    // log_info("e%+1.2f u%+1.2f", error, pid_output);
    statePlatformNotify(st_now);
  }
}

TEST(pidVelocityUsingEncoders, When_DriveForwardWithConstantSpeed_Then_CheckThatVelocity)
{
  test_pid(true, LINEAR_VELOCITY_TYPICAL);
}

TEST(pidVelocityUsingEncoders, When_RotateWithConstantSpeed_Then_CheckThatVelocity)
{
  test_pid(false, ANGULAR_VELOCITY_TYPICAL);
}

TEST_GROUP_RUNNER(pidVelocityUsingEncoders)
{
  RUN_TEST_CASE(pidVelocityUsingEncoders, When_DriveForwardWithConstantSpeed_Then_CheckThatVelocity);
  RUN_TEST_CASE(pidVelocityUsingEncoders, When_RotateWithConstantSpeed_Then_CheckThatVelocity);
}

int main(void)
{
  UNITY_BEGIN();
  platformInit();

  RUN_TEST_GROUP(pidVelocityUsingEncoders);

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}
