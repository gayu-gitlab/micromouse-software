#include "unity_fixture.h"

#include "labirynth_utils.h"
#include "vector.h"
#include "platform_base.h"
#include "point.h"
#include "platform_printer.h"

/**
 * @brief Generate random path
 * Useful for debugging printing path
 */
void pp_gen_random_path(vector_t* path)
{
  if(!path) { return; }

  if(RET_SUCCESS != vector_clear(path)) { return; }
  point_t p;

  for(uint8_t i = 0; i < 10; i++)
  {
    p.x = (double)(LAB_DIM_MAX / (i+1));
    p.y = (double)(i * LAB_DIM_CELL);
    if(RET_SUCCESS != vector_add(path, &p)) { return; }
  }
}

void SetUp(void)
{

}
void TearDown(void)
{

}

void test_print_path(void)
{
  vector_t path;
  char mem[300];
  vector_init(&path, mem, sizeof(mem), sizeof(point_t));

  pp_gen_random_path(&path);
  log_debug("path count %d\n", vector_item_cnt(path));
  TEST_ASSERT_EQUAL(RET_SUCCESS, printPathPlannerPoints(path));
}


int main(void)
{
  UNITY_BEGIN();

  platformInit();
  RUN_TEST(test_print_path);

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}
