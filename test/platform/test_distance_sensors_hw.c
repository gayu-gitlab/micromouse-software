
#include "distance_sensors_hw.h"
#include "unity.h"
#include "ret_codes.h"
#include "platform_base.h"
#include "string.h"
#include "state.h"
#include "platform_state.h"
#include "labirynths.h"
#include "useful.h"


static const dsConfig_t defaultCfg =
{
  .numberOfSensors = 5,
  .sensors =
  {
    {.offset = {-40, 0, M_PI/2}},
    {.offset = {-20, 20, M_PI/4}},
    {.offset = {0.00, 40, 0}},
    {.offset = {20, 20, -M_PI/4}},
    {.offset = {40, 0, -M_PI/2}},
  },
};

void setUp(void)
{
  state_t state_now;
  stateInitialize(&state_now);
  statePlatformSet(state_now);
}
void tearDown(void)
{
}

void dsGetDistances_test(void)
{
  double dists[MAX_DISTANCE_SENSORS_NUM];
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsInit(dsGetCfgPlatformDefault()));
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsGetDistances(dists, sizeof(dists)/sizeof(dists[0])));
  for(unsigned int i=0; i<dsGetCfg()->numberOfSensors; i++)
  {
    log_debug("dist %3.1f", dists[i]);
    TEST_ASSERT_LESS_OR_EQUAL_DOUBLE(MAX_DISTANCE_SENSOR_RANGE_MM, dists[i]);
    TEST_ASSERT_GREATER_OR_EQUAL_DOUBLE(MIN_DISTANCE_SENSOR_RANGE_MM, dists[i]);
  }
}

int main(void)
{
  UNITY_BEGIN();
  platformInit();
  RUN_TEST(dsGetDistances_test);

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}
