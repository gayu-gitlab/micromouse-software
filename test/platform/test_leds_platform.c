
#include <string.h>

#include "platform_base.h"
#include "unity.h"
#include "logger.h"

void setUp(void)
{
}

void tearDown(void)
{
}


void turnOnLedsSequentially(void)
{
  platformSetLedState(0, 0);
  platformSetLedState(1, 0);
  platformSetLedState(2, 0);
  platformDelayMs(1000);

  platformSetLedState(0, 1); platformDelayMs(1000);
  platformSetLedState(1, 1); platformDelayMs(1000);
  platformSetLedState(2, 1); platformDelayMs(1000);
  platformSetLedState(0, 0);
  platformSetLedState(1, 0);
  platformSetLedState(2, 0);
}


int main(void)
{
  UNITY_BEGIN();
  platformInit();
  turnOnLedsSequentially();

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}

