
#include <stdbool.h>
#include <string.h>

#include "unity.h"
#include "unity_fixture.h"

#include "simulator_broker.h"
#include "path_planner.h"
#include "state.h"
#include "platform_base.h"
#include "labirynths.h"
#include "labirynth.h"
#include "config.h"
#include "distance_sensors.h"
#include "distance_sensors_hw.h"



void test_sim_alive(void)
{
  bool alive;
  ret_code_t ret;

  ret = sim_get_param(SIM_PARAM_ALIVE, &alive);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(true, alive);
}

void test_sim_get_set_config(void)
{
  ret_code_t ret;
  config_t cfgSim, cfgFw;

  memset(&cfgSim, 0xab, sizeof(cfgSim));
  cfgFw.whlTrack = 120;
  cfgFw.whlRadius = 110;
  cfgFw.whlTicksPerTurn = 32;
  // Do not set whlCirc, as it should be calculated based on the radius

  ret = sim_set_param(SIM_PARAM_CONFIG, &cfgFw);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  ret = sim_get_param(SIM_PARAM_CONFIG, &cfgSim);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

  TEST_ASSERT_EQUAL(cfgSim.whlTrack, cfgFw.whlTrack);
  TEST_ASSERT_EQUAL(cfgSim.whlRadius, cfgFw.whlRadius);
  TEST_ASSERT_EQUAL(cfgSim.whlTicksPerTurn, cfgFw.whlTicksPerTurn);
}

void test_sim_set_state(void)
{
  state_t st;
  st.x = 1;
  st.y = 2;
  st.t = 0;
  st.v = 0.1;
  st.w = 0.2;
  TEST_ASSERT_EQUAL(RET_SUCCESS, sim_set_param(SIM_PARAM_STATE, (void*) &st));
}

void test_sim_two_ask_resp(void)
{
  bool alive;
  ret_code_t ret;

  ret = sim_get_param(SIM_PARAM_ALIVE, &alive);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(true, alive);
  ret = sim_get_param(SIM_PARAM_ALIVE, &alive);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(true, alive);
}

void test_sim_load_map(void)
{
  ret_code_t ret;

  ret = sim_set_param(SIM_PARAM_MAP_NAME, (void*) labirynthNameEnumToString(LABIRYNTHS_A));
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
}

void test_sim_show_path(void)
{
  ret_code_t ret;
  vector_t path;
  point_t point;
  char mem[100];

  if(RET_SUCCESS != vector_init(&path, mem, sizeof(mem), sizeof(point_t)))
  { return ; }

  point.x = 0; point.y = 0;
  if(RET_SUCCESS != vector_add(&path, &point)) { return; }
  point.x = 1; point.y = 0.5;
  if(RET_SUCCESS != vector_add(&path, &point)) { return; }
  point.x = 2; point.y = 0.5;
  if(RET_SUCCESS != vector_add(&path, &point)) { return; }
  point.x = 1; point.y = 1.5;
  if(RET_SUCCESS != vector_add(&path, &point)) { return; }

  ret = sim_set_param(SIM_PARAM_PATH, (void*) &path);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
}

void test_sim_cell_text(void)
{
    ret_code_t ret;
    pp_cell_text_t ct;
    ct.xy.x = 3;
    ct.xy.y = 4;
    static const char* txt = "hello";
    ct.text = txt;
    ret = sim_set_param(SIM_PARAM_CELL_TEXT, (void*) &ct);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

    ret = sim_set_param(SIM_PARAM_CLEAR_CELL_TEXTS, NULL);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
}

void test_sim_notify_wall(void)
{
    ret_code_t ret;
    SIM_WALL_NOTIFY_t n;
    n.x = 0;
    n.y = 0;
    n.mask_walls = LAB_SOUTH | LAB_WEST;
    n.exists = false;

    ret = sim_set_param(SIM_PARAM_NOTIFY_WALL, (void *)&n);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

    n.x = LAB_N-1;
    n.y = 0;
    n.mask_walls = LAB_SOUTH | LAB_EAST;
    n.exists = false;

    ret = sim_set_param(SIM_PARAM_NOTIFY_WALL, (void *)&n);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

    n.x = LAB_N-1;
    n.y = LAB_N-1;
    n.mask_walls = LAB_NORTH | LAB_EAST;
    n.exists = false;

    ret = sim_set_param(SIM_PARAM_NOTIFY_WALL, (void *)&n);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

    n.x = 0;
    n.y = LAB_N-1;
    n.mask_walls = LAB_NORTH | LAB_WEST;
    n.exists = false;

    ret = sim_set_param(SIM_PARAM_NOTIFY_WALL, (void *)&n);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

    n.x = 1;
    n.y = 1;
    n.mask_walls = LAB_ALL_DIRS;
    n.exists = true;

    ret = sim_set_param(SIM_PARAM_NOTIFY_WALL, (void *)&n);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
}

void test_sim_show_current_lab_knowledge(void)
{
    ret_code_t ret;

    labLoad(labirynths[LABIRYNTHS_A]);
    ret = sim_show_current_lab_knowledge();
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
}

void test_sim_clear_saved_labirynth(void)
{
    ret_code_t ret;
    ret = sim_set_param(SIM_PARAM_NOTIFY_WALL_CLEAR, NULL);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

}


TEST_GROUP(config_distance_sensors);
TEST_SETUP(config_distance_sensors) {
  sim_init();
 }
TEST_TEAR_DOWN(config_distance_sensors) {
  sim_deinit();
 }

TEST(config_distance_sensors, When_SetZero_Then_SimReturnsAlsoZero)
{
  ret_code_t ret;
  dsConfig_t dscfgSim, dscfgFw;
  double distances[MAX_DISTANCE_SENSORS_NUM];
  double distancesInitial[MAX_DISTANCE_SENSORS_NUM];

  memset(&dscfgSim, 0xab, sizeof(dscfgSim));
  memset(&distancesInitial, 0xc1, sizeof(distancesInitial));
  memcpy(distances, distancesInitial, sizeof(distancesInitial));

  dscfgFw.numberOfSensors = 0;

  ret = sim_set_param(SIM_PARAM_CONFIG_DISTANCE_SENSORS, &dscfgFw);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  ret = sim_get_param(SIM_PARAM_CONFIG_DISTANCE_SENSORS, &dscfgSim);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  // Simulator should also return now correct number of measurements
  ret = sim_get_param(SIM_PARAM_DIST_SENSORS, (void*) distances);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);


  TEST_ASSERT_EQUAL(dscfgSim.numberOfSensors, 0);
  TEST_ASSERT_EQUAL_MEMORY(distances, distancesInitial, sizeof(distances));
}

TEST(config_distance_sensors, When_SetOne_Then_SimReturnsAlsoOne)
{
  ret_code_t ret;
  dsConfig_t dscfgSim, dscfgFw;
  double distances[MAX_DISTANCE_SENSORS_NUM];
  double distancesInitial[MAX_DISTANCE_SENSORS_NUM];

  memset(&dscfgSim, 0xab, sizeof(dscfgSim));
  memset(&distancesInitial, 0xc1, sizeof(distancesInitial));
  memcpy(distances, distancesInitial, sizeof(distancesInitial));

  dscfgFw.numberOfSensors = 1;
  dscfgFw.sensors[0].offset.x = -0.1;
  dscfgFw.sensors[0].offset.y = 0.003;
  dscfgFw.sensors[0].offset.t = 1;

  ret = sim_set_param(SIM_PARAM_CONFIG_DISTANCE_SENSORS, &dscfgFw);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  ret = sim_get_param(SIM_PARAM_CONFIG_DISTANCE_SENSORS, &dscfgSim);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  // Simulator should also return now correct number of measurements
  ret = sim_get_param(SIM_PARAM_DIST_SENSORS, (void*) distances);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);


  TEST_ASSERT_EQUAL(dscfgSim.numberOfSensors, 1);
  TEST_ASSERT_EQUAL_DOUBLE(dscfgSim.sensors[0].offset.x, dscfgFw.sensors[0].offset.x);
  TEST_ASSERT_EQUAL_DOUBLE(dscfgSim.sensors[0].offset.y, dscfgFw.sensors[0].offset.y);
  TEST_ASSERT_EQUAL_DOUBLE(dscfgSim.sensors[0].offset.t, dscfgFw.sensors[0].offset.t);
  TEST_ASSERT_EQUAL_MEMORY(&distances[1], &distancesInitial[1], sizeof(distances) - sizeof(distances[0]));
}


TEST(config_distance_sensors, When_SetDefault_Then_SimReturnsTheSame)
{
  ret_code_t ret;
  dsConfig_t dscfgSim, dscfgFw;
  double distances[MAX_DISTANCE_SENSORS_NUM];
  double distancesInitial[MAX_DISTANCE_SENSORS_NUM];

  memset(&dscfgSim, 0xab, sizeof(dscfgSim));
  memset(&distancesInitial, 0xc1, sizeof(distancesInitial));
  memcpy(distances, distancesInitial, sizeof(distancesInitial));

  dscfgFw = dsGetCfgPlatformDefault();

  ret = sim_set_param(SIM_PARAM_CONFIG_DISTANCE_SENSORS, &dscfgFw);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  ret = sim_get_param(SIM_PARAM_CONFIG_DISTANCE_SENSORS, &dscfgSim);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  ret = sim_get_param(SIM_PARAM_DIST_SENSORS, (void*) distances);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);


  TEST_ASSERT_EQUAL(dscfgSim.numberOfSensors, dscfgFw.numberOfSensors);
  for(uint8_t i = 0; i<dscfgFw.numberOfSensors; i++)
  {
    TEST_ASSERT_DOUBLE_WITHIN(0.0001, dscfgSim.sensors[i].offset.x, dscfgFw.sensors[i].offset.x);
    TEST_ASSERT_DOUBLE_WITHIN(0.0001, dscfgSim.sensors[i].offset.y, dscfgFw.sensors[i].offset.y);
    TEST_ASSERT_DOUBLE_WITHIN(0.0001, dscfgSim.sensors[i].offset.t, dscfgFw.sensors[i].offset.t);
  }
  // other readings remain unchanged
  for(uint8_t i = dscfgFw.numberOfSensors; i < MAX_DISTANCE_SENSORS_NUM; i++)
  {
    TEST_ASSERT_EQUAL_DOUBLE(distances[i], distancesInitial[i]);
  }
}

TEST_GROUP_RUNNER(config_distance_sensors)
{
  RUN_TEST_CASE(config_distance_sensors, When_SetZero_Then_SimReturnsAlsoZero);
  RUN_TEST_CASE(config_distance_sensors, When_SetOne_Then_SimReturnsAlsoOne);
  RUN_TEST_CASE(config_distance_sensors, When_SetDefault_Then_SimReturnsTheSame);
}

int main(void)
{
  UNITY_BEGIN();

  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
  sim_init();
  RUN_TEST(test_sim_alive);
  sim_deinit();
  sim_init();
  RUN_TEST(test_sim_get_set_config);
  sim_deinit();
  sim_init();
  RUN_TEST(test_sim_set_state);
  sim_deinit();
  sim_init();
  RUN_TEST(test_sim_two_ask_resp);
  sim_deinit();
  sim_init();
  RUN_TEST(test_sim_load_map);
  sim_deinit();
  sim_init();
  RUN_TEST(test_sim_show_path);
  sim_deinit();
  sim_init();
  RUN_TEST(test_sim_cell_text);
  sim_deinit();
  sim_init();
  RUN_TEST(test_sim_notify_wall);
  sim_deinit();
  sim_init();
  RUN_TEST(test_sim_show_current_lab_knowledge);
  sim_deinit();
  sim_init();
  RUN_TEST(test_sim_clear_saved_labirynth);
  sim_deinit();

  RUN_TEST_GROUP(config_distance_sensors);

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}






