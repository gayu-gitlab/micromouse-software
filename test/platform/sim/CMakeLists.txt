##############################################################################
set(SIMULATOR_BROKER_SRCS
  ${PLATFORM_DIR}/platform_base.c
  ${PLATFORM_DIR}/motor.c
  ${PLATFORM_DIR}/distance_sensors_hw.c
  ${PLATFORM_DIR}/platform_printer.c
  ${PLATFORM_DIR}/platform_state.c
)
set(SIMULATOR_BROKER_MOCKS
)
add_custom_hw_test(test_simulator_broker "${SIMULATOR_BROKER_SRCS}" "${SIMULATOR_BROKER_MOCKS}")
