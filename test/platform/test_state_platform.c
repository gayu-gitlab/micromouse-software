
#include <string.h>

#include "unity.h"
#include "useful.h"
#include "state.h"
#include "platform_base.h"
#include "platform_state.h"

state_t st_global;

void setUp(void)
{
  stateInitModule(configGet());

  st_global.x = METERS_TO_MILLIMETERS(0.5);
  st_global.y = METERS_TO_MILLIMETERS(0.2);
  st_global.t = -M_PI/4;
  st_global.v = METERS_TO_MILLIMETERS(0.2);
  st_global.w = 0.1;
}

void tearDown(void)
{
}

#define ALLOWED_DELTA_DEFAULT_POSITION 1
#define ALLOWED_DELTA_DEFAULT_ANGLE deg2rad(5)

#define TEST_ASSERT_STATE_WITHIN(expected, actual)  {  \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_POSITION, expected.x, actual.x, "x");     \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_POSITION, expected.y, actual.y, "y");     \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_ANGLE, expected.t, actual.t, "t");     \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_POSITION, expected.v, actual.v, "v");     \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_ANGLE, expected.w, actual.w, "w");     \
}
void statePlatformSet_test(void)
{
  //should be able to set the state of the mouse
  //for example by picking it by hand
  //or by setting proper state in simulator
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(st_global));
}

void statePlatformGet_test(void)
{
  state_t st;
  //should be also able to get the state
  //this should work only in simulator (because in real life we do not have
  //it... all work is to get it...)
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformGet(&st));
  TEST_ASSERT_STATE_WITHIN(st_global, st);
}

void statePlatformNotify_test(void)
{
  state_t st;
  st.x = 0.2;
  st.y = 0.5;
  st.t = 3*M_PI/4;
  st.v = 0.4;
  st.w = 0.5;
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformNotify(st));
}

void stateTrackingStraightTest(void)
{
  state_t state_now, state_enco, state_sim;
  double time_s = 1.00;
  motors_speed_t values;
  motors_ticks_t ticks;

  //initialize
  stateInitialize(&state_now);
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(state_now));
  memset(&ticks, 0, sizeof(ticks));

  // drive straight
  values.left = 1.0; values.right = 1.0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(values));
  platformDelayMs(SECONDS_TO_MILLISECONDS(time_s));
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));

  // calculate and get position from simulator
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time_s, state_now, &state_enco, ticks));
  // run again for a shorter while in order to get proper velocity based on encoders
  platformDelayMs(SECONDS_TO_MILLISECONDS(time_s/10));
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time_s/10, state_enco, &state_enco, ticks));


  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformGet(&state_sim));
  statePlatformNotify(state_enco);

  stateDump("encoders", state_enco);
  stateDump("state_sim", state_sim);

  TEST_ASSERT_STATE_WITHIN(state_enco, state_sim);
}

void stateTrackingCurveTest(void)
{
  state_t state_now, state_model, state_enco, state_sim;
  double time_s = 0.05;
  motors_speed_t values;
  motors_ticks_t ticks;

  //initialize
  stateInitialize(&state_now);
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(state_now));
  memset(&ticks, 0, sizeof(ticks));

  // drive
  values.left = 1.0; values.right = 0.5;
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(values));
  platformDelayMs(time_s*1000);
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));

  // calculate and get position from simulator
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextModel(time_s, state_now, &state_model));
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time_s, state_now, &state_enco, ticks));
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformGet(&state_sim));
  statePlatformNotify(state_enco);

  for(int i = 0; i<10; i++)
  {
    platformDelayMs(time_s*1000);
    TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextModel(time_s, state_now, &state_model));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time_s, state_now, &state_enco, ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformGet(&state_sim));
    state_now = state_enco;
    statePlatformNotify(state_enco);
  }


  //do not compare state_model because it depends on current state.
  //current state is "staying at 0 speed", so in estimated position by
  //model nothing changes.
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.01, state_sim.x, state_enco.x, "x");
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.01, state_sim.y, state_enco.y, "y");
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.1, state_sim.t, state_enco.t, "t");
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.2, state_sim.v, state_enco.v, "v");
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.8, state_sim.w, state_enco.w, "w");
}


int main(void)
{
  UNITY_BEGIN();

  platformInit();

  // TODO
  // RUN_TEST(statePlatformSet_test);
  // RUN_TEST(statePlatformGet_test);
  // RUN_TEST(statePlatformNotify_test);

  RUN_TEST(stateTrackingStraightTest);
  // RUN_TEST(stateTrackingCurveTest);


  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}
