
#include "unity_fixture.h"

#include "distance_sensors.h"
#include "unity.h"
#include "ret_codes.h"
#include "platform_base.h"
#include "string.h"
#include "state.h"
#include "platform_state.h"
#include "labirynths.h"
#include "useful.h"

// in millimeters
#define DELTA 0.1
const double FURTHER_FACTOR = 1.1;
const double CLOSER_FACTOR = 0.9;
const double ONE_CM = 10;
const double TWO_CM = 20;

static const dsConfig_t defaultCfg =
{
  .numberOfSensors = 5,
  .sensors =
  {
    {.offset = {-40, 0, M_PI/2}},
    {.offset = {-20, 20, M_PI/4}},
    {.offset = {0, 40, 0}},
    {.offset = {20, 20, -M_PI/4}},
    {.offset = {40, 0, -M_PI/2}},
  },
};

#define TEST_ASSERT_EQUAL_WALL(expected, real) \
{\
TEST_ASSERT_EQUAL_INT_MESSAGE(expected.cell_xy.x, real.cell_xy.x, "x"); \
TEST_ASSERT_EQUAL_INT_MESSAGE(expected.cell_xy.y, real.cell_xy.y, "y"); \
TEST_ASSERT_EQUAL_INT_MESSAGE(expected.direction, real.direction, "direction"); \
}


TEST_GROUP(dsEstimatePositionOfOneSensor_FUN);
TEST_SETUP(dsEstimatePositionOfOneSensor_FUN) { }
TEST_TEAR_DOWN(dsEstimatePositionOfOneSensor_FUN) { }


TEST(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsEast_When_MeasurementSmaller_Then_MoveToEast)
{
  double wall_x = 1.0;
  pos_t sensor_pos, sensor_pos_from_measurement;
  ret_code_t ret;
  double measured_distance;

  sensor_pos.x = 0.0;
  sensor_pos.y = 0.0;

  // Three cases, always should update position of the sensor to be closer to the wall
  // North - East
  sensor_pos.t = M_PI/4; //45'
  sensor_pos_from_measurement = sensor_pos;
  // this distance can be calculated nicely, but some cases were done on the paper
  // and I want to stick with them
  measured_distance = sqrt(2)/2;
  ret = dsEstimatePositionOfOneSensor(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_x);
  posDump("sensor pred", sensor_pos);
  posDump("sensor meas", sensor_pos_from_measurement);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be closer to the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.x, sensor_pos_from_measurement.x);
  // the angle of the sensor should be rotated, so that the ray is shorter
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);

  // Only East
  sensor_pos.t = 0; //0'
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_x - sensor_pos.x)/cos(sensor_pos.t) * CLOSER_FACTOR);
  ret = dsEstimatePositionOfOneSensor(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_x);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be closer to the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.x, sensor_pos_from_measurement.x);
  // the angle of the sensor is not checked

  // South - East
  sensor_pos.t = -M_PI/4; //-45'
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_x - sensor_pos.x)/cos(sensor_pos.t) * CLOSER_FACTOR);
  ret = dsEstimatePositionOfOneSensor(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_x);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be closer to the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.x, sensor_pos_from_measurement.x);
  // the angle of the sensor should be rotated, so that the ray is shorter
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);
}

TEST(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsEast_When_MeasurementGreater_Then_MoveToWest)
{
  double wall_x = 1.0;
  pos_t sensor_pos, sensor_pos_from_measurement;
  ret_code_t ret;
  double measured_distance;

  sensor_pos.x = 0.0;
  sensor_pos.y = 0.0;

  // Three cases, always should update position of the sensor to be further from the wall
  // North - East
  sensor_pos.t = M_PI/4; //45'
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_x - sensor_pos.x)/cos(sensor_pos.t) * FURTHER_FACTOR);
  measured_distance = 2.5;
  ret = dsEstimatePositionOfOneSensor(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_x);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.x, sensor_pos_from_measurement.x);
  // the angle of the sensor should be rotated, so that the ray is longer
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);

  // Only East
  sensor_pos.t = 0; //0'
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_x - sensor_pos.x)/cos(sensor_pos.t) * FURTHER_FACTOR);
  ret = dsEstimatePositionOfOneSensor(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_x);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.x, sensor_pos_from_measurement.x);
  // the angle of the sensor is not checked

  // North - East
  sensor_pos.t = -M_PI/4;
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_x - sensor_pos.x)/cos(sensor_pos.t) * FURTHER_FACTOR);
  ret = dsEstimatePositionOfOneSensor(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_x);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.x, sensor_pos_from_measurement.x);
  // the angle of the sensor should be rotated, so that the ray is longer
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);
}

TEST(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsWest_When_MeasurementGreater_Then_MoveToEast)
{
  double wall_x = 2.0;
  pos_t sensor_pos, sensor_pos_from_measurement;
  ret_code_t ret;
  double measured_distance;

  sensor_pos.x = 4.0;
  sensor_pos.y = 10.0;

  // Three cases, always should update position of the sensor to be further from the wall
  // South - West
  sensor_pos.t = -5*M_PI/6; //-150'
  sensor_pos_from_measurement = sensor_pos;
  // this distance can be calculated nicely, but some cases were done on the paper
  // and I want to stick with them
  measured_distance = 2.5;
  ret = dsEstimatePositionOfOneSensor(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_x);
  posDump("sensor pred", sensor_pos);
  posDump("sensor meas", sensor_pos_from_measurement);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.x, sensor_pos_from_measurement.x);
  // the angle of the sensor should be rotated, so that the ray is longer
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);

  // Only West
  sensor_pos.t = M_PI; //180'
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_x - sensor_pos.x)/cos(sensor_pos.t) * FURTHER_FACTOR);
  ret = dsEstimatePositionOfOneSensor(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_x);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  posDump("sensor pred", sensor_pos);
  posDump("sensor meas", sensor_pos_from_measurement);
  // sensor should be further from the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.x, sensor_pos_from_measurement.x);
  // the angle of the sensor is not checked

  // North - West
  sensor_pos.t = 5*M_PI/6; //150'
  sensor_pos_from_measurement = sensor_pos;
  // this distance can be calculated nicely, but some cases were done on the paper
  // and I want to stick with them
  measured_distance = 2.5;
  ret = dsEstimatePositionOfOneSensor(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_x);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.x, sensor_pos_from_measurement.x);
  // the angle of the sensor should be rotated, so that the ray is longer
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);
}

TEST(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsNorth_When_MeasurementSmaller_Then_MoveToNorth)
{
  double wall_y = 4.0;
  pos_t sensor_pos, sensor_pos_from_measurement;
  ret_code_t ret;
  double measured_distance;

  sensor_pos.x = 5.0;
  sensor_pos.y = 3.0;

  // Three cases, always should update position of the sensor to be closer to the wall
  // North - West
  sensor_pos.t = deg2rad(125);
  sensor_pos_from_measurement = sensor_pos;
  // this distance can be calculated nicely, but some cases were done on the paper
  // and I want to stick with them
  measured_distance = 1.155;
  ret = dsEstimatePositionOfOneSensorY(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_y);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be closer to the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.y, sensor_pos_from_measurement.y);
  // the angle of the sensor should be rotated, so that the ray is shorter
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);

  // Only North
  sensor_pos.t = M_PI/2;
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_y - sensor_pos.y)/sin(sensor_pos.t)) * CLOSER_FACTOR;
  ret = dsEstimatePositionOfOneSensorY(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_y);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be closer to the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.y, sensor_pos_from_measurement.y);
  // the angle of the sensor is not checked

  // North - East
  sensor_pos.t = M_PI/4; //45'
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_y - sensor_pos.y)/sin(sensor_pos.t)) * CLOSER_FACTOR;
  ret = dsEstimatePositionOfOneSensorY(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_y);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be closer to the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.y, sensor_pos_from_measurement.y);
  // the angle of the sensor should be rotated, so that the ray is shorter
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);
}

TEST(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsNorth_When_MeasurementGreater_Then_MoveToSouth)
{
  double wall_y = 4.0;
  pos_t sensor_pos, sensor_pos_from_measurement;
  ret_code_t ret;
  double measured_distance;

  sensor_pos.x = 5.0;
  sensor_pos.y = 3.0;

  // Three cases, always should update position of the sensor to be closer to the wall
  // North - West
  sensor_pos.t = deg2rad(125);
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_y - sensor_pos.y)/sin(sensor_pos.t)) * FURTHER_FACTOR;
  ret = dsEstimatePositionOfOneSensorY(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_y);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.y, sensor_pos_from_measurement.y);
  // the angle of the sensor should be rotated, so that the ray is longer
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);

  // Only North
  sensor_pos.t = M_PI/2;
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_y - sensor_pos.y)/sin(sensor_pos.t)) * FURTHER_FACTOR;
  ret = dsEstimatePositionOfOneSensorY(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_y);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.y, sensor_pos_from_measurement.y);
  // the angle of the sensor is not checked

  // North - East
  sensor_pos.t = M_PI/4; //45'
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_y - sensor_pos.y)/sin(sensor_pos.t)) * FURTHER_FACTOR;
  ret = dsEstimatePositionOfOneSensorY(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_y);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.y, sensor_pos_from_measurement.y);
  // the angle of the sensor should be rotated, so that the ray is longer
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);
}

TEST(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsSouth_When_MeasurementGreater_Then_MoveToNorth)
{
  double wall_y = 2.0;
  pos_t sensor_pos, sensor_pos_from_measurement;
  ret_code_t ret;
  double measured_distance;

  sensor_pos.x = 5.0;
  sensor_pos.y = 3.0;

  // South - West
  sensor_pos.t = deg2rad(-125);
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_y - sensor_pos.y)/sin(sensor_pos.t)) * FURTHER_FACTOR;
  ret = dsEstimatePositionOfOneSensorY(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_y);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.y, sensor_pos_from_measurement.y);
  // the angle of the sensor should be rotated, so that the ray is longer
  TEST_ASSERT_LESS_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);

  // Only South
  sensor_pos.t = -M_PI/2;
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_y - sensor_pos.y)/sin(sensor_pos.t)) * FURTHER_FACTOR;
  ret = dsEstimatePositionOfOneSensorY(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_y);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.y, sensor_pos_from_measurement.y);
  // the angle of the sensor is not checked

  // South - East
  sensor_pos.t = -M_PI/4;
  sensor_pos_from_measurement = sensor_pos;
  measured_distance = fabs((wall_y - sensor_pos.y)/sin(sensor_pos.t)) * FURTHER_FACTOR;
  ret = dsEstimatePositionOfOneSensorY(sensor_pos, &sensor_pos_from_measurement, measured_distance, wall_y);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
  // sensor should be further from the wall
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.y, sensor_pos_from_measurement.y);
  // the angle of the sensor should be rotated, so that the ray is longer
  TEST_ASSERT_GREATER_THAN_DOUBLE(sensor_pos.t, sensor_pos_from_measurement.t);
}


TEST_GROUP_RUNNER(dsEstimatePositionOfOneSensor_FUN)
{
  RUN_TEST_CASE(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsEast_When_MeasurementSmaller_Then_MoveToEast)
  RUN_TEST_CASE(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsEast_When_MeasurementGreater_Then_MoveToWest)
  RUN_TEST_CASE(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsWest_When_MeasurementGreater_Then_MoveToEast)
  RUN_TEST_CASE(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsNorth_When_MeasurementSmaller_Then_MoveToNorth)
  RUN_TEST_CASE(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsNorth_When_MeasurementGreater_Then_MoveToSouth)
  RUN_TEST_CASE(dsEstimatePositionOfOneSensor_FUN, Given_DistSensorPointsSouth_When_MeasurementGreater_Then_MoveToNorth)
}




TEST_GROUP(dsInit_FUN);
TEST_SETUP(dsInit_FUN) { }
TEST_TEAR_DOWN(dsInit_FUN) { }

TEST(dsInit_FUN, When_Init_Then_ReturnSuccess)
{
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsInit(defaultCfg));
}

TEST_GROUP_RUNNER(dsInit_test)
{
  RUN_TEST_CASE(dsInit_FUN, When_Init_Then_ReturnSuccess);
}

double distance;
lab_specific_wall_t wall;
lab_specific_wall_t wall_expected;
ret_code_t ret;

void SetUpCommonForClosestWall(void)
{
  distance = -1;
  wall.cell_xy.x = UINT8_MAX;
  wall.cell_xy.y = UINT8_MAX;
  wall.direction = LAB_ALL_DIRS;
  wall_expected = wall;
  wall_expected.cell_xy = labCoordsStartingPoint();
  ret = RET_ERR_OTHER;
}


TEST_GROUP(DistanceToClosestWallNorth);
TEST_SETUP(DistanceToClosestWallNorth) {
  SetUpCommonForClosestWall();
  wall_expected.direction = LAB_NORTH;
  }
TEST_TEAR_DOWN(DistanceToClosestWallNorth) { }

TEST(DistanceToClosestWallNorth, GivenMidCell_When_PointingDirectly_Then_ReturnHalfCorridor)
{
  point_t mid = labCellToPoint(labCoordsStartingPoint());
  pos_t sensor_pos = {mid.x, mid.y, M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, LAB_DIM_CORRIDOR_HALF, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallNorth, GivenOneCmToWall_When_PointingDirectly_Then_ReturnOneCm)
{
  pos_t sensor_pos = {LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, LAB_DIM_CELL - ONE_CM, M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallNorth, GivenTwoCmToWall_When_PointingDirectly_Then_ReturnTwoCm)
{
  pos_t sensor_pos = {LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, LAB_DIM_CELL - TWO_CM, M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, TWO_CM, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallNorth, GivenTwoCmToWall_When_RotatedCW_Then_ReturnProperDistance)
{
  pos_t sensor_pos = {LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, LAB_DIM_CELL - TWO_CM, M_PI_2-M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, TWO_CM/sqrt(2), distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallNorth, GivenOneCmToWall_When_RotatedCCW_Then_ReturnProperDistance)
{
  pos_t sensor_pos = {LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, LAB_DIM_CELL - ONE_CM, M_PI_4+M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM/sqrt(2), distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST_GROUP(DistanceToClosestWallSouth);
TEST_SETUP(DistanceToClosestWallSouth) {
  SetUpCommonForClosestWall();
  wall_expected.direction = LAB_SOUTH;
}
TEST_TEAR_DOWN(DistanceToClosestWallSouth) { }

TEST(DistanceToClosestWallSouth, GivenMidCell_When_PointingDirectly_Then_ReturnHalfCorridor)
{
  point_t mid = labCellToPoint(labCoordsStartingPoint());
  pos_t sensor_pos = {mid.x, mid.y, -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, LAB_DIM_CORRIDOR_HALF, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallSouth, GivenOneCmToWall_When_PointingDirectly_Then_ReturnOneCm)
{
  pos_t sensor_pos = {LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, LAB_DIM_WALL + ONE_CM, -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallSouth, GivenTwoCmToWall_When_PointingDirectly_Then_ReturnTwoCm)
{
  pos_t sensor_pos = {LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, LAB_DIM_WALL + TWO_CM, -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, TWO_CM, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallSouth, GivenTwoCmToWall_When_RotatedCW_Then_ReturnProperDistance)
{
  pos_t sensor_pos = {LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, LAB_DIM_WALL + TWO_CM, -M_PI_2-M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, TWO_CM/sqrt(2), distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallSouth, GivenOneCmToWall_When_RotatedCCW_Then_ReturnProperDistance)
{
  pos_t sensor_pos = {LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, LAB_DIM_WALL + ONE_CM, -M_PI_2+M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM/sqrt(2), distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST_GROUP(DistanceToClosestWallEast);
TEST_SETUP(DistanceToClosestWallEast) {
  SetUpCommonForClosestWall();
  wall_expected.direction = LAB_EAST;
}
TEST_TEAR_DOWN(DistanceToClosestWallEast) { }

TEST(DistanceToClosestWallEast, GivenMidCell_When_PointingDirectly_Then_ReturnHalfCorridor)
{
  point_t mid = labCellToPoint(labCoordsStartingPoint());
  pos_t sensor_pos = {mid.x, mid.y, 0};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, LAB_DIM_CORRIDOR_HALF, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallEast, GivenOneCmToWall_When_PointingDirectly_Then_ReturnOneCm)
{
  pos_t sensor_pos = {LAB_DIM_CELL - ONE_CM, LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, 0};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallEast, GivenTwoCmToWall_When_PointingDirectly_Then_ReturnTwoCm)
{
  pos_t sensor_pos = {LAB_DIM_CELL - TWO_CM, LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, 0};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, TWO_CM, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallEast, GivenTwoCmToWall_When_RotatedCW_Then_ReturnProperDistance)
{
  pos_t sensor_pos = {LAB_DIM_CELL - TWO_CM, LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, -M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, TWO_CM/sqrt(2), distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallEast, GivenOneCmToWall_When_RotatedCCW_Then_ReturnProperDistance)
{
  pos_t sensor_pos = {LAB_DIM_CELL - ONE_CM, LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, +M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM/sqrt(2), distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST_GROUP(DistanceToClosestWallWest);
TEST_SETUP(DistanceToClosestWallWest) {
  SetUpCommonForClosestWall();
  wall_expected.direction = LAB_WEST;
}
TEST_TEAR_DOWN(DistanceToClosestWallWest) { }

TEST(DistanceToClosestWallWest, GivenMidCell_When_PointingDirectly_Then_ReturnHalfCorridor)
{
  point_t mid = labCellToPoint(labCoordsStartingPoint());
  pos_t sensor_pos = {mid.x, mid.y, M_PI};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, LAB_DIM_CORRIDOR_HALF, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallWest, GivenOneCmToWall_When_PointingDirectly_Then_ReturnOneCm)
{
  pos_t sensor_pos = {LAB_DIM_WALL + ONE_CM, LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, M_PI};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallWest, GivenTwoCmToWall_When_PointingDirectly_Then_ReturnTwoCm)
{
  pos_t sensor_pos = {LAB_DIM_WALL + TWO_CM, LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, M_PI};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, TWO_CM, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallWest, GivenTwoCmToWall_When_RotatedCW_Then_ReturnProperDistance)
{
  pos_t sensor_pos = {LAB_DIM_WALL + TWO_CM, LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, M_PI-M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, TWO_CM/sqrt(2), distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallWest, GivenOneCmToWall_When_RotatedCCW_Then_ReturnProperDistance)
{
  pos_t sensor_pos = {LAB_DIM_WALL + ONE_CM, LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL, M_PI+M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM/sqrt(2), distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST_GROUP(DistanceToClosestWallCornerCase);
TEST_SETUP(DistanceToClosestWallCornerCase) {
  SetUpCommonForClosestWall();
}
TEST_TEAR_DOWN(DistanceToClosestWallCornerCase) { }

TEST(DistanceToClosestWallCornerCase, GivenOneCmToWallAndWeirdCell_When_RotatedCCW_Then_ReturnProperDistance)
{
  wall_expected.cell_xy.x = 3;
  wall_expected.cell_xy.y = 5;
  wall_expected.direction = LAB_WEST;
  pos_t sensor_pos = {
    LAB_DIM_CELL*wall_expected.cell_xy.x + LAB_DIM_WALL + ONE_CM,
    LAB_DIM_CELL*wall_expected.cell_xy.y + LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL,
    M_PI+M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM/sqrt(2), distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}

TEST(DistanceToClosestWallCornerCase, GivenOneCmToWallAndWeirdCell_When_PointingDirectly_Then_ReturnProperDistance)
{
  wall_expected.cell_xy.x = 4;
  wall_expected.cell_xy.y = 7;
  wall_expected.direction = LAB_SOUTH;
  pos_t sensor_pos = {
      LAB_DIM_CELL * wall_expected.cell_xy.x + LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL,
      LAB_DIM_CELL * wall_expected.cell_xy.y + LAB_DIM_WALL + ONE_CM,
      -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, ONE_CM, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}


double const SMALL_DISTANCE = 0.001;
TEST_GROUP(DistanceToClosestWall);
TEST_SETUP(DistanceToClosestWall) {
  SetUpCommonForClosestWall();
}
TEST_TEAR_DOWN(DistanceToClosestWall) { }

TEST(DistanceToClosestWall, Given_InsideWall_When_NorthMax_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_N * LAB_DIM_CELL + LAB_DIM_WALL - SMALL_DISTANCE,
      .y = LAB_DIM_CELL_HALF,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWall, Given_InsideWall_When_SouthMax_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = + SMALL_DISTANCE,
      .y = LAB_DIM_CELL_HALF,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWall, Given_InsideWall_When_AnyHorizontal_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = 3 * LAB_DIM_CELL + LAB_DIM_WALL - SMALL_DISTANCE,
      .y = LAB_DIM_CELL_HALF,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}

TEST(DistanceToClosestWall, Given_InsideWall_When_EastMax_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CELL_HALF,
      .y = LAB_N * LAB_DIM_CELL + LAB_DIM_WALL - SMALL_DISTANCE,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}

TEST(DistanceToClosestWall, Given_InsideWall_When_WestMax_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CELL_HALF,
      .y = + SMALL_DISTANCE,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWall, Given_InsideWall_When_AnyVertical_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CELL_HALF,
      .y = 5 * LAB_DIM_CELL + LAB_DIM_WALL - SMALL_DISTANCE,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}

TEST(DistanceToClosestWall, Given_OutsideMaze_When_North_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CELL_HALF,
      .y = (LAB_N+1) * LAB_DIM_CELL,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWall, Given_OutsideMaze_When_South_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CELL_HALF,
      .y = -0.001,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWall, Given_OutsideMaze_When_East_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = (LAB_N+1) * LAB_DIM_CELL,
      .y = LAB_DIM_CELL_HALF,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWall, Given_OutsideMaze_When_West_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = -0.01,
      .y = LAB_DIM_CELL_HALF,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}

TEST_GROUP(DistanceToClosestWallInsideNotEdgeSouthWall);
TEST_SETUP(DistanceToClosestWallInsideNotEdgeSouthWall) {
  SetUpCommonForClosestWall();
}
TEST_TEAR_DOWN(DistanceToClosestWallInsideNotEdgeSouthWall) { }

TEST(DistanceToClosestWallInsideNotEdgeSouthWall, Given_South_When_PointsSouthDirectly_Then_ReturnSouthAndDistance)
{
  wall_expected.cell_xy.x = 0;
  wall_expected.cell_xy.y = 1;
  wall_expected.direction = LAB_SOUTH;

  pos_t sensor_pos = {
      .x = LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL,
      .y = LAB_DIM_CELL + LAB_DIM_WALL_HALF + SMALL_DISTANCE,
      .t = -M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, SMALL_DISTANCE, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}
// TE PONIŻEJ DO POPRAWKI
TEST(DistanceToClosestWallInsideNotEdgeSouthWall, Given_South_When_PointsSouthUnderAngle_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL,
      .y = LAB_DIM_CELL + LAB_DIM_WALL_HALF + SMALL_DISTANCE,
      .t = -M_PI_2 + M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);

  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWallInsideNotEdgeSouthWall, Given_South_When_PointsNorthDirectly_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL,
      .y = LAB_DIM_CELL + LAB_DIM_WALL_HALF + SMALL_DISTANCE,
      .t = +M_PI_2};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);

  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWallInsideNotEdgeSouthWall, Given_South_When_PointsNorthUnderAngle_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL,
      .y = LAB_DIM_CELL + LAB_DIM_WALL_HALF + SMALL_DISTANCE,
      .t = +M_PI_2+M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);

  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWallInsideNotEdgeSouthWall, Given_South_When_PointsEastDirectly_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL,
      .y = LAB_DIM_CELL + LAB_DIM_WALL_HALF + SMALL_DISTANCE,
      .t = 0};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);

  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWallInsideNotEdgeSouthWall, Given_South_When_PointsEastUnderAngle_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL,
      .y = LAB_DIM_CELL + LAB_DIM_WALL_HALF + SMALL_DISTANCE,
      .t = 0+M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);

  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}
TEST(DistanceToClosestWallInsideNotEdgeSouthWall, Given_South_When_PointsWestDirectly_Then_ReturnError)
{
  pos_t sensor_pos = {
      .x = LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL,
      .y = LAB_DIM_CELL + LAB_DIM_WALL_HALF + SMALL_DISTANCE,
      .t = +M_PI};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);

  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}

TEST(DistanceToClosestWallInsideNotEdgeSouthWall, When_PointsSouthDirectly_Then_ReturnSouthAndDistance)
{
  wall_expected.cell_xy.x = 0;
  wall_expected.cell_xy.y = 0;
  wall_expected.direction = LAB_SOUTH;

  pos_t sensor_pos = {
      .x = LAB_DIM_CORRIDOR_HALF+LAB_DIM_WALL,
      .y = LAB_DIM_CELL + LAB_DIM_WALL_HALF + SMALL_DISTANCE,
      .t = +M_PI + M_PI_4};

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, &distance);

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(DELTA, LAB_DIM_WALL_HALF+LAB_DIM_CORRIDOR +SMALL_DISTANCE, distance);
  TEST_ASSERT_EQUAL_WALL(wall_expected, wall);
}


TEST_GROUP_RUNNER(Pointing)
{
  RUN_TEST_CASE(DistanceToClosestWallNorth, GivenMidCell_When_PointingDirectly_Then_ReturnHalfCorridor);
  RUN_TEST_CASE(DistanceToClosestWallNorth, GivenOneCmToWall_When_PointingDirectly_Then_ReturnOneCm)
  RUN_TEST_CASE(DistanceToClosestWallNorth, GivenTwoCmToWall_When_PointingDirectly_Then_ReturnTwoCm)
  RUN_TEST_CASE(DistanceToClosestWallNorth, GivenTwoCmToWall_When_RotatedCW_Then_ReturnProperDistance)
  RUN_TEST_CASE(DistanceToClosestWallNorth, GivenOneCmToWall_When_RotatedCCW_Then_ReturnProperDistance)

  RUN_TEST_CASE(DistanceToClosestWallSouth, GivenMidCell_When_PointingDirectly_Then_ReturnHalfCorridor);
  RUN_TEST_CASE(DistanceToClosestWallSouth, GivenOneCmToWall_When_PointingDirectly_Then_ReturnOneCm)
  RUN_TEST_CASE(DistanceToClosestWallSouth, GivenTwoCmToWall_When_PointingDirectly_Then_ReturnTwoCm)
  RUN_TEST_CASE(DistanceToClosestWallSouth, GivenTwoCmToWall_When_RotatedCW_Then_ReturnProperDistance)
  RUN_TEST_CASE(DistanceToClosestWallSouth, GivenOneCmToWall_When_RotatedCCW_Then_ReturnProperDistance)

  RUN_TEST_CASE(DistanceToClosestWallEast, GivenMidCell_When_PointingDirectly_Then_ReturnHalfCorridor);
  RUN_TEST_CASE(DistanceToClosestWallEast, GivenOneCmToWall_When_PointingDirectly_Then_ReturnOneCm)
  RUN_TEST_CASE(DistanceToClosestWallEast, GivenTwoCmToWall_When_PointingDirectly_Then_ReturnTwoCm)
  RUN_TEST_CASE(DistanceToClosestWallEast, GivenTwoCmToWall_When_RotatedCW_Then_ReturnProperDistance)
  RUN_TEST_CASE(DistanceToClosestWallEast, GivenOneCmToWall_When_RotatedCCW_Then_ReturnProperDistance)

  RUN_TEST_CASE(DistanceToClosestWallWest, GivenMidCell_When_PointingDirectly_Then_ReturnHalfCorridor);
  RUN_TEST_CASE(DistanceToClosestWallWest, GivenOneCmToWall_When_PointingDirectly_Then_ReturnOneCm)
  RUN_TEST_CASE(DistanceToClosestWallWest, GivenTwoCmToWall_When_PointingDirectly_Then_ReturnTwoCm)
  RUN_TEST_CASE(DistanceToClosestWallWest, GivenTwoCmToWall_When_RotatedCW_Then_ReturnProperDistance)
  RUN_TEST_CASE(DistanceToClosestWallWest, GivenOneCmToWall_When_RotatedCCW_Then_ReturnProperDistance)


  RUN_TEST_CASE(DistanceToClosestWallCornerCase, GivenOneCmToWallAndWeirdCell_When_RotatedCCW_Then_ReturnProperDistance)
  RUN_TEST_CASE(DistanceToClosestWallCornerCase, GivenOneCmToWallAndWeirdCell_When_PointingDirectly_Then_ReturnProperDistance)

  // Inside wall: return failure
  RUN_TEST_CASE(DistanceToClosestWall, Given_InsideWall_When_NorthMax_Then_ReturnError)
  RUN_TEST_CASE(DistanceToClosestWall, Given_InsideWall_When_SouthMax_Then_ReturnError)
  RUN_TEST_CASE(DistanceToClosestWall, Given_InsideWall_When_AnyHorizontal_Then_ReturnError)

  RUN_TEST_CASE(DistanceToClosestWall, Given_InsideWall_When_EastMax_Then_ReturnError)
  RUN_TEST_CASE(DistanceToClosestWall, Given_InsideWall_When_WestMax_Then_ReturnError)
  RUN_TEST_CASE(DistanceToClosestWall, Given_InsideWall_When_AnyVertical_Then_ReturnError)

  RUN_TEST_CASE(DistanceToClosestWall, Given_OutsideMaze_When_North_Then_ReturnError)
  RUN_TEST_CASE(DistanceToClosestWall, Given_OutsideMaze_When_South_Then_ReturnError)
  RUN_TEST_CASE(DistanceToClosestWall, Given_OutsideMaze_When_East_Then_ReturnError)
  RUN_TEST_CASE(DistanceToClosestWall, Given_OutsideMaze_When_West_Then_ReturnError)
}

int main(void)
{
  UNITY_BEGIN();

  // TODO test <0 values
  // test when due to offset.y sensor is in didfferent cell then mouse
  // test boudary values, between cells and so on

  RUN_TEST_GROUP(dsInit_test);
  RUN_TEST_GROUP(dsEstimatePositionOfOneSensor_FUN);
  RUN_TEST_GROUP(Pointing);


  return (UnityEnd());
}
