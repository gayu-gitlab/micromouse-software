

#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "unity.h"
#include "unity_fixture.h"

#include "path_planner.h"
#include "labirynths.h"

#include "logger.h"
#include "platform_base.h"
#include "printer.h"
#include "labirynth_mock.h"
#include "labirynths.h"

static vector_t path;
char mem[1000];
static vector_t pathB;
char memB[1000];
lab_cellxy_t from, to;
ret_code_t ret;
lab_cellxy_t next_target;

// Used for small demo
static const bool VISUALIZE = false;

TEST_GROUP(find_path_given_lab_fully_known);
TEST_SETUP(find_path_given_lab_fully_known)
{
  memset(mem, 0, sizeof(mem));
  memset(memB, 0, sizeof(mem));
  from.x = rand();
  from.y = rand();
  to.x = rand();
  to.y = rand();
  vector_init(&path, mem, sizeof(mem), sizeof(lab_cellxy_t));
  vector_init(&pathB, memB, sizeof(memB), sizeof(lab_cellxy_t));
  // Use well known labirynth
  labLoad(labirynths[LABIRYNTHS_A]);
  pp_init();
}
TEST_TEAR_DOWN(find_path_given_lab_fully_known) {}

TEST(find_path_given_lab_fully_known, When_PathAvailable_Then_FindIt)
{
  from.x = 0;
  from.y = 0;
  to.x = 7;
  to.y = 7;
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_init(&path, mem, sizeof(mem), sizeof(lab_cellxy_t)));

  from.x = 5;
  from.y = 6;
  to.x = 9;
  to.y = 6;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pp_find_path(from, to, &path));
  TEST_ASSERT_EQUAL(5, vector_item_cnt(path));

  from.x = 0;
  from.y = 0;
  to.x = 7;
  to.y = 7;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pp_find_path(from, to, &path));
  TEST_ASSERT_EQUAL(69, vector_item_cnt(path));

  from.x = 0;
  from.y = 0;
  to.x = 8;
  to.y = 0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pp_find_path(from, to, &path));
  TEST_ASSERT_EQUAL(97, vector_item_cnt(path));

  from.x = 11;
  from.y = 11;
  to.x = 8;
  to.y = 7;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pp_find_path(from, to, &path));
  TEST_ASSERT_EQUAL(20, vector_item_cnt(path));

  from.x = 8;
  from.y = 5;
  to.x = 9;
  to.y = 5;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pp_find_path(from, to, &path));
  TEST_ASSERT_EQUAL(112, vector_item_cnt(path));
  // printLabWithOneRoute(path);
}

TEST(find_path_given_lab_fully_known, When_NoConnection_Then_ReturnErrNotFound)
{
  from.x = 3;
  from.y = 3;
  to.x = 4;
  to.y = 3;
  TEST_ASSERT_EQUAL(RET_ERR_NOT_FOUND, pp_find_path(from, to, &path));
}

TEST(find_path_given_lab_fully_known, When_InvalidCoords_Then_ReturnErr)
{
  from.x = 29;
  from.y = 5;
  to.x = 8;
  to.y = 6;
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, pp_find_path(from, to, &path));
}

TEST(find_path_given_lab_fully_known, When_ToEqualsFrom_Then_ReturnOnePointRoute)
{
  from.x = 4;
  from.y = 3;
  to.x = 4;
  to.y = 3;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pp_find_path(from, to, &path));
  TEST_ASSERT_EQUAL(1, vector_item_cnt(path));
  // printLabWithOneRoute(path);
}

TEST_GROUP(find_path_unknown_lab);
TEST_SETUP(find_path_unknown_lab)
{
  memset(mem, 0, sizeof(mem));
  memset(memB, 0, sizeof(mem));
  from.x = rand();
  from.y = rand();
  to.x = rand();
  to.y = rand();
  vector_init(&path, mem, sizeof(mem), sizeof(lab_cellxy_t));
  vector_init(&pathB, memB, sizeof(memB), sizeof(lab_cellxy_t));
  labResetToDefault();
  pp_init();
}
TEST_TEAR_DOWN(find_path_unknown_lab) {}

/**
 * @brief Pathplanner should choose the way, even though labirynth is not known
 * Used during mapping
 */
TEST(find_path_unknown_lab, When_LabDefaultNoKnown_Then_FindPathAndReturnMappingNeeded)
{
  lab_cellxy_t startingCell = {0, 0};
  lab_cellxy_t Goal = {7, 7};

  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_init(&path, mem, sizeof(mem), sizeof(lab_cellxy_t)));
  TEST_ASSERT_EQUAL(RET_MAPPING_NEEDED, pp_find_path(startingCell, Goal, &path));
  TEST_ASSERT_EQUAL(15u, vector_item_cnt(path));
  printLabWithOneRoute(path);
}

/**
 * @brief Route should be changed, when there is wall detected on the way
 * This applies during mapping, when the map is not yet known
 */
TEST(find_path_unknown_lab, When_WallDetectedOnRoute_Then_ChangeTheWay)
{
  vector_t pathWithoutObstacle, pathWithObstacle;
  lab_cellxy_t startingCell = {0, 0};
  lab_cellxy_t Goal = {7, 7};

  vector_init(&pathWithObstacle, mem, sizeof(mem), sizeof(lab_cellxy_t));
  vector_init(&pathWithoutObstacle, memB, sizeof(mem), sizeof(lab_cellxy_t));
  pp_find_path(startingCell, Goal, &pathWithoutObstacle);

  // update walls, add new on the way
  labWallChange((lab_cellxy_t){0, 1}, LAB_EAST, true);
  labWallChange((lab_cellxy_t){0, 2}, LAB_EAST, true);
  pp_find_path(startingCell, Goal, &pathWithObstacle);
  printLabWithTwoRoutes(pathWithObstacle, pathWithoutObstacle);

  TEST_ASSERT_FALSE(vector_are_equal(pathWithObstacle, pathWithoutObstacle));
  TEST_ASSERT_TRUE(vector_item_cnt(pathWithObstacle) > 0u);
}

/**
 * @brief This test is showing how the mapping should be implemented
 * and also allows to check visually that everything goes as expected.
 */
TEST(find_path_unknown_lab, COMPLICATED_When_GoingToGoal_Then_AchieveIt)
{
  const uint16_t MAX_ITER = 1000u;
  lab_cellxy_t currentPos = labCoordsStartingPoint();
  const lab_cellxy_t GOAL = labCoordsGoalNE();

  // labirynth is not known
  labResetToDefault();

  for (uint16_t i = 0u; i < MAX_ITER; i++)
  {
    // plan where to go
    ret = pp_find_path(currentPos, GOAL, &path);
    if(ret != RET_SUCCESS && ret != RET_MAPPING_NEEDED)
    {
      TEST_ASSERT_MESSAGE(false, "Finding error");
    }

    if(VISUALIZE)
    {
      // print also full path from start to middle
      pp_find_path(labCoordsStartingPoint(), GOAL, &pathB);
      printLabWithTwoRoutes(path, pathB);
      usleep(400*1000);
    }

    if(ret == RET_SUCCESS)
    {
      printf("break\n");
      break;
    }

    // move one tile ahead
    vector_at(path, 1u, &currentPos);
    // discover the cell
    labWallChange(currentPos, labirynths[LABIRYNTHS_A][currentPos.x][currentPos.y], true);
    labWallChange(currentPos, (~labirynths[LABIRYNTHS_A][currentPos.x][currentPos.y]), false);
  }

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
}

TEST_GROUP(plan_mapping);
TEST_SETUP(plan_mapping)
{
  memset(mem, 0, sizeof(mem));
  memset(memB, 0, sizeof(mem));
  from.x = rand();
  from.y = rand();
  to.x = rand();
  to.y = rand();
  vector_init(&path, mem, sizeof(mem), sizeof(lab_cellxy_t));
  vector_init(&pathB, memB, sizeof(memB), sizeof(lab_cellxy_t));
  labResetToDefault();
  pp_init();
}
TEST_TEAR_DOWN(plan_mapping) {}

TEST(plan_mapping, When_InvalidStartingPoint_Then_ReturnError)
{
  lab_cellxy_t invalid_cell = {LAB_N, 0};

  ret = pp_plan_mapping(invalid_cell, &next_target, &path, &pathB);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);

  invalid_cell.x = 0;
  invalid_cell.y = LAB_N;
  ret = pp_plan_mapping(invalid_cell, &next_target, &path, &pathB);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}

TEST(plan_mapping, When_NullPointerParam_Then_ReturnError)
{
  ret = pp_plan_mapping(labCoordsStartingPoint(), NULL, &path, &pathB);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);

  ret = pp_plan_mapping(labCoordsStartingPoint(), &next_target, NULL, &pathB);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);

  ret = pp_plan_mapping(labCoordsStartingPoint(), &next_target, &path, NULL);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}

TEST(plan_mapping, When_WrongVectors_Then_ReturnError)
{
  int invalid_size = 23;
  vector_init(&path, mem, sizeof(mem), invalid_size);
  ret = pp_plan_mapping(labCoordsStartingPoint(), NULL, &path, &pathB);
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}

/**
 * @brief For the initial run, the pp_find_path with current position and goal cell
 * should be used.
 */
TEST(plan_mapping, When_RouteToMiddleNotKnown_Then_ReturnNextCellAndMappingNeeded)
{
  lab_cellxy_t expected_next_target = {0, 1};

  ret = pp_plan_mapping(labCoordsStartingPoint(), &next_target, &path, &pathB);
  TEST_ASSERT_EQUAL(RET_MAPPING_NEEDED, ret);
  TEST_ASSERT_TRUE(labCoordsEqual(next_target, expected_next_target));
}

/**
 * @brief For the initial run, the pp_find_path with current position and goal cell
 * should be used.
 */
TEST(plan_mapping, When_NotifiedNewWall_Then_ReturnNextCellAndMappingNeeded)
{
  lab_cellxy_t expected_next_target = {0, 2};

  labWallChange((lab_cellxy_t){0, 1}, LAB_EAST|LAB_WEST, true);
  labWallChange((lab_cellxy_t){0, 1}, LAB_NORTH, false);
  ret = pp_plan_mapping(labCoordsStartingPoint(), &next_target, &path, &pathB);
  TEST_ASSERT_EQUAL(RET_MAPPING_NEEDED, ret);
  TEST_ASSERT_TRUE(labCoordsEqual(next_target, expected_next_target));
}

/**
 * @brief For the initial run, the pp_find_path with current position and goal cell
 * should be used.
 */
TEST(plan_mapping, When_FullLabIsKnown_Then_ReturnStartingPointAndSuccess)
{
  lab_cellxy_t expected_next_target = labCoordsStartingPoint();

  labLoad(labirynths[LABIRYNTHS_A]);
  ret = pp_plan_mapping(labCoordsStartingPoint(), &next_target, &path, &pathB);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_TRUE(labCoordsEqual(next_target, expected_next_target));
}

/**
 * @brief This is quite complicated UT,
 * I have mixed feelings about it, as:
 * - it is too long and too complicated
 * + but it shows how the robot is mapping
 */
TEST(plan_mapping, COMPLICATED_When_MappingLabirynthA_Then_AchieveGoalAndTurnAround)
{
  const uint16_t MAX_ITER = 1000u;
  lab_cellxy_t currentPos = labCoordsStartingPoint();
  const lab_cellxy_t GOAL = labCoordsGoalNE();

  // labirynth is not known
  labResetToDefault();

  // this part is equal to UT COMPLICATED_When_GoingToGoal_Then_AchieveIt
  for (uint16_t i = 0u; i < MAX_ITER; i++)
  {
    // plan where to go
    ret = pp_find_path(currentPos, GOAL, &path);

    if(ret == RET_SUCCESS)
    {
      printf("break\n");
      break;
    }

    // move one tile ahead
    vector_at(path, 1u, &currentPos);
    // discover the cell
    labWallChange(currentPos, labirynths[LABIRYNTHS_A][currentPos.x][currentPos.y], true);
    labWallChange(currentPos, (~labirynths[LABIRYNTHS_A][currentPos.x][currentPos.y]), false);
  }

  // in this moment the robot achieved goal, but the best solution is not known yet, and robot
  // must plan where to go to map optimally
  ret = pp_find_path(labCoordsStartingPoint(), GOAL, &path);
  TEST_ASSERT_EQUAL(RET_MAPPING_NEEDED, ret);
  ret = pp_plan_mapping(currentPos, &next_target, &path, &pathB);
  TEST_ASSERT_EQUAL(RET_MAPPING_NEEDED, ret);

  bool breakk = false;
  for (uint16_t i = 0u; i < MAX_ITER; i++)
  {
    // plan where to go
    ret = pp_plan_mapping(currentPos, &next_target, &path, &pathB);
    if(ret == RET_SUCCESS)
    {
      breakk = true;
    }
    else if(ret != RET_MAPPING_NEEDED)
    {
      TEST_ASSERT_MESSAGE(false, "Mapping error");
    }

    // find the way to go there
    ret = pp_find_path(currentPos, next_target, &path);
    if(ret != RET_SUCCESS && ret != RET_MAPPING_NEEDED)
    {
      TEST_ASSERT_MESSAGE(false, "Finding error");
    }

    if(VISUALIZE)
    {
      // print also full path from start to middle
      pp_find_path(labCoordsStartingPoint(), labCoordsGoalNE(), &pathB);
      printLabWithTwoRoutes(path, pathB);
      usleep(400*1000);
    }

    if(breakk)
    {
      break;
    }

    // move one tile ahead
    vector_at(path, 1u, &currentPos);
    // discover the cell
    labWallChange(currentPos, labirynths[LABIRYNTHS_A][currentPos.x][currentPos.y], true);
    labWallChange(currentPos, (~labirynths[LABIRYNTHS_A][currentPos.x][currentPos.y]), false);
  }

  // full path from beginning to end should be known
  ret = pp_find_path(labCoordsStartingPoint(), labCoordsGoalNE(), &pathB);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
}

TEST_GROUP_RUNNER(find_path_given_lab_fully_known)
{
  // All these tests use "labirynth A"
  RUN_TEST_CASE(find_path_given_lab_fully_known, When_PathAvailable_Then_FindIt);
  RUN_TEST_CASE(find_path_given_lab_fully_known, When_NoConnection_Then_ReturnErrNotFound);
  RUN_TEST_CASE(find_path_given_lab_fully_known, When_InvalidCoords_Then_ReturnErr);
  RUN_TEST_CASE(find_path_given_lab_fully_known, When_ToEqualsFrom_Then_ReturnOnePointRoute);
}

TEST_GROUP_RUNNER(find_path_unknown_lab)
{
  RUN_TEST_CASE(find_path_unknown_lab, When_LabDefaultNoKnown_Then_FindPathAndReturnMappingNeeded);
  RUN_TEST_CASE(find_path_unknown_lab, When_WallDetectedOnRoute_Then_ChangeTheWay);
  RUN_TEST_CASE(find_path_unknown_lab, COMPLICATED_When_GoingToGoal_Then_AchieveIt);
}

TEST_GROUP_RUNNER(plan_mapping)
{
  RUN_TEST_CASE(plan_mapping, When_InvalidStartingPoint_Then_ReturnError)
  RUN_TEST_CASE(plan_mapping, When_NullPointerParam_Then_ReturnError)
  RUN_TEST_CASE(plan_mapping, When_WrongVectors_Then_ReturnError)
  RUN_TEST_CASE(plan_mapping, When_RouteToMiddleNotKnown_Then_ReturnNextCellAndMappingNeeded)
  RUN_TEST_CASE(plan_mapping, When_NotifiedNewWall_Then_ReturnNextCellAndMappingNeeded);
  RUN_TEST_CASE(plan_mapping, When_FullLabIsKnown_Then_ReturnStartingPointAndSuccess);
  RUN_TEST_CASE(plan_mapping, COMPLICATED_When_MappingLabirynthA_Then_AchieveGoalAndTurnAround);
}

int main(void)
{
  UNITY_BEGIN();
  log_initialize(platformPutChar, LOGGER_LVL_INFO);

  RUN_TEST_GROUP(find_path_given_lab_fully_known);
  RUN_TEST_GROUP(find_path_unknown_lab);
  RUN_TEST_GROUP(plan_mapping);


  return (UnityEnd());
}
