
#include <string.h>


#include "pid.h"
#include "point.h"
#include "unity.h"
#include "platform_base.h"


uint8_t pidMem[PID_CONTROLLER_STRUCT_SIZE_BYTES];
pid_controller_t pid = (pid_controller_t) pidMem;

double P=1, I=1, D=0.1;
double valid_time_s = 1;
double error = 0;
double u;

void setUp(void)
{
  memset(pidMem, 0, sizeof(pidMem));
  P=1; I=1; D=0.1;
  valid_time_s = 1;
  error = 0;
  u = 0;
}

void tearDown(void)
{
}


void When_Inited_Then_ReturnSuccess(void)
{
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidInit(pid, 1, 1, 1));
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
}

void When_WrongTimeProvided_Then_ReturnFailure(void)
{
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidInit(pid, P, I, D));

  double wrong_time = 0;
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, pidStep(pid, wrong_time, 0, &u));
}

void When_ErrorIsZero_Then_OutputIsConstantlyZero(void)
{
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidInit(pid, P, I, D));
  error = 0;

  // no error - control signal should not change
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_EQUAL(0, u);
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_EQUAL(0, u);
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_EQUAL(0, u);
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_EQUAL(0, u);
}

void When_ErrorIsConstant_Then_OutputIsRising(void)
{
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidInit(pid, P, I, D));
  error = 0.1;

  // constant error, control signal should increase
  double u_prev = 0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_GREATER_THAN_DOUBLE(u_prev, u); u_prev = u;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_GREATER_THAN_DOUBLE(u_prev, u); u_prev = u;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_GREATER_THAN_DOUBLE(u_prev, u); u_prev = u;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_GREATER_THAN_DOUBLE(u_prev, u); u_prev = u;
  TEST_ASSERT_EQUAL(RET_SUCCESS, pidStep(pid, valid_time_s, error, &u));
  TEST_ASSERT_GREATER_THAN_DOUBLE(u_prev, u); u_prev = u;
}


int main(void)
{
  UNITY_BEGIN();
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
  RUN_TEST(When_Inited_Then_ReturnSuccess);
  RUN_TEST(When_WrongTimeProvided_Then_ReturnFailure);
  RUN_TEST(When_ErrorIsZero_Then_OutputIsConstantlyZero);
  RUN_TEST(When_ErrorIsConstant_Then_OutputIsRising);


  return (UnityEnd());
}
