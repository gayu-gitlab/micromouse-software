

#include "position.h"
#include "useful.h"
#include "unity.h"
#include "logger.h"
#include "platform_base.h"


void setUp(void)
{
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
}

void tearDown(void)
{
}

void posGetPoint_test(void)
{
  TEST_ASSERT_DOUBLE_WITHIN(1e-9, 10, posGetPoint((pos_t) {10, 11, 12}).x);
  TEST_ASSERT_DOUBLE_WITHIN(1e-9, 11, posGetPoint((pos_t) {10, 11, 12}).y);
}

void posGetHeadingToPoint_test(void)
{
  pos_t from;
  point_t to;

  from.x = 0, from.y = 0; from.t = 0;
  to.x = 0, to.y = 0;
  TEST_ASSERT_EQUAL_DOUBLE(0, posGetHeadingToPoint(from, to));
  from.t = 2;
  TEST_ASSERT_EQUAL_DOUBLE(0, posGetHeadingToPoint(from, to));

  from.x = 0, from.y = 0; from.t = 0;
  to.x = 1, to.y = 0;
  TEST_ASSERT_EQUAL_DOUBLE(0,      posGetHeadingToPoint(from, to));
  from.t = M_PI/2;
  TEST_ASSERT_EQUAL_DOUBLE(-M_PI/2,posGetHeadingToPoint(from, to));

  from.x = 1, from.y = 0; from.t = 0;
  to.x = 0, to.y = 0;
  TEST_ASSERT_EQUAL_DOUBLE(M_PI,   posGetHeadingToPoint(from, to));
  from.t = M_PI/2;
  TEST_ASSERT_EQUAL_DOUBLE(M_PI/2, posGetHeadingToPoint(from, to));

  from.x = 1, from.y = 1; from.t = 0;
  to.x = 2, to.y = 0;
  TEST_ASSERT_EQUAL_DOUBLE(-M_PI/4, posGetHeadingToPoint(from, to));

  from.x = 2, from.y = 0; from.t = 0;
  to.x = 1, to.y = 1;
  TEST_ASSERT_EQUAL_DOUBLE(3*M_PI/4,posGetHeadingToPoint(from, to));

  //tests from function description
  from.x = 1, from.y = 1; from.t = M_PI/2;
  to.x = 2, to.y = 1;
  TEST_ASSERT_EQUAL_DOUBLE(-M_PI/2,posGetHeadingToPoint(from, to));

  from.x = 0, from.y = 0; from.t = M_PI;
  to.x = 1, to.y = -1;
  TEST_ASSERT_EQUAL_DOUBLE(3*M_PI/4,posGetHeadingToPoint(from, to));
}

void posCoordsLocalToGlobal_test(void)
{
  pos_t result;

  struct GivenExpected{
    pos_t posLocal;
    pos_t state;
    pos_t posGlobal;
  };

  struct GivenExpected values[] = {
    // mouse on 0,0, rotated to N
    { {1.00, 2.00, 0.00},        {0.00, 0.00, M_PI/2},    {1.00, 2.00, M_PI/2} },
    // mouse on starting pos, rotated to N
    { {-0.04, 0.01, M_PI/2},     {0.096, 0.096, M_PI/2},  {0.056, 0.106, M_PI} },
    // mouse on starting pos, rotated to NEE (30'), weird position
    { {0.05, -0.05, -M_PI/4},     {0.096, 0.096, M_PI/4},  {0.096, 0.096 - 0.05 * sqrt(2), 0} },
  };
  for(uint8_t i = 0; i < ARRAY_SIZE(values); i++)
  {
    result = posCoordsLocalToGlobal(values[i].posLocal, values[i].state);
    TEST_ASSERT_DOUBLE_WITHIN(1e-6, values[i].posGlobal.x, result.x);
    TEST_ASSERT_DOUBLE_WITHIN(1e-6, values[i].posGlobal.y, result.y);
    TEST_ASSERT_DOUBLE_WITHIN(1e-6, values[i].posGlobal.t, result.t);
  }
}

void posRotateAroundPoint_test(void)
{
  pos_t end;

  struct PointRotationPoint{
    pos_t start;
    point_t about;
    pos_t expected;
    double angleRad;
  };
  struct PointRotationPoint testValues[] =
  {
    {{ 1, 2, 0.00},     { 0.00,  0.00 },   { 1, 2, 0.00}, 0},
    {{ 4, -3, 0.00},    { 0.00,  0.00 },   {4, -3, 0.00}, 0},
    {{ 4, -3, 0.00},    { 0.00,  0.00 },   {4, -3, 0.00}, M_PI*2},
    {{ 4, -3, 0.00},    { 0.00,  0.00 },   {4, -3, 0.00}, M_PI*4},
    {{ 1, 2, 0.00},     { 0.00,  0.00 },   { -2, 1, M_PI/2}, M_PI/2},
    {{ 1, 2, 0.00},     { 0.00,  0.00 },   { -1, -2, M_PI}, M_PI},

    {{ 1, 2, 0.00},     { 1.00,  1.00 },   { 0.00, 1.00 , M_PI/2}, M_PI/2 },
    {{ 1, 2, 0.00},     { 1.00,  1.00 },   { 1.00, 0.00 , M_PI}, M_PI },
    {{ 1, 2, -M_PI/2},  { 1.00,  1.00 },   { 2.00, 1.00 , M_PI}, 3*M_PI/2 },

    {{ 2, -1, M_PI/6},  { 1.00,  0.00 },   { 1.00 + sqrt(2), 0.00 , 5*M_PI/12}, M_PI/4 },
  };

  for(uint8_t i = 0; i < ARRAY_SIZE(testValues); i++)
  {
    log_info("iter %d", i);
    end = posRotateAroundPoint(
        testValues[i].start, testValues[i].about, testValues[i].angleRad);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, testValues[i].expected.x, end.x);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, testValues[i].expected.y, end.y);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, testValues[i].expected.t, end.t);
  }
}

void posCoordsGlobalToLocal_test(void)
{
  pos_t result;

  struct GivenExpected{
    pos_t posLocal;
    pos_t stateMouse;
    pos_t posGlobal;
  };

  struct GivenExpected values[] = {
    // mouse on 0,0, rotated to N
    { {1.00, 2.00, 0.00},        {0.00, 0.00, M_PI/2},    {1.00, 2.00, M_PI/2} },
    // mouse on starting pos, rotated to N
    { {-0.04, 0.01, M_PI/2},     {0.096, 0.096, M_PI/2},  {0.056, 0.106, M_PI} },
    // mouse on starting pos, rotated to NEE (30'), weird position
    { {0.05, -0.05, -M_PI/4},    {0.096, 0.096, M_PI/4},  {0.096, 0.096 - 0.05 * sqrt(2), 0} },
    // mouse on 0,0, rotated to S,
    { {1, 2, -3*M_PI/4},         {0.00, 0.00, -M_PI/2},   {-1.0, -2.0, 3*M_PI/4} },
    // same, but mouse moved without rotation
    { {1, 2, -3*M_PI/4},         {3.00, 4.00, -M_PI/2},   {2.0, 2.0, 3*M_PI/4} },
    // mouse on 0,0, weird position
    { {3, 4, M_PI/4},            {0.00, 0.00, 0.00},   {cos(-atan2(3,4))*5, sin(-atan2(3,4))*5, M_PI/4} },
    // mouse on 0,0, weird position & weird rotation
    { {3, 4, M_PI/4},            {0.00, 0.00, -M_PI/6},   {cos(-atan2(3,4)-M_PI/6)*5, sin(-atan2(3,4)-M_PI/6)*5, M_PI/12} },
    // same, but also weird position of mouse
    { {3, 4, M_PI/4},            {1.23, 3.45, -M_PI/6},   {1.23 + cos(-atan2(3,4)-M_PI/6)*5, 3.45+sin(-atan2(3,4)-M_PI/6)*5, M_PI/12} },
  };

  for(uint8_t i = 0; i < ARRAY_SIZE(values); i++)
  {
    log_info("iter %d", i);
    result = posCoordsGlobalToLocal(values[i].posGlobal, values[i].stateMouse);
    TEST_ASSERT_DOUBLE_WITHIN(1e-6, values[i].posLocal.x, result.x);
    TEST_ASSERT_DOUBLE_WITHIN(1e-6, values[i].posLocal.y, result.y);
    TEST_ASSERT_DOUBLE_WITHIN(1e-6, values[i].posLocal.t, result.t);
  }
}

void posCoordsTranslation_test(void)
{
  pos_t ret;

  struct GivenExpected{
    pos_t pos;
    pos_t s;
  };

  struct GivenExpected values[] = {
    { {1.00, 2.00, 0.00},        {0.00, 0.00, M_PI/2} },
    { {4.00, 3.00, 0.00},        {0.00, 0.00, M_PI/2} },
    { {4.00, 3.00, 0.00},        {0.00, 0.00, M_PI} },
    { {4.00, 3.00, 0.00},        {0.00, 0.00, M_PI/14} },
    { {4.00, 3.00, 0.00},        {0.00, 0.00, 0.2321} },
    { {4.00, 3.00, 0.00},        {1.00, 2.00, 0.00} },
    { {4.00, 5.00, 0.00},        {2.00, 2.00, 0.00} },
  };

  for(uint8_t i = 0; i < ARRAY_SIZE(values); i++)
  {
    log_info("iter %d", i);
    ret = posCoordsLocalToGlobal(posCoordsGlobalToLocal(values[i].pos, values[i].s), values[i].s);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, values[i].pos.x, ret.x);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, values[i].pos.y, ret.y);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, values[i].pos.t, ret.t);

    ret = posCoordsGlobalToLocal(posCoordsLocalToGlobal(values[i].pos, values[i].s), values[i].s);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, values[i].pos.x, ret.x);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, values[i].pos.y, ret.y);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, values[i].pos.t, ret.t);
  }
}


int main(void)
{
  UNITY_BEGIN();

  RUN_TEST(posGetPoint_test);
  RUN_TEST(posGetHeadingToPoint_test);
  RUN_TEST(posRotateAroundPoint_test);
  RUN_TEST(posCoordsLocalToGlobal_test);
  RUN_TEST(posCoordsGlobalToLocal_test);
  RUN_TEST(posCoordsTranslation_test);

  return (UnityEnd());
}
