
#include "unity_fixture.h"

#include "printer.h"
#include "vector.h"
#include "logger.h"
#include "platform_base.h"
#include "labirynth.h"
#include "labirynths.h"

TEST_GROUP(printLabWithTwoRoutes_FUN);
TEST_SETUP(printLabWithTwoRoutes_FUN) { }
TEST_TEAR_DOWN(printLabWithTwoRoutes_FUN) { }

TEST(printLabWithTwoRoutes_FUN, When_Called_Then_ManuallyCheckIt)
{
    vector_t pathA, pathB;
    uint8_t memA[1000];
    uint8_t memB[1000];
    lab_cellxy_t a, b;

    labLoad(labirynths[LABIRYNTHS_A]);
    vector_init(&pathA, memA, sizeof(memA), sizeof(lab_cellxy_t));
    vector_init(&pathB, memB, sizeof(memB), sizeof(lab_cellxy_t));

    a.x = LAB_N / 2u;
    b.y = LAB_N / 2u;
    for (uint8_t i = 0; i < LAB_N; i++)
    {
        a.y = i;
        b.x = i;
        vector_add(&pathA, &a);
        vector_add(&pathB, &b);
    }

    printLabWithTwoRoutes(pathA, pathB);
}

TEST_GROUP_RUNNER(printLabWithTwoRoutes_FUN)
{
  RUN_TEST_CASE(printLabWithTwoRoutes_FUN, When_Called_Then_ManuallyCheckIt);
}

static void run_all_tests(void)
{
  RUN_TEST_GROUP(printLabWithTwoRoutes_FUN);
}

int main(int argc, const char **argv)
{
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
  return UnityMain(argc, argv, run_all_tests);
}