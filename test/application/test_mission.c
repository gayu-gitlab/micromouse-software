
#include "unity.h"
#include "unity_fixture.h"


#include "labirynth.h"
#include "labirynths.h"
#include "state.h"
#include "platform_state.h"
#include "config.h"
#include "mission.h"
#include "platform_base.h"
#include "executor.h"

/**
 * @brief This module is WIP.
 * TODO
 * firstly all lower level modules should be implemented
 * and well designed
 *
 */


TEST_GROUP(missionAchieveGoal);
TEST_SETUP(missionAchieveGoal)
{
  executorMockInit();
}
TEST_TEAR_DOWN(missionAchieveGoal)
{

}

TEST(missionAchieveGoal, When_DriveNorthFromStart_Then_EndProperly)
{
  // labLoad(labirynth_test_straight_NS_3);

  // missionInit();

  // ret_code_t ret = RET_AGAIN;
  // stateInitModule(configGet());
  // TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(stateGetCurrentState()));

  // while (ret == RET_AGAIN)
  // {
  //   log_debug("---------------------------------------\n\n");
  //   ret = missionAchieveGoal((lab_cellxy_t){0, 2});
  //   // getc(stdin);
  // }
  // TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  /*
  TODO
  this test is a great place to work on
  */
}

TEST(missionAchieveGoal, When_Initialized_Then_InitializeLowerLayers)
{
  // TEST_ASSERT_EQUAL(0, executorInitMockCalledTimes());
  missionInit();
  TEST_ASSERT_EQUAL(1, executorInitMockCalledTimes());
}

TEST_GROUP_RUNNER(missionAchieveGoal)
{
  // RUN_TEST_CASE(missionAchieveGoal, When_DriveNorthFromStart_Then_EndProperly);
  RUN_TEST_CASE(missionAchieveGoal, When_Initialized_Then_InitializeLowerLayers);
}

int main(void)
{
  UNITY_BEGIN();
  // platformInit();
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);

  RUN_TEST_GROUP(missionAchieveGoal);

  int failures = UnityEnd();
  platformShowTestResult(failures);

  return failures;
}