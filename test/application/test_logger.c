


#include "logger.h"
#include "unity.h"
#include "platform_base.h"


void setUp(void)
{
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
}

void tearDown(void)
{
}

void log_test(void)
{
  log_raw("test\n");
  log_debug("test");
  log_info("test");
  log_error("test");
}

int main(void)
{
  UNITY_BEGIN();
  RUN_TEST(log_test);


  return (UnityEnd());
}
