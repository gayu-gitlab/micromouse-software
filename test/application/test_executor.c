
#include <stdlib.h>
#include <time.h>

#include "executor.h"
#include "unity.h"
#include "config.h"
#include "state.h"
#include "useful.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void executorInit_test(void)
{
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
}

void executorGoToTarget_When_NullProvided_Then_ReturnFailure(void)
{
  point_t p = {0,0};
  state_t now;
  double time = 1;

  executorInit(configGet());
  stateInitialize(&now);
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, executorGoToTarget(p, now, time, NULL));
}

void executorGoToTarget_testBasic(void)
{
  point_t p = {0,0};
  state_t now;
  motors_speed_t mot;
  double time = 1;

  now.x = 0; now.y = 0; now.t = 0; now.v = 0; now.w = 0;
  executorInit(configGet());
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorGoToTarget(p, now, time, &mot));
  p.x = rand() % ONE_METER; p.y = rand() % ONE_METER;
  now.x = p.x;
  now.y = p.y;
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorGoToTarget(p, now, time, &mot));

  p.x = 1234*ONE_METER;
  TEST_ASSERT_EQUAL(RET_ERR_OUT_OF_BOUNDS, executorGoToTarget(p, now, time, &mot));

  p.x = METERS_TO_MILLIMETERS(1.34);
  TEST_ASSERT_EQUAL(RET_AGAIN, executorGoToTarget(p, now, time, &mot));
}

void executorGoToTarget_testAdvanced(void)
{
  point_t p;
  state_t now;
  motors_speed_t mot;
  double time = 1;

  // straight
  now.x = 0; now.y = 0; now.t = 0; now.v = 0; now.w = 0;
  p.x = 2*ONE_METER; p.y = 0;
  mot.right = 0; mot.left = 0;
  executorInit(configGet());
  TEST_ASSERT_EQUAL(RET_AGAIN, executorGoToTarget(p, now, time, &mot));
  TEST_ASSERT_EQUAL(mot.left, mot.right);

  // turn left
  now.x = ONE_METER; now.y = ONE_METER; now.t = M_PI; now.v = 0; now.w = 0;
  p.x = 0; p.y = 0;
  mot.right = 0; mot.left = 0;
  TEST_ASSERT_EQUAL(RET_AGAIN, executorGoToTarget(p, now, time, &mot));
  TEST_ASSERT(mot.left <= mot.right);

}

void executorGoToTargetAndStop_Given_PreviousTargetAchieved_When_NewTarget_Then_SetMotors(void)
{
  state_t state = stateStartingPoint();
  point_t first_aim = stateGetPoint(state);
  point_t second_aim = {.x = ONE_METER, .y = 2*ONE_METER};
  const double NOT_IMPORTANT_TIME = 0.01;
  motors_speed_t motors;
  ret_code_t ret;

  executorInit(configGet());

  // first aim is achieved
  ret = executorGoToTargetAndStop(first_aim, state, NOT_IMPORTANT_TIME, &motors);
  ret = executorGoToTargetAndStop(second_aim, state, NOT_IMPORTANT_TIME, &motors);

  TEST_ASSERT_EQUAL(RET_AGAIN, ret);
  TEST_ASSERT_NOT_EQUAL_DOUBLE(0, motors.left);
  TEST_ASSERT_NOT_EQUAL_DOUBLE(0, motors.right);
}


void executorGoToTargetAndStop_When_TargetAchieved_Then_StopMotorsAndReturnSuccess(void)
{
  state_t state = stateStartingPoint();
  point_t aim = stateGetPoint(state);
  const double NOT_IMPORTANT_TIME = 0.01;
  motors_speed_t motors;
  ret_code_t ret;

  executorInit(configGet());

  ret = executorGoToTargetAndStop(aim, state, NOT_IMPORTANT_TIME, &motors);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL_DOUBLE(0, motors.left);
  TEST_ASSERT_EQUAL_DOUBLE(0, motors.right);
}

int main(void)
{
  UNITY_BEGIN();
  srand(time(NULL));
  RUN_TEST(executorInit_test);
  RUN_TEST(executorGoToTarget_When_NullProvided_Then_ReturnFailure);
  RUN_TEST(executorGoToTarget_testBasic);
  RUN_TEST(executorGoToTarget_testAdvanced);
  RUN_TEST(executorGoToTargetAndStop_When_TargetAchieved_Then_StopMotorsAndReturnSuccess);
  RUN_TEST(executorGoToTargetAndStop_Given_PreviousTargetAchieved_When_NewTarget_Then_SetMotors);


  return (UnityEnd());
}

