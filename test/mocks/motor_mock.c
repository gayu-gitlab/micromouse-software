#include  "motor_mock.h"

static motors_ticks_t ticks = {0, 0};

void motorSetTicks(motors_ticks_t ticks_)
{
    ticks = ticks_;
}

ret_code_t motorGetTicks(motors_ticks_t *ticks_)
{
    *ticks_ = ticks;
    return RET_SUCCESS;
}

ret_code_t motorSetSpeed(motors_speed_t values)
{
    (void) values;
    return RET_SUCCESS;
}
