#ifndef LABIRYNTH_MOCK_H__
#define LABIRYNTH_MOCK_H__

#include "labirynth.h"

/**
 * @brief Change wall
 * 
 * @param[in] coords      coords
 * @param[in] walls       walls to set / clear
 * @param[in] exists      set / clear
 */
ret_code_t labWallChange(lab_cellxy_t coords, lab_cell_t cell, bool add);

#endif // LABIRYNTH_MOCK_H__