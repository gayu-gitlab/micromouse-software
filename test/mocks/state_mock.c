

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

#include "state.h"


point_t stateGetPoint(state_t state)
{
  (void) state;
  point_t p = {0, 0};
  return p;
}

// pos_t stateGetPosition(state_t state)
// {

// }

// void stateInitModule(const config_t *cfg)
// {

// }


ret_code_t stateStep(double time)
{
  (void)time;
  return RET_SUCCESS;
}

// ret_code_t stateNextModel(
//     double time,
//     state_t st_now,
//     state_t *st_next);

// ret_code_t stateNextEncoders(
//     double time,
//     state_t now,
//     state_t *next,
//     motors_ticks_t ticks);

// ret_code_t stateNextDistSensors(
//     const state_t prev,
//     state_t *next,
//     double *measurements);

// ret_code_t stateInitialize(state_t *st);

void stateDump(char* desc, state_t s)
{
 (void) desc;
 (void) s;
}

state_t stateGetCurrentState(void)
{
  state_t s = {0};
  return s;
}
