
#include "platform_printer.h"
#include "vector.h"
#include "point.h"
#include "labirynth.h"

ret_code_t printPathPlannerPoints(vector_t points)
{
  (void) points;
  return RET_SUCCESS;
}

ret_code_t printPathPlannerCells(vector_t lab_cells)
{
  (void) lab_cells;
  return RET_SUCCESS;
}
