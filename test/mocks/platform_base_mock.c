

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

#include "platform_base.h"


void platformInit(void)
{
 //nothing to do
}

void platformDelayMs(uint32_t ms)
{
  (void) ms;
}

void platformPutChar(const char c)
{
  (void) c;
  printf("%c", c);
}

// void platformSetLedState(uint8_t led_idx, bool on);

// bool platformCheckIfBatteryIsEmpty(void);

void platformShowTestResult(int failures)
{
  (void) failures;
}

// void platformStartTimingMeasurement(void);

// uint32_t platformStopTimingMeasurementUs(void);
