#include "distance_sensors.h"
#include "distance_sensors_hw_mock.h"
#include "distance_sensors_hw.h"
#include "useful.h"
#include "ret_codes.h"


static int length;
static double distances[10];

ret_code_t dsGetDistances(double* dists, int len)
{
    if(len != length)
    {
        return RET_ERR_INVALID_PARAM;
    }

    for(uint8_t idx = 0; idx < len; idx++)
    {
        dists[idx] = distances[idx];
    }

    return RET_SUCCESS;
}


void dsSetDistances(double* dists, int len)
{
    if(len > (int)ARRAY_SIZE(distances))
    {
        printf("ERROR\n");
        return;
    }
    length = len;

    for(uint8_t idx = 0; idx < len; idx++)
    {
        distances[idx] = dists[idx];
    }

}