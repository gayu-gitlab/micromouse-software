

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

#include "path_planner.h"

ret_code_t pp_find_path(lab_cellxy_t from, lab_cellxy_t to, vector_t *path)
{
  (void) from;
  (void) to;
  (void) path;
  return RET_ERR_NOT_IMPLEMENTED;
}

ret_code_t pp_plan_mapping(lab_cellxy_t pos, lab_cellxy_t *target, vector_t *path, vector_t *pathB)
{
  (void) pos;
  (void) target;
  (void) path;
  (void) pathB;
  return RET_ERR_NOT_IMPLEMENTED;
}

bool pp_goal_once_reached(void)
{
  return RET_ERR_NOT_IMPLEMENTED;
}

bool pp_is_mapping_still_needed(void)
{
  return RET_ERR_NOT_IMPLEMENTED;
}
