

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

#include "executor.h"

void executorMockInit()
{

}

uint32_t executorInitMockCalledTimes(void)
{
  return 1;
}

ret_code_t executorInit(const config_t *cfg_)
{
  (void) cfg_;
  return RET_ERR_NOT_IMPLEMENTED;
}

ret_code_t executorGoToTarget(point_t target, state_t state, double time, motors_speed_t *motors)
{
  (void) target;
  (void) state;
  (void) time;
  (void) motors;
  return RET_ERR_NOT_IMPLEMENTED;
}

ret_code_t executorGoToTargetAndStop(point_t aim, state_t state, double time, motors_speed_t *motors);

motors_speed_t executorSetRawVelocities(double raw_linear, double raw_angular);
