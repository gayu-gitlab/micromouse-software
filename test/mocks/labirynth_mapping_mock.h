
#include "labirynth_alter.h"


lab_cellxy_t labNoticeWall_mock_coords();
uint32_t labNoticeWall_mock_called_times();
lab_cell_t labNoticeWall_mock_wall();
bool labNoticeWall_mock_existence();