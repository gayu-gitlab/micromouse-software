
#include "labirynth_alter.h"




lab_cellxy_t _coords;
lab_cell_t _wall;
bool _existence;
uint32_t _times = 0;

void labNoticeWall_mock_init(void)
{
  _coords.x = 0;
  _coords.y = 0;
  _wall = 0;
  _existence = false;
  _times = 0;
}

ret_code_t labNoticeWall(lab_specific_wall_t wall, bool existence)
{
  _coords = wall.cell_xy;
  _wall = wall.direction;
  _existence = existence;
  _times++;
  return RET_SUCCESS;
}

lab_cellxy_t labNoticeWall_mock_coords()
{
  return _coords;
}
uint32_t labNoticeWall_mock_called_times()
{
  return _times;
}
lab_cell_t labNoticeWall_mock_wall()
{
  return _wall;
}

bool labNoticeWall_mock_existence()
{
  return _existence;
}
