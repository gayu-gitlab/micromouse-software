
Welcome to micromouse software.


# ABOUT

This is micromouse repo.
It is written for PC and embedded microcontroller.
For now, host part is under development. It uses simulator instead of hardware.

# HOW TO RUN IT

```bash
git submodule update --init --recursive
./build_all_ci.sh
```
Docker will do everything for you.

For developing more useful is to call
```bash
./builder/docker.sh
```
And then do everything in docker. See build_all.sh to analyze more.


# Techincal stuff
## Coordinate systems

Care was taken to use North-South-East-West (NSEW) notation.
Of course: N=top, S=bottom, E=right, W=left.

### Local / mouse / robot coordinate system

Point (0, 0) is in the middle between wheels. Axis x points to right wheel
and axis Y points the direction in which robot is going. Theta is counted
starting from OY, counter-clockwise.

### Global coordinate system

Point (0, 0) is the bottom (south) left (west) point of the stick
in the starting corner (most SW point of whole labirynth).
Rotation (heading) theta is counted starting from OX, counter-clockwise.
So the middle of the mouse at starting point should have
coords x = y = (LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL), and
robot heading is equal to PI/2 (robot is pointing to north).
Note: x, y are in <0, LAB_DIM_MAX>.

### Labirynth cells indices

There are also cells with their position in the maze. Again,
each cell has x and y index, and x, y are in [0, 1, 2, ..., LAB_N-2, LAB_N-1).
See cellxy - indexes x and y for a cell.

### Angles
Angles are in radians.
Heading of the robot in global coordinations is between (-M_PI, M_pi].
When robot is pointing as OX axis, heading  = 0 rad.
When robot is pointing as OY axis, heading  = M_PI/2 rad.

![alt text](doc/distances.jpg "Coordinates systems")

## Software architecture and testing
### Application unit tests
Use other source files (not mocks). These modules are simple enough to make such tests.

### Platform test
Basic tests checking that imlementation of platform specific setters/getters work properly.

### System tests
Test all layers of logic using specific platform.


![alt text](doc/software_diagram.png "Software architecture")

## CI
Jenkins. Configuration:
- Pipeline script from SCM
- do not allow concurent builds
- SCM git
    - URL git@gitlab.com:Bazastral/micromouse-software.git
    - credentials added specially to Jenkins and gitlab
    - branch specifier: */master
- Additional behaviors
    - Wipe out repository and force clone
- script path: Jenkinsfile


# LICENSE

MIT. See LICENSE file.
