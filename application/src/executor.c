
#include <stdio.h>
#include <math.h>

#include "executor.h"
#include "labirynth.h"
#include "pid.h"
#include "useful.h"
#include "config.h"
#include "position.h"


static const double DISTANCE_MARGIN = 10;
static const double MAX_SPEED_LINEAR = 800;
static const double MIN_SPEED_LINEAR = 10;
static const double HEADING_THRESHOLD = (5*M_PI)/180;
static const double DISTANCE_CLOSE_TO_TARGET = 100;
static const double TYPICAL_SPEED_LINEAR = 400;
static const double VELOCITY_STOP_MARGIN_LINEAR = 10;
static const double VELOCITY_STOP_MARGIN_ANGULAR = (5*M_PI)/180;

uint8_t memPidV[PID_CONTROLLER_STRUCT_SIZE_BYTES];
uint8_t memPidW[PID_CONTROLLER_STRUCT_SIZE_BYTES];
static pid_controller_t pidV = (pid_controller_t) memPidV;
static pid_controller_t pidW = (pid_controller_t) memPidW;
static const config_t *cfg;

ret_code_t executorInit(const config_t *cfg_)
{
  ret_code_t ret = RET_ERR_INVALID_PARAM;

  if(cfg_ != NULL)
  {
    cfg = cfg_;
    ret = pidInit(pidV, cfg->linearPIDp, cfg->linearPIDi, cfg->linearPIDd);
  }

  if(ret == RET_SUCCESS)
  {
    ret = pidInit(pidW, cfg->angularPIDp, cfg->angularPIDi, cfg->angularPIDd);
  }


  return ret;
}

ret_code_t executorGoToTarget(point_t aim, state_t state, double time, motors_speed_t *motors)
{
  double w, v;
  ret_code_t ret;

  double aimDist = pointGetDistance(stateGetPoint(state), aim);

  if(aimDist > LAB_DIM_MAX)
  {
    log_debug("EXE: distance too big: %f > %f", aimDist, LAB_DIM_MAX);
    return RET_ERR_OUT_OF_BOUNDS;
  }
  if(aimDist < DISTANCE_MARGIN)
  {
    log_debug("EXE: target achieved");
    return RET_SUCCESS;
  }
  if(motors == NULL)
  {
    return RET_ERR_INVALID_PARAM;
  }
  if(cfg == NULL)
  {
    return RET_ERR_NOT_INITED;
  }

  double heading = posGetHeadingToPoint(stateGetPosition(state), aim);
  double linear_velocity_mm_per_s = 0.0;

  double rotational_velocity = heading;

  if(fabs(heading) < HEADING_THRESHOLD)
  {
    if (aimDist < DISTANCE_CLOSE_TO_TARGET)
    {
      linear_velocity_mm_per_s = aimDist * 3;
    }
    else
    {
      linear_velocity_mm_per_s = TYPICAL_SPEED_LINEAR;
    }

    if(linear_velocity_mm_per_s > MAX_SPEED_LINEAR)
    {
      linear_velocity_mm_per_s = MAX_SPEED_LINEAR;
    }
    if (linear_velocity_mm_per_s < MIN_SPEED_LINEAR)
    {
      linear_velocity_mm_per_s = MIN_SPEED_LINEAR;
    }
    // log_debug("EXE: linear_velocity_mm_per_s %+1.3f", linear_velocity_mm_per_s);
  }
  //only rotation
  else
  {
    double sign = 1;
    if(heading< 0 ){sign = -1;}
    heading = fabs(heading);
    if(heading > deg2rad(180)){rotational_velocity = 4.0 * sign;}
    else{
      rotational_velocity = 2.0*sign;
    }
  }


  ret = pidStep(pidV, time, linear_velocity_mm_per_s - state.v, &v); CHECK_ERR(ret);
  ret = pidStep(pidW, time, rotational_velocity - state.w, &w); CHECK_ERR(ret);

  *motors = executorSetRawVelocities(v, w);

  return RET_AGAIN;;
}


ret_code_t executorGoToTargetAndStop(point_t aim, state_t state, double time, motors_speed_t *motors)
{
  ret_code_t ret = RET_SUCCESS;
  double w, v;

  ret = executorGoToTarget(aim, state, time, motors);

  if(ret == RET_SUCCESS)
  {
    if(
      (fabs(state.v) > VELOCITY_STOP_MARGIN_LINEAR) ||
      (fabs(state.w) > VELOCITY_STOP_MARGIN_ANGULAR)
    )
    {
      ret = pidStep(pidV, time, -state.v, &v); CHECK_ERR(ret);
      ret = pidStep(pidW, time, -state.w, &w); CHECK_ERR(ret);
      *motors = executorSetRawVelocities(v, w);
      ret = RET_AGAIN;
    }
    else
    {
      motors->left = 0;
      motors->right = 0;
      ret = RET_SUCCESS;
    }
  }

  return ret;
}

motors_speed_t executorSetRawVelocities(double raw_linear, double raw_angular)
{
  motors_speed_t motors;

  motors.left = raw_linear - raw_angular;
  motors.right = raw_linear + raw_angular;

  // log_debug("EXE: set v %-1.3f, w %-1.3f -> L %+1.3f R %+1.3f", raw_linear, raw_angular, motors.left, motors.right);

  return motors;
}