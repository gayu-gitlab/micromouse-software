
#include <string.h>
#include <stdbool.h>

#include "vector.h"

#define MAGIC "VECT"

bool vector_valid(vector_t vect)
{
  if(
      vect.data != NULL
      && vect.itemSize != 0
      && vect.itemTotalAvailable > 0
      && vect.itemCount <= vect.itemTotalAvailable
      && memcmp(&vect.magic, MAGIC, sizeof(vect.magic)) == 0
    )
  { return true; }
  return false;
}

ret_code_t vector_add(vector_t* vect, void* item)
{
  if(!vect){ return RET_ERR_INVALID_PARAM; }
  if(!item){ return RET_ERR_INVALID_PARAM; }
  if(vect->itemTotalAvailable == vect->itemCount) { return RET_ERR_NO_MEM; }
  if(!vector_valid(*vect)){ return RET_ERR_OTHER; }

  memcpy(&vect->data[vect->itemCount * vect->itemSize], item, vect->itemSize);
  vect->itemCount++;

  return RET_SUCCESS;
}

ret_code_t vector_init(vector_t* vect, void* mem, uint32_t memBytes, uint32_t itemBytes)
{
  if(!vect){ return RET_ERR_INVALID_PARAM; }
  if(!mem){ return RET_ERR_INVALID_PARAM; }
  if(memBytes < itemBytes){ return RET_ERR_INVALID_PARAM; }
  if(!itemBytes){ return RET_ERR_INVALID_PARAM; }

  vect->itemTotalAvailable = memBytes / itemBytes;
  vect->data = mem;
  vect->itemCount = 0;
  vect->itemSize = itemBytes;
  memcpy(&vect->magic, MAGIC, sizeof(vect->magic));

  if(!vector_valid(*vect)){ return RET_ERR_OTHER; }
  return RET_SUCCESS;
}

uint32_t vector_item_cnt(vector_t vect)
{
  if(!vector_valid(vect)){ return 0; }
  return vect.itemCount;
}

ret_code_t vector_del_idx(vector_t* vect, uint32_t idx)
{
  if(!vect){ return RET_ERR_INVALID_PARAM; }
  if(idx >= vect->itemCount) { return RET_ERR_OUT_OF_BOUNDS; }
  if(!vector_valid(*vect)){ return RET_ERR_OTHER; }

  for(uint32_t i = idx; i < vect->itemCount -1 ; i++)
  {
    memcpy(&vect->data[i * vect->itemSize],
      &vect->data[(i+1) * vect->itemSize],
      vect->itemSize);
  }
  vect->itemCount--;

  return RET_SUCCESS;
}

ret_code_t vector_at(vector_t vect, uint32_t idx, void* item)
{
  if(idx >= vect.itemCount) { return RET_ERR_OUT_OF_BOUNDS; }
  if(!vector_valid(vect)){ return RET_ERR_OTHER; }

  memcpy(item, &vect.data[idx*vect.itemSize], vect.itemSize);
  return RET_SUCCESS;
}

ret_code_t vector_clear(vector_t* vect)
{
  vect->itemCount = 0;
  if(!vector_valid(*vect)){ return RET_ERR_OTHER; }
  return RET_SUCCESS;
}

ret_code_t vector_get_item_idx(vector_t vect, uint32_t* idx, void* item)
{
  if(!idx){ return RET_ERR_INVALID_PARAM; }
  if(!item){ return RET_ERR_INVALID_PARAM; }
  if(!vector_valid(vect)){ return RET_ERR_INVALID_PARAM; }

  for(uint32_t i = 0; i < vector_item_cnt(vect); i++)
  {
    if(memcmp(item, &vect.data[i*vect.itemSize], vect.itemSize) == 0)
    {
      *idx = i;
      return RET_SUCCESS;
    }
  }
  return RET_ERR_NOT_FOUND;
}

ret_code_t vector_turnover(vector_t* vect)
{
  if(!vect) { return RET_ERR_INVALID_PARAM; }
  if(!vector_valid(*vect)){ return RET_ERR_INVALID_PARAM; }
  char buf[10];
  if(vect->itemSize > sizeof(buf)) { return RET_ERR_NO_MEM; }
  uint32_t cnt = vector_item_cnt(*vect);

  for(uint32_t i = 0; i < cnt/2; i++)
  {
    memcpy(buf, &vect->data[i*vect->itemSize], vect->itemSize);
    memcpy(&vect->data[i*vect->itemSize], &vect->data[(cnt-i-1)* vect->itemSize], vect->itemSize);
    memcpy(&vect->data[(cnt-i-1)*vect->itemSize], buf, vect->itemSize);
  }
  return RET_SUCCESS;
}

bool vector_are_equal(vector_t a, vector_t b)
{
  bool equal = false;

  if(vector_valid(a) && vector_valid(b))
  {
    if(vector_item_cnt(a) == vector_item_cnt(b))
    {
      if(memcmp(a.data, b.data, vector_item_cnt(a)) == 0u)
      {
        equal = true;
      }
    }
  }

  return equal;
}