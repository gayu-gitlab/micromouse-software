#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

#include "labirynth_utils.h"
#include "ret_codes.h"
#include "typedef.h"
#include "useful.h"

ret_code_t labCheckXy(lab_cellxy_t coords)
{
  if(coords.x >= LAB_N) return RET_ERR_INVALID_PARAM;
  if(coords.y >= LAB_N) return RET_ERR_INVALID_PARAM;
  return RET_SUCCESS;
}

ret_code_t labPoint2Cellxy(lab_cellxy_t* cellxy, point_t point)
{
  if(point.x < 0 || point.x > LAB_DIM_MAX) { return RET_ERR_INVALID_PARAM; }
  if(point.y < 0 || point.y > LAB_DIM_MAX) { return RET_ERR_INVALID_PARAM; }
  cellxy->x = (uint8_t)((double)(point.x - LAB_DIM_WALL_HALF)/(double)(LAB_DIM_CELL));
  cellxy->y = (uint8_t)((double)(point.y - LAB_DIM_WALL_HALF)/(double)(LAB_DIM_CELL));
  if(cellxy->x >= LAB_N) { cellxy->x = LAB_N-1u; }
  if(cellxy->y >= LAB_N) { cellxy->y = LAB_N-1u; }
  return RET_SUCCESS;
}

lab_cellxy_t labPoint2CellXY(point_t point)
{
  lab_cellxy_t cellxy = {0u, 0u};
  ret_code_t ret = labPoint2Cellxy(&cellxy, point);
  if(ret != RET_SUCCESS)
  {
    cellxy.x = 0;
    cellxy.y = 0;
  }
  return cellxy;
}

ret_code_t labGetMidCell(lab_cellxy_t cellxy, point_t* point)
{
  if(point == NULL) { return RET_ERR_INVALID_PARAM; }
  if(cellxy.x >= LAB_N) { return RET_ERR_INVALID_PARAM; }
  if(cellxy.y >= LAB_N) { return RET_ERR_INVALID_PARAM; }
  point->x = (cellxy.x * LAB_DIM_CELL) + LAB_DIM_WALL + LAB_DIM_CORRIDOR_HALF;
  point->y = (cellxy.y * LAB_DIM_CELL) + LAB_DIM_WALL + LAB_DIM_CORRIDOR_HALF;
  return RET_SUCCESS;
}

point_t labCellToPoint(lab_cellxy_t cellxy)
{
  point_t p = {0, 0};
  (void) labGetMidCell(cellxy, &p);
  return p;
}


bool labCoordsEqual(lab_cellxy_t a, lab_cellxy_t b)
{
  if((a.x == b.x) && (a.y == b.y))
  {
    return true;
  }
  return false;
}

lab_walls_coords_t labGetWallsCoordsFromCellXY(lab_cellxy_t cellxy)
{
  lab_walls_coords_t walls = {0};

  if(labCheckXy(cellxy) == RET_SUCCESS)
  {
    walls.N = (double)(cellxy.y + 1u) * LAB_DIM_CELL;
    walls.S = (double)cellxy.y * LAB_DIM_CELL + LAB_DIM_WALL;
    walls.E = (double)(cellxy.x + 1u) * LAB_DIM_CELL;
    walls.W = (double)cellxy.x * LAB_DIM_CELL + LAB_DIM_WALL;
  }

  return walls;
}

lab_cellxy_t labCoordsStartingPoint(void)
{
  return labCoordsCornerSW();
}
lab_cellxy_t labCoordsCornerNE(void)
{
  return (lab_cellxy_t){LAB_N - 1u, LAB_N - 1u};
}
lab_cellxy_t labCoordsCornerNW(void)
{
  return (lab_cellxy_t){0u, LAB_N - 1u};
}
lab_cellxy_t labCoordsCornerSE(void)
{
  return (lab_cellxy_t){LAB_N - 1u, 0u};
}
lab_cellxy_t labCoordsCornerSW(void)
{
  return (lab_cellxy_t){0u, 0u};
}
lab_cellxy_t labCoordsGoalNE(void)
{
  return (lab_cellxy_t){8u, 8u};
}
lab_cellxy_t labCoordsGoalNW(void)
{
  return (lab_cellxy_t){7u, 8u};
}
lab_cellxy_t labCoordsGoalSE(void)
{
  return (lab_cellxy_t){8u, 7u};
}
lab_cellxy_t labCoordsGoalSW(void)
{
  return (lab_cellxy_t){7u, 7u};
}

void labDumpSpecificWall(char* description, lab_specific_wall_t wall)
{
  char buf[100];
  char* ptr = buf;
  uint16_t len;

  len = snprintf(ptr, sizeof(buf) - (ptr - buf), "Wall %s: ", description); ptr += len; if(ptr > buf + sizeof(buf)){return;}

  len = snprintf(ptr, sizeof(buf) - (ptr - buf), " (%02d %02d) ", wall.cell_xy.x, wall.cell_xy.y); ptr += len; if(ptr > buf + sizeof(buf)){return;}

  if((wall.direction & LAB_NORTH) != 0)
  {
    len = snprintf(ptr, sizeof(buf) - (ptr - buf), "N ", wall.cell_xy.x, wall.cell_xy.y); ptr += len; if(ptr > buf + sizeof(buf)){return;}
  }
  if((wall.direction & LAB_SOUTH) != 0)
  {
    len = snprintf(ptr, sizeof(buf) - (ptr - buf), "S ", wall.cell_xy.x, wall.cell_xy.y); ptr += len; if(ptr > buf + sizeof(buf)){return;}
  }
  if((wall.direction & LAB_EAST) != 0)
  {
    len = snprintf(ptr, sizeof(buf) - (ptr - buf), "E ", wall.cell_xy.x, wall.cell_xy.y); ptr += len; if(ptr > buf + sizeof(buf)){return;}
  }
  if((wall.direction & LAB_WEST) != 0)
  {
    len = snprintf(ptr, sizeof(buf) - (ptr - buf), "W ", wall.cell_xy.x, wall.cell_xy.y); ptr += len; if(ptr > buf + sizeof(buf)){return;}
  }

  log_debug(buf);
}