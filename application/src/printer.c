
#include "printer.h"
#include "logger.h"
#include "labirynth.h"

#define LAB_DUMP_STICK             "o"
#define LAB_DUMP_WALL_HORI_1       "---"
#define LAB_DUMP_WALL_HORI_0       "   "
#define LAB_DUMP_WALL_HORI_UNKNOWN LOG_COL_GRAY" ? "LOG_COL_RESET
#define LAB_DUMP_WALL_VERT_1       "|"
#define LAB_DUMP_WALL_VERT_0       " "
#define LAB_DUMP_WALL_VERT_UNKNOWN LOG_COL_GRAY"?"LOG_COL_RESET
#define LAB_DUMP_PATH_A            LOG_COL_RED"x"LOG_COL_RESET
#define LAB_DUMP_PATH_B            LOG_COL_GREEN"x"LOG_COL_RESET
#define LAB_DUMP_PATH_A_BEGIN      LOG_COL_RED"B"LOG_COL_RESET
#define LAB_DUMP_PATH_B_BEGIN      LOG_COL_GREEN"B"LOG_COL_RESET
#define LAB_DUMP_PATH_A_END        LOG_COL_RED"E"LOG_COL_RESET
#define LAB_DUMP_PATH_B_END        LOG_COL_GREEN"E"LOG_COL_RESET

/**
 * @brief Dump one line of vertical walls
 * 
 * @param[in] y row to dump
 * @param[in] a vector to print in red
 * @param[in] b vector to print in green
 */
static void labDumpVertical(uint8_t y, vector_t a, vector_t b);

/**
 * @brief Dump one line of horizontal walls
 * 
 * @param[in] y     row to dump
 * @param[in] wall  choose North or South
 */
static void labDumpHorizontalNS(uint8_t y, uint8_t wall);

static void labDumpHorizontalNS(uint8_t y, uint8_t wall)
{
  lab_cellxy_t coor;
  coor.y = y;

  for ( uint8_t x = 0; x < LAB_N; x++)
  {
    log_raw(LAB_DUMP_STICK);
    coor.x = x;
    if(labWallsKnown(coor, wall))
    {

    if(labWallsExist(coor, wall))
    {
      log_raw(LAB_DUMP_WALL_HORI_1);
    }
    else
    {
      log_raw(LAB_DUMP_WALL_HORI_0);
    }
    }
    else
    {
      log_raw(LAB_DUMP_WALL_HORI_UNKNOWN);

    }
  }
  log_raw(LAB_DUMP_STICK);
  log_raw("\n");
}

static void labDumpVertical(uint8_t y, vector_t a, vector_t b)
{
  lab_cellxy_t coor;
  uint32_t idx;

  // first west wall
  coor.x = 0;
  coor.y = y;

  for (uint8_t x = 0; x < LAB_N; x++)
  {
    coor.x = x;
    if (labWallsKnown(coor, LAB_WEST))
    {
      if (labWallsExist(coor, LAB_WEST))
      {
        log_raw(LAB_DUMP_WALL_VERT_1);
      }
      else
      {
        log_raw(LAB_DUMP_WALL_VERT_0);
      }
    }
    else
    {
      log_raw(LAB_DUMP_WALL_VERT_UNKNOWN);
    }

    // print routes
    log_raw(" ");
    if(vector_get_item_idx(a, &idx, (void*) &coor) == RET_SUCCESS)
    {
      if(idx == 0u)
      {
        log_raw(LAB_DUMP_PATH_A_BEGIN);
      }
      else if(idx == vector_item_cnt(a) - 1u)
      {
      log_raw(LAB_DUMP_PATH_A_END);
      }
      else
      {
      log_raw(LAB_DUMP_PATH_A);
      }
    }
    else
    {
      log_raw(" ");
    }
    if(vector_get_item_idx(b, &idx, (void*) &coor) == RET_SUCCESS)
    {
      if(idx == 0u)
      {
        log_raw(LAB_DUMP_PATH_B_BEGIN);
      }
      else if(idx == vector_item_cnt(b) - 1u)
      {
      log_raw(LAB_DUMP_PATH_B_END);
      }
      else
      {
      log_raw(LAB_DUMP_PATH_B);
      }
    }
    else
    {
      log_raw(" ");
    }
  }

  if (labWallsExist(coor, LAB_EAST))
  {
    log_raw(LAB_DUMP_WALL_VERT_1);
  }
  else
  {
    log_raw(LAB_DUMP_WALL_VERT_0);
  }
  log_raw("\n");
}


void printLabWithTwoRoutes(vector_t pathA, vector_t pathB)
{
  log_raw("\n");

  for(uint8_t y = LAB_N-1; y <= LAB_N-1; y--)
  {
    labDumpHorizontalNS(y, LAB_NORTH);
    labDumpVertical(y, pathA, pathB);
  }
  labDumpHorizontalNS(0, LAB_SOUTH);
  log_raw("\n");
}

void printLabWithOneRoute(vector_t pathA)
{
  vector_t dummy;
  char mem[10];
  vector_init(&dummy, mem, sizeof(mem), sizeof(point_t));

  printLabWithTwoRoutes(pathA, dummy);
}