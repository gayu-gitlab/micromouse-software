

#include <math.h>
#include <string.h>

#include "useful.h"


double normalizeRad(double rad)
{
  while(rad > M_PI) { rad -= 2*M_PI; }
  while(rad <= -M_PI) { rad += 2*M_PI; }
  return rad;
}

double sign_double(double val)
{
  if(val < 0.0)
  {
    return -1;
  }

  return 1;
}

double deg2rad(double deg)
{
    return normalizeRad((deg * M_PI) / 180);
}

double rad2deg(double rad)
{
    return (rad * 180)/M_PI;
}

double ctg(double rad)
{
  double s = sin(rad);
  if(s != 0)
  {
    return cos(rad)/sin(rad);
  }
  return __DBL_MAX__;
}