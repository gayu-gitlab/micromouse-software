

#include <math.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "state.h"
#include "labirynth.h"
#include "labirynth_mapping.h"
#include "useful.h"
#include "distance_sensors.h"
#include "distance_sensors_hw.h"


#define MAX_LINEAR_VELOCITY 1700
#define MAX_ANGULAR_VELOCITY 27

static const config_t* cfg = NULL;
static bool inited = false;
static state_t state_now;

void stateInitModule(const config_t *cfg_)
{
  cfg = cfg_;
  inited = true;
  stateInitialize(&state_now);
}

ret_code_t stateNextModel(
    double time,
    state_t now,
    state_t *next)
{
  if(!inited) { log_debug("State module not inited"); return RET_ERR_NOT_INITED; }

  next->x = now.x + now.v * cos(now.t) * time;
  next->y = now.y + now.v * sin(now.t) * time;
  next->t = now.t + now.w * time;
  next->v = now.v;
  next->w = now.w;
  return RET_SUCCESS;
}

ret_code_t stateNextEncodersDeprecated(
    double time,
    state_t now,
    state_t *next,
    motors_ticks_t ticks)
{
  if(time == 0) return RET_ERR_INVALID_PARAM;
  if(!inited) { log_debug("State module not inited"); return RET_ERR_NOT_INITED; }
  /*
  This implementation is quite simple but also quite not accurate
  And I think it has some bugs in turning...
  */

  double avg_ticks = (ticks.left + ticks.right) / 2;
  //assume that robot drives forward - there is no curve
  next->x = now.x + cfg->whlCirc * avg_ticks * cos(now.t) / cfg->whlTicksPerTurn;
  next->y = now.y + cfg->whlCirc * avg_ticks * sin(now.t) / cfg->whlTicksPerTurn;
  double dt = ((ticks.right - ticks.left) * cfg->whlCirc / cfg->whlTrack) / cfg->whlTicksPerTurn;
  next->t = now.t + dt;
  next->v = (cfg->whlCirc * avg_ticks / time) / cfg->whlTicksPerTurn;
  next->w = dt / time;

  return RET_SUCCESS;
}

ret_code_t stateNextEncoders(
    double time,
    state_t now,
    state_t *next,
    motors_ticks_t ticks)
{
  /*
  Implementation based on https://www.cs.columbia.edu/~allen/F19/NOTES/icckinematics.pdf
  Dudek and Jenkin, Computational Principles of Mobile Robotics
  */
  if(time == 0) return RET_ERR_INVALID_PARAM;
  if(!inited) { log_debug("State module not inited"); return RET_ERR_NOT_INITED; }


  double distance_left = ticks.left * cfg->whlCirc / cfg->whlTicksPerTurn;
  double distance_right = ticks.right * cfg->whlCirc / cfg->whlTicksPerTurn;

  if(distance_left == distance_right)
  {
    // robot drives forward - there is no curve
    next->x = now.x + distance_left * cos(now.t);
    next->y = now.y + distance_left * sin(now.t);
    next->t = now.t;
    next->v = distance_left / time;
    next->w = 0;
  }
  else
  {
    double R = (cfg->whlTrack/2) * (distance_left+distance_right) / (distance_right-distance_left);
    double w = (distance_right-distance_left)/(cfg->whlTrack);
    point_t ICC;
    ICC.x = now.x - R*sin(now.t);
    ICC.y = now.y + R*cos(now.t);

    next->x = cos(w) * (now.x-ICC.x) - sin(w) * (now.y - ICC.y) + ICC.x;
    next->y = sin(w) * (now.x-ICC.x) + cos(w) * (now.y - ICC.y) + ICC.y;
    next->t = normalizeRad(now.t + w);
    next->v = ((distance_left+distance_right)/2) / time;
    next->w = w / time;
  }

  // stateDump("now", now);
  // stateDump("nex", *next);
  // log_debug("ticks %d %d, circ %f ", ticks.left, ticks.right, cfg->whlCirc);
  // log_debug("circ %f", 2*M_PI*cfg->whlRadius);
  // log_debug("distance l %f, r %f", distance_left, distance_right);

  return RET_SUCCESS;
}

ret_code_t stateInitialize(state_t *st)
{
  st->x = LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL;
  st->y = LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL;
  st->t = M_PI/2;
  st->v = 0;
  st->w = 0;
  return RET_SUCCESS;
}

state_t stateStartingPoint(void)
{
  state_t s;
  stateInitialize(&s);
  return s;
}

ret_code_t stateNextDistSensors(
    const state_t prev,
    state_t *next,
    double *measurements)
{
  ret_code_t ret = RET_SUCCESS;
  state_t next_states[MAX_DISTANCE_SENSORS_NUM] = {0};
  dsConfig_t* dsCfg = dsGetCfg();
  bool take_into_account[MAX_DISTANCE_SENSORS_NUM];

  memset(take_into_account, 0, sizeof(take_into_account));

  if(dsCfg->numberOfSensors == 0)
  {
    return RET_ERR_OTHER;
  }

  for (uint8_t i = 0u; i < dsCfg->numberOfSensors; i++)
  {
    if(measurements[i] < MIN_DISTANCE_SENSOR_RANGE_MM || measurements[i] > MAX_DISTANCE_SENSOR_RANGE_MM)
    {
      log_debug("skip sensor %d reading %f", i, measurements[i]);
      continue;
    }

    pos_t sensor_pos = posCoordsLocalToGlobal(dsCfg->sensors[i].offset, stateGetPosition(prev));
    pos_t sensor_pos_updated = sensor_pos;
    posDump("mouse ", stateGetPosition(prev));
    posDump("sensor", sensor_pos);
    lab_specific_wall_t wall;
    ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
    labDumpSpecificWall(" ", wall);
    if(ret != RET_SUCCESS)
    {
      continue;
    }

    lab_walls_coords_t walls_positions = labGetWallsCoordsFromCellXY(labPoint2CellXY(posGetPoint(sensor_pos)));

    double wall_axis_xy = walls_positions.E;
    if(wall.direction & LAB_WEST)
    {
      wall_axis_xy = walls_positions.W;
    }
    else if(wall.direction&LAB_NORTH)
    {
      wall_axis_xy = walls_positions.N;
    }
    else if(wall.direction&LAB_SOUTH)
    {
      wall_axis_xy = walls_positions.S;
    }

    next_states[i] = prev;

    if(measurements[i] > LAB_DIM_CORRIDOR*sqrt(2))
    {
      // only the closest wall can be used
      continue;
    }
    if(measurements[i] < LAB_DIM_WALL_HALF)
    {
      // assume that measurement cannot be too small
      continue;
    }

    if(labWallsExist(labPoint2CellXY(posGetPoint(sensor_pos)), wall.direction))
    {
      printf("walls exist!");
      next_states[i] = prev;
      if((wall.direction&LAB_WEST) || (wall.direction&LAB_EAST))
      {
        dsEstimatePositionOfOneSensor(
          sensor_pos,
          &sensor_pos_updated,
          measurements[i],
          wall_axis_xy);

          next_states[i].x = prev.x + (sensor_pos_updated.x - sensor_pos.x);
          next_states[i].t = prev.t + (sensor_pos_updated.t - sensor_pos.t);
      }
      else
      {
        dsEstimatePositionOfOneSensorY(
          sensor_pos,
          &sensor_pos_updated,
          measurements[i],
          wall_axis_xy);

          next_states[i].y = prev.y + (sensor_pos_updated.y - sensor_pos.y);
          next_states[i].t = prev.t + (sensor_pos_updated.t - sensor_pos.t);
      }
      take_into_account[i] = true;
      log_debug("take into account %d", i);
    }
    else
    {
      log_debug("Proper reading, but no wall?");
    }
  }

  *next = prev;
  uint8_t sensors_taken = 0;
  for (uint8_t i = 0u; i < dsCfg->numberOfSensors; i++)
  {
    if(take_into_account[i])
    {
      next->x += next_states[i].x;
      next->y += next_states[i].y;
      next->t += next_states[i].t;
      sensors_taken++;
    }
  }
  log_debug("sensors taken %d", sensors_taken);
  next->x /= (sensors_taken+1);
  next->y /= (sensors_taken+1);
  next->t /= (sensors_taken+1);

  return RET_SUCCESS;
}

point_t stateGetPoint(state_t state)
{
  return (point_t) {state.x, state.y};
}

pos_t stateGetPosition(state_t state)
{
  return (pos_t) {state.x, state.y, state.t};
}

void stateDump(char* desc, state_t s)
{
  char buf[100];
  char* ptr = buf;
  uint16_t len;
  char color_red[] = LOG_COL_RED;
  char color_normal[] = LOG_COL_RESET;
  char* color;

  len = snprintf(ptr, sizeof(buf) - (ptr - buf), "State %s: ", desc); ptr += len; if(ptr > buf + sizeof(buf)){return;}

  if(s.x < 0 || s.x > LAB_DIM_MAX) { color = color_red; }
  else { color = color_normal; }
  len = snprintf(ptr, sizeof(buf) - (ptr - buf), "%sx=%.3f ", color, s.x); ptr += len; if(ptr > buf + sizeof(buf)){return;}

  if(s.y < 0 || s.y > LAB_DIM_MAX) { color = color_red; }
  else { color = color_normal; }
  len = snprintf(ptr, sizeof(buf) - (ptr - buf), "%sy=%.3f ", color, s.y); ptr += len; if(ptr > buf + sizeof(buf)){return;}

  if(s.t < -M_PI || s.t > M_PI) { color = color_red; }
  else { color = color_normal; }
  len = snprintf(ptr, sizeof(buf) - (ptr - buf), "%st=%+2.2f (%+4.0f') ", color, s.t, rad2deg(s.t)); ptr += len; if(ptr > buf + sizeof(buf)){return;}

  if(s.v < -MAX_LINEAR_VELOCITY || s.v > MAX_LINEAR_VELOCITY) { color = color_red; }
  else { color = color_normal; }
  len = snprintf(ptr, sizeof(buf) - (ptr - buf), "%sv=%f ", color, s.v); ptr += len; if(ptr > buf + sizeof(buf)){return;}

  if(s.w < -MAX_ANGULAR_VELOCITY || s.w > MAX_ANGULAR_VELOCITY) { color = color_red; }
  else { color = color_normal; }
  len = snprintf(ptr, sizeof(buf) - (ptr - buf), "%sw=%+f ", color, s.w); ptr += len; if(ptr > buf + sizeof(buf)){return;}

  len = snprintf(ptr, sizeof(buf) - (ptr - buf), "%s ", color_normal); ptr += len; if(ptr > buf + sizeof(buf)){return;}
  log_debug(buf);
}

state_t stateGetCurrentState(void)
{
  return state_now;
}

ret_code_t stateStep(double time)
{
  ret_code_t ret = RET_SUCCESS;
  state_t state_next_model;
  state_t state_next_encoders;
  state_t state_next_ds;
  // state_t state_prev = state_now;
  motors_ticks_t ticks;
  double measurements[5];

  state_next_model = state_now;
  state_next_encoders = state_now;
  state_next_ds = state_now;

  ret = dsGetDistances(measurements, ARRAY_SIZE(measurements));

  ret = motorGetTicks(&ticks);

  ret = stateNextModel(time, state_now, &state_next_model);

  ret = stateNextEncoders(time, state_now, &state_next_encoders, ticks);

  ret = stateNextDistSensors(state_now, &state_next_ds, measurements);

  state_now.x = (state_next_model.x + 8*state_next_encoders.x + state_next_ds.x)/10;
  state_now.y = (state_next_model.y + 8*state_next_encoders.y + state_next_ds.y)/10;
  state_now.t = (state_next_model.t + state_next_encoders.t + state_next_ds.t)/3;
  state_now.w = (state_next_model.w + state_next_encoders.w + state_next_ds.w)/3;
  state_now.v = (state_next_model.v + state_next_encoders.v + state_next_ds.v)/3;

  // state_now.v = pointGetDistance(stateGetPoint(state_prev), stateGetPoint(state_now));

  stateDump("model", state_next_model);
  stateDump("encod", state_next_encoders);
  stateDump("ds   ", state_next_ds);
  stateDump("avg  ", state_now);

  return ret;
}

ret_code_t stateDetectWalls(
    const state_t current_state,
    double *measurements)
{

  ret_code_t ret = RET_SUCCESS;
  dsConfig_t *dsCfg = dsGetCfg();
  const double DETECTION_THRESHOLD = 10;
  const double SENSOR_REJECT_LOWER_THRESHOLD = 30;

  if(measurements == NULL)
  {
    return RET_ERR_INVALID_PARAM;
  }

  for(uint8_t i = 0u; i < dsCfg->numberOfSensors; i++)
  {
    double measurement = measurements[i];
    if (measurement < 0)
    {
      ret = RET_ERR_INVALID_PARAM;
      log_warn("Wall detection: invalid [%d] measurement = %f", i, measurement);
      continue;
    }

    if (measurement > SENSOR_REJECT_LOWER_THRESHOLD)
    {
      pos_t sensor_pos = posCoordsLocalToGlobal(dsCfg->sensors[0].offset, stateGetPosition(current_state));
      double measurement_if_wall_exists = 0;
      lab_specific_wall_t spec_wall;
      dsWhichClosestWallSensorShouldPoint(sensor_pos, &spec_wall, &measurement_if_wall_exists);

      bool exists = false;
      if (measurement < measurement_if_wall_exists + DETECTION_THRESHOLD)
      {
          exists = true;
      }
      labNoticeWall(spec_wall, exists);
    }
  }

  return ret;
}