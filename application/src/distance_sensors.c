
#include <math.h>
#include <stdbool.h>

#include "distance_sensors_hw.h"
#include "useful.h"
#include "ret_codes.h"


static dsConfig_t cfg = { 0 };

ret_code_t dsInit(const dsConfig_t dsCfg)
{
  cfg.numberOfSensors = dsCfg.numberOfSensors;
  if(cfg.numberOfSensors > MAX_DISTANCE_SENSORS_NUM)
  {
    cfg.numberOfSensors = MAX_DISTANCE_SENSORS_NUM;
    log_warn("too many of distance sensors");
  }

  for(uint8_t i = 0; i < cfg.numberOfSensors; i++)
  {
    cfg.sensors[i] = dsCfg.sensors[i];
  }

  return RET_SUCCESS;
}

dsConfig_t* dsGetCfg(void)
{
  return &cfg;
}

ret_code_t dsEstimatePositionOfOneSensor(
    pos_t sensor_pos,
    pos_t *sensor_pos_updated,
    double distance,
    double wall_x)
{
  /**
   * @brief explanation
   * Function updates all three components of the position of the ds (distance sensor): x, y, theta
   * Of course, algorithm has to update 3 variables based on only one measurement. It is not possible.
   * So it updates each one variable as if the two others were constant.
   *
   * Naming convention:
   * o - position where ray of the "laser" crosses wall.
   * @see doc/distance_sensors.drawio.png
   */
  double dx = wall_x - sensor_pos.x;
  double yop = sensor_pos.y + dx * tan(sensor_pos.t);
  double dm_sqared = distance*distance;
  double dy = sqrt(fabs(dm_sqared - dx*dx)) * sign_double(sin(sensor_pos.t));
  sensor_pos_updated->t = atan2(dy, dx);
  sensor_pos_updated->x = wall_x - sqrt(fabs(dm_sqared - pow(yop - sensor_pos.y, 2))) * sign_double(cos(sensor_pos.t));
  sensor_pos_updated->y = yop - sqrt(fabs(dm_sqared - pow(wall_x - sensor_pos.x, 2))) * sign_double(sin(sensor_pos.t));

  return RET_SUCCESS;
}

ret_code_t dsEstimatePositionOfOneSensorY(
    pos_t sensor_pos,
    pos_t *sensor_pos_updated,
    double distance,
    double wall_y)
{
  double dy = wall_y - sensor_pos.y;
  double dm_sqared = distance*distance;
  double dx = sqrt(fabs(dm_sqared - dy*dy)) * sign_double(cos(sensor_pos.t));
  double xop = sensor_pos.x + dy * ctg(sensor_pos.t);

  sensor_pos_updated->t = atan2(dy, dx);
  sensor_pos_updated->x = xop - sqrt(fabs(dm_sqared - pow(wall_y - sensor_pos.y, 2))) * sign_double(cos(sensor_pos.t));
  sensor_pos_updated->y = wall_y - sqrt(fabs(dm_sqared - pow(xop - sensor_pos.x, 2))) * sign_double(sin(sensor_pos.t));

  return RET_SUCCESS;
}

lab_cell_mask_t dsWhichWallSensorPoints(pos_t sensor_pos)
{
  double theta = normalizeRad(sensor_pos.t);
  lab_cellxy_t cellxy = labPoint2CellXY(posGetPoint(sensor_pos));
  lab_walls_coords_t walls = labGetWallsCoordsFromCellXY(cellxy);

  if ((sensor_pos.x <= walls.W) ||
      (sensor_pos.x >= walls.E) ||
      (sensor_pos.y <= walls.S) ||
      (sensor_pos.y >= walls.N))
  {
    return LAB_ALL_DIRS;
  }

  double thNW = atan2(walls.N - sensor_pos.y, walls.W - sensor_pos.x);
  double thNE = atan2(walls.N - sensor_pos.y, walls.E - sensor_pos.x);
  double thSE = atan2(walls.S - sensor_pos.y, walls.E - sensor_pos.x);
  double thSW = atan2(walls.S - sensor_pos.y, walls.W - sensor_pos.x);

  if (theta <= M_PI && theta > thNW)
  {
    return LAB_WEST;
  }
  else if (theta > thNE)
  {
    return LAB_NORTH;
  }
  else if (theta > thSE)
  {
    return LAB_EAST;
  }
  else if (theta > thSW)
  {
    return LAB_SOUTH;
  }

  return LAB_WEST;
}


ret_code_t dsWhichClosestWallSensorShouldPoint(
    pos_t sensor_pos,
    lab_specific_wall_t* wall,
    double *distance
)
{
  ret_code_t ret = RET_SUCCESS;

  double y = sensor_pos.y;
  lab_cellxy_t cellxy = labPoint2CellXY(posGetPoint(sensor_pos));
  lab_cell_mask_t stupid_wall = dsWhichWallSensorPoints(sensor_pos);

  if(stupid_wall == LAB_ALL_DIRS)
  {
    return RET_ERR_INVALID_PARAM;
  }

  if(distance != NULL)
  {
    if (stupid_wall & LAB_NORTH)
    {
      *distance = (((double)(cellxy.y + 1u) * LAB_DIM_CELL) - y) * sin(sensor_pos.t);
    }
    else if (stupid_wall & LAB_SOUTH)
    {
      *distance = (((double)(cellxy.y) * LAB_DIM_CELL) + LAB_DIM_WALL - y) * sin(sensor_pos.t);
    }
    else if (stupid_wall & LAB_EAST)
    {
      *distance = (((double)(cellxy.x + 1u) * LAB_DIM_CELL) - sensor_pos.x) * cos(sensor_pos.t);
    }
    else
    {
      *distance = (((double)(cellxy.x) * LAB_DIM_CELL) + LAB_DIM_WALL - sensor_pos.x) * cos(sensor_pos.t);
    }
  }

  if(wall != NULL)
  {
    wall->cell_xy = cellxy;
    wall->direction = stupid_wall;
  }

  return ret;
}

ret_code_t dsWhichWallSensorPointsWithDistance(
    pos_t sensor_pos,
    double max_distance,
    lab_specific_wall_t* wall,
    double *distance
)
{
  ret_code_t ret = RET_SUCCESS;

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, wall, distance);

  return ret;
}