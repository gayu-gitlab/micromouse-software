#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

#include "labirynth.h"
#include "labirynth_internal.h"
#include "labirynth_alter.h"
#include "ret_codes.h"
#include "typedef.h"
#include "useful.h"

#define LAB_STORE_NOTICED_WALLS_N 80

typedef struct
{
  lab_cellxy_t coords;
  lab_cell_t wall;
  bool exists;
} lab_notice_wall_t;

typedef struct
{
  lab_notice_wall_t notices[LAB_STORE_NOTICED_WALLS_N];
  uint8_t count;
} noticed_walls_t;


static noticed_walls_t noticedWalls;

/**
 * @brief Process walls that were noticed.
 * When one wall is noticed enough times, it will be added to known walls
 * and all its entries from noticedWalls will be erased
 */
static void labProcessNoticedWalls(lab_notice_wall_t newNotice);

/**
 * @brief Remove all notices which are same as a provided notice
 *
 * @param[in] notice remove all the same notices
 */
static void removeNoticedWalls(lab_notice_wall_t notice);

/**
 * @brief Remove one notice from buffer, by idx
 *
 * @param[in] idx index of the notice in buffer to remove
 */
static void removeOneNoticeByIdx(uint8_t idx);

void labMappingInit(void)
{
  memset(&noticedWalls, 0, sizeof(noticedWalls));
}

ret_code_t labNoticeWall(lab_specific_wall_t wall, bool existence)
{
  ret_code_t ret = RET_ERR_INVALID_PARAM;

  ret = labCheckXy(wall.cell_xy);

  if(ret == RET_SUCCESS)
  {
    if((wall.direction != LAB_NORTH) &&
    (wall.direction != LAB_SOUTH) &&
    (wall.direction != LAB_EAST) &&
    (wall.direction != LAB_WEST))
    {
      ret = RET_ERR_INVALID_PARAM;
    }
  }

  // already known walls cannot be changed
  if(ret == RET_SUCCESS)
  {
    if(labWallsKnown(wall.cell_xy, wall.direction))
    {
      if(labWallsExist(wall.cell_xy, wall.direction) != existence)
      {
        // strange. Wall should not be here...
        log_warn("LAB wall noticed differently than saved!");
        ret = RET_ERR_OTHER;
      }
    }
  }

  if(ret == RET_SUCCESS)
  {
    // add to noticed walls
    // unify walls
    if (wall.direction == LAB_SOUTH && wall.cell_xy.y != 0u)
    {
      wall.direction = LAB_NORTH;
      wall.cell_xy.y--;
    }
    if (wall.direction == LAB_WEST && wall.cell_xy.x != 0)
    {
      wall.direction = LAB_EAST;
      wall.cell_xy.x--;
    }
    labProcessNoticedWalls((lab_notice_wall_t){wall.cell_xy, wall.direction, existence});
  }

  return ret;
}

static void labProcessNoticedWalls(lab_notice_wall_t newNotice)
{
  ret_code_t ret;
  uint8_t sameNotices = 0u;
  bool save = false;
  // printf("Notice x %d y %d wall %04x, count %d", newNotice.coords.x, newNotice.coords.y, newNotice.wall, noticedWalls.count);

  // look only for the last entry, as it is the most recent
  // other entries (wall notices) did not change, so there is no point in checking them
  if (noticedWalls.count >= LAB_WALL_NOTICE_TO_SAVE - 1u)
  {
    for (uint8_t i = 0u; i < noticedWalls.count; i++)
    {
      if (
          labCoordsEqual(noticedWalls.notices[i].coords, newNotice.coords) &&
          noticedWalls.notices[i].wall == newNotice.wall)
      {
        if (noticedWalls.notices[i].exists == newNotice.exists)
        {
          sameNotices++;
        }
        else
        {
          log_warn("LAB noticed that wall has different state in two measurements...");
          // two different notices annul each other, so current notice will not be saved,
          // and one notice from past, which has different state of wall, will be deleted
          removeOneNoticeByIdx(i);
          return; //end processing
        }
      }
    }
    // add recent notice
    sameNotices++;
    if (sameNotices >= LAB_WALL_NOTICE_TO_SAVE)
    {
      // printf(" store ");
      ret = labWallChange(newNotice.coords, newNotice.wall, newNotice.exists);
      // remove all notices which are about this wall
      removeNoticedWalls(newNotice);
      if (ret != RET_SUCCESS)
      {
        log_error("LAB cannot save wall");
      }
    }
    else
    {
      save = true;
    }
  }
  else
  {
    save = true;
  }

  if (save)
  {
    if (noticedWalls.count >= ARRAY_SIZE(noticedWalls.notices))
    {
      log_warn("LAB buffer full, remove some old entries");
      for(uint8_t i = 0u; i < LAB_FREE_ELDEST_NOTICES; i++)
      {
        removeOneNoticeByIdx(0u);
      }
    }

      noticedWalls.notices[noticedWalls.count] = newNotice;
      noticedWalls.count++;
      // printf(" add ");
  }
  // printf("\n");
}

static void removeNoticedWalls(lab_notice_wall_t notice)
{
  // printf("\nremove x %d y %d wall %04x\n", notice.coords.x, notice.coords.y, notice.wall);
  // remove all occurences, starting from end
  for (int8_t i = noticedWalls.count - 1u; i >= 0; i--)
  {
    if (labCoordsEqual(noticedWalls.notices[i].coords, notice.coords) &&
        notice.wall == noticedWalls.notices[i].wall)
    {
      removeOneNoticeByIdx(i);
    }
  }
}

static void removeOneNoticeByIdx(uint8_t idx)
{
  // shift all other entries by one index, starting from end
  for (uint8_t j = idx; j < noticedWalls.count - 1u; j++)
  {
    noticedWalls.notices[j] = noticedWalls.notices[j + 1u];
  }
  noticedWalls.count--;
}
