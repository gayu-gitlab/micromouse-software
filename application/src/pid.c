
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "pid.h"

#define MAGIC_INITED_PID 0x91D0u

struct pid_controller
{
  double kp;
  double ki;
  double kd;
  double e_prev;
  double e_integral;
  double saturation_upper;
  double saturation_lower;
  uint32_t magic_inited;
};

static_assert(sizeof(struct pid_controller) == PID_CONTROLLER_STRUCT_SIZE_BYTES);


ret_code_t pidInit(pid_controller_t pid, double P, double I, double D)
{
  if(pid->magic_inited == MAGIC_INITED_PID)
  {
    log_warn("PID warning: pid was initialized earlier");
  }
  pid->e_prev = 0;
  pid->e_integral = 0;
  pid->magic_inited = MAGIC_INITED_PID;
  pid->kp = P;
  pid->ki = I;
  pid->kd = D;
  pid->saturation_lower = -1;
  pid->saturation_upper = +1;
  return RET_SUCCESS;
}

ret_code_t pidStep(pid_controller_t pid, double time, double error, double* output)
{
  double pid_output = 0;
  if(time == 0) return RET_ERR_INVALID_PARAM;
  if(pid->magic_inited != MAGIC_INITED_PID)
  {
    log_info("PID controller not properly initialized");
    return RET_ERR_NOT_INITED;
  }
  *output = 0;

  double e_deriviate = (error - pid->e_prev)/time;
  pid->e_integral += error*time;
  pid->e_integral = fmin(pid->e_integral, pid->saturation_upper/pid->ki);
  pid->e_integral = fmax(pid->e_integral, pid->saturation_lower/pid->ki);
  double part_p = pid->kp*error;
  double part_i = pid->ki*pid->e_integral;
  double part_d = pid->kd*e_deriviate;
  pid_output = part_p + part_i + part_d;
  if(pid_output > pid->saturation_upper || pid_output < pid->saturation_lower)
  {
    log_warn("PID warning: u (%3.3f) out of bounds <%3.3f, %3.3f>", pid_output,
        pid->saturation_lower, pid->saturation_upper);
  }
  pid_output = fmin(pid_output, pid->saturation_upper);
  pid_output = fmax(pid_output, pid->saturation_lower);
  pid->e_prev = error;

  // log_debug("PID e %+1.3f P %+1.3f I %+1.3f D %+1.3f u %+1.3f", error, part_p, part_i, part_d, pid_output);

  // log_debug("kp %f", pid->kp);
  // log_debug("ki %f", pid->ki);
  // log_debug("kd %f", pid->kd);
  // log_debug("e  %f", error);
  // log_debug("ei %f", pid->e_integral);
  // log_debug("ed %f", e_deriviate);
  // log_debug("u %f", pid_output);

  *output = pid_output;

  return RET_SUCCESS;
}
