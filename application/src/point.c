
#include <string.h>
#include <stdbool.h>

#include "point.h"
#include "logger.h"

void pointDump(const char* description, const point_t p)
{
  log_debug("Point %s: (%1.6f, %1.6f)", description, p.x, p.y);
}

point_t pointRotateAroundOrigin(point_t toRotate, double angleRad)
{
  point_t afterRotation = {
    toRotate.x * cos(angleRad) - toRotate.y * sin(angleRad),
    toRotate.x * sin(angleRad) + toRotate.y * cos(angleRad)
  };
  return afterRotation;
}

point_t pointRotateAroundPoint(point_t toRotate, point_t pointAbout, double angleRad)
{
  point_t normalized, rotated, ret;
  // normalize point to rotate it over (0, 0) and then come back
  normalized.x = toRotate.x - pointAbout.x;
  normalized.y = toRotate.y - pointAbout.y;
  rotated = pointRotateAroundOrigin(normalized, angleRad);
  ret.x = rotated.x + pointAbout.x;
  ret.y = rotated.y + pointAbout.y;
  return ret;
}

double pointGetDistance(point_t from, point_t to)
{
  return sqrt(
      pow(from.x - to.x, 2) +
      pow(from.y - to.y, 2)
      );
}

bool pointEqual(point_t pointA, point_t pointB)
{
  if ((pointA.x == pointB.x) &&
      (pointA.y == pointB.y))
  {
    return true;
  }
  return false;
}
