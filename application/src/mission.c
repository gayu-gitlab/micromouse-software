
#include <string.h>

#include "mission.h"
#include "labirynth.h"
#include "path_planner.h"
#include "platform_printer.h"
#include "state.h"
#include "printer.h"
#include "executor.h"
#include "motor.h"
#include "platform_base.h"
#include "platform_state.h"

static vector_t path;
static uint8_t mem[1000];

typedef enum{
    STATE_INIT,
    STATE_PLANNING,
    STATE_EXECUTING,
} MISSION_STATE_t;

static MISSION_STATE_t mission_state;

ret_code_t missionInit(void)
{
    mission_state = STATE_INIT;
    return RET_ERR_OTHER;
}

ret_code_t missionAchieveGoal(lab_cellxy_t goal)
{
    ret_code_t ret = RET_AGAIN;
    lab_cellxy_t from;

    memset(mem, 0, sizeof(mem));

    switch (mission_state)
    {
    case STATE_INIT:
    {
        ret = vector_init(&path, mem, sizeof(mem), sizeof(lab_cellxy_t));

        if (ret == RET_SUCCESS)
        {
            mission_state = STATE_PLANNING;
            ret = RET_AGAIN;
        }
        break;
    }

    case STATE_PLANNING:
    {
        from = labPoint2CellXY(stateGetPoint(stateGetCurrentState()));
        ret = pp_find_path(from, goal, &path);

        if (ret == RET_SUCCESS)
        {
            ret = printPathPlannerCells(path);
            printLabWithOneRoute(path);
        }

        if (ret == RET_SUCCESS)
        {
            ret = RET_AGAIN;
            mission_state = STATE_EXECUTING;
        }
        break;
    }

    case STATE_EXECUTING:
    {
        point_t goal_point = labCellToPoint(goal);
        double steps_seconds = 0.1;
        motors_speed_t motors;

        ret = executorGoToTarget(goal_point, stateGetCurrentState(), steps_seconds, &motors);
        motorSetSpeed(motors);

        stateStep(steps_seconds);

        statePlatformNotify(stateGetCurrentState());

        platformDelayMs(steps_seconds*1000);

        break;
    }

    default:
        ret = RET_ERR_OTHER;
    }

    log_debug("Mission to goal. State %d, Status %d", mission_state, ret);
    return ret;
}