#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

#include "labirynth.h"
#include "labirynth_internal.h"
#include "labirynth_alter.h"
#include "ret_codes.h"
#include "typedef.h"
#include "useful.h"

/**
 * Sets all maze to 0
 */
void labTotalReset();



ret_code_t labWallDel(lab_cellxy_t coords, lab_cell_t cell)
{
  return labWallChange(coords, cell, false);
}

ret_code_t labWallAdd(lab_cellxy_t coords, lab_cell_t cell)
{
  return labWallChange(coords, cell, true);
}

void wallAdd(lab_cellxy_t coords, lab_cell_t cell)
{
  lab[coords.x][coords.y] |= cell;
  labKnownWalls[coords.x][coords.y] |= cell;
  // printf("wallAdd %02d %02d %02x\n", coords.x, coords.y, cell);
}
void wallDel(lab_cellxy_t coords, lab_cell_t cell)
{
  lab[coords.x][coords.y] &= ~cell;
  labKnownWalls[coords.x][coords.y] |= cell;
  // printf("wallDel %02d %02d %02x\n", coords.x, coords.y, cell);
}

ret_code_t labWallChange(lab_cellxy_t coords, lab_cell_t cell, bool add)
{
  if(labCheckXy(coords)) return RET_ERR_INVALID_PARAM;
  if(!(cell & LAB_ALL_DIRS)) return RET_ERR_INVALID_PARAM;

  add ?
    wallAdd(coords, cell):
    wallDel(coords, cell);

  lab_cellxy_t neighbourgh;
  lab_cell_t neighbourgh_wall;
  if(cell & LAB_NORTH)
  {
    if(coords.y < LAB_N-1)
    {
      neighbourgh.x = coords.x; neighbourgh.y = coords.y+1; neighbourgh_wall = LAB_SOUTH;
      add ?
        wallAdd(neighbourgh, neighbourgh_wall):
        wallDel(neighbourgh, neighbourgh_wall);
    }
  }

  if(cell & LAB_SOUTH)
  {
    if(coords.y > 0)
    {
      neighbourgh.x = coords.x; neighbourgh.y = coords.y-1; neighbourgh_wall = LAB_NORTH;
      add ?
        wallAdd(neighbourgh, neighbourgh_wall):
        wallDel(neighbourgh, neighbourgh_wall);
    }
  }

  if(cell & LAB_EAST)
  {
    if(coords.x < LAB_N-1)
    {
      neighbourgh.x = coords.x+1; neighbourgh.y = coords.y; neighbourgh_wall = LAB_WEST;
      add ?
        wallAdd(neighbourgh, neighbourgh_wall):
        wallDel(neighbourgh, neighbourgh_wall);
    }
  }

  if(cell & LAB_WEST)
  {
    if(coords.x > 0)
    {
      neighbourgh.x = coords.x-1; neighbourgh.y = coords.y; neighbourgh_wall = LAB_EAST;
      add ?
        wallAdd(neighbourgh, neighbourgh_wall):
        wallDel(neighbourgh, neighbourgh_wall);
    }
  }

  return RET_SUCCESS;
}


ret_code_t labResetToDefault(void)
{
  lab_cellxy_t coords;
  ret_code_t ret;
  lab_cell_t cell;

  labTotalReset();

  //south border
  cell = LAB_SOUTH;
  coords.y = 0;
  for(coords.x = 1; coords.x < LAB_N-1; coords.x++)
  {
    ret = labWallAdd(coords, cell); CHECK_ERR(ret);
  }

  //north border
  cell = LAB_NORTH;
  coords.y = LAB_N-1;
  for(coords.x = 1; coords.x < LAB_N-1; coords.x++)
  {
    ret = labWallAdd(coords, cell); if(ret) return ret;
  }

  //east border
  cell = LAB_EAST;
  coords.x = LAB_N-1;
  for(coords.y = 1; coords.y < LAB_N-1; coords.y++)
  {
    ret = labWallAdd(coords, cell); if(ret) return ret;
  }

  //west border
  cell = LAB_WEST;
  coords.x = 0;
  for(coords.y = 1; coords.y < LAB_N-1; coords.y++)
  {
    ret = labWallAdd(coords, cell); if(ret) return ret;
  }

  //starting point
  coords.x = 0; coords.y = 0; cell = (LAB_SOUTH | LAB_WEST | LAB_EAST);
  ret = labWallAdd(coords, cell); if(ret) return ret;
  cell = LAB_NORTH;
  ret = labWallDel(coords, cell); if(ret) return ret; // store that this wall does not exist

  coords.x = LAB_N-1; coords.y = 0; cell = LAB_SOUTH | LAB_EAST;
  ret = labWallAdd(coords, cell); if(ret) return ret;

  coords.x = 0; coords.y = LAB_N-1; cell = LAB_NORTH | LAB_WEST;
  ret = labWallAdd(coords, cell); if(ret) return ret;

  coords.x = LAB_N-1; coords.y = LAB_N-1; cell = LAB_NORTH | LAB_EAST;
  ret = labWallAdd(coords, cell); if(ret) return ret;
  return RET_SUCCESS;
}

void labTotalReset()
{
  memset(lab, 0, sizeof(lab));
  memset(labKnownWalls, 0, sizeof(labKnownWalls));
}

ret_code_t labLoad(const labirynth_t load)
{
  memcpy(lab, load, sizeof(lab));

  for(uint8_t x = 0u; x < LAB_N; x++)
  {
    for(uint8_t y = 0u; y < LAB_N; y++)
    {
      labKnownWalls[x][y] = LAB_ALL_DIRS;
    }
  }
  return RET_SUCCESS;
}

