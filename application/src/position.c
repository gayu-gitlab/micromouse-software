
#include "position.h"
#include "logger.h"
#include "useful.h"


void posDump(const char* description, const pos_t pos)
{
  log_debug("Position %s: (%1.6f, %1.6f, %1.6f)", description, pos.x, pos.y, pos.t);
}

point_t posGetPoint(const pos_t pos)
{
  return (point_t) {pos.x, pos.y};
}

double posGetHeadingToPoint(pos_t from, point_t to)
{
  if((from.x == to.x) && (from.y == to.y))
  { return 0;}
  return normalizeRad(-from.t + atan2(to.y - from.y, to.x - from.x));
}

pos_t posRotateAroundPoint(pos_t toRotate, point_t pointAround, double angleRad)
{
  point_t rotated = pointRotateAroundPoint(posGetPoint(toRotate), pointAround, angleRad);
  return (pos_t){rotated.x, rotated.y, normalizeRad(angleRad + toRotate.t)};
}

pos_t posCoordsLocalToGlobal(
    pos_t local,
    pos_t mouseState)
{
  pos_t tmp = local;

  tmp.x = local.x + mouseState.x;
  tmp.y = local.y + mouseState.y;
  tmp = posRotateAroundPoint(tmp, posGetPoint(mouseState), mouseState.t - M_PI/2);
  tmp.t += M_PI/2;
  tmp.t = normalizeRad(tmp.t);
  return tmp;
}

pos_t posCoordsGlobalToLocal(
    pos_t global,
    pos_t mouseState)
{
  point_t senMouse = {0};
  point_t senLoc = {0};
  pos_t local = {0};

  senMouse.x = global.x - mouseState.x;
  senMouse.y = global.y - mouseState.y;
  senLoc = pointRotateAroundOrigin(senMouse, -mouseState.t + M_PI/2);
  local.x = senLoc.x;
  local.y = senLoc.y;
  local.t = normalizeRad(global.t - mouseState.t);
  return local;
}
