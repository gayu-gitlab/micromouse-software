#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

#include "labirynth.h"
#include "labirynth_internal.h"
#include "ret_codes.h"
#include "typedef.h"
#include "useful.h"


labirynth_t lab;
labirynth_t labKnownWalls;

/**
 * @brief Load cell info from given coordinations
 * @param[in] coords  cords
 * @param[out] cell   pointer to cell struct
 *
 * @returns success when valid coords and not null cell
 */
STATIC ret_code_t labGetCell(lab_cellxy_t coords, lab_cell_t *cell);

STATIC ret_code_t labGetCell(lab_cellxy_t coords, lab_cell_t *cell)
{
  if(labCheckXy(coords)) return RET_ERR_INVALID_PARAM;
  *cell = lab[coords.x][coords.y];
  return RET_SUCCESS;
}

ret_code_t labForEachNeighbor(lab_cellxy_t coords, neighbor_cb cb)
{
  lab_cell_t cell, neigh;
  lab_cellxy_t neigh_coords;

  if(!cb) { return RET_ERR_INVALID_PARAM; }

  CHECK_ERR(labGetCell(coords, &cell));


  if(!(cell & LAB_NORTH))
  {
    neigh_coords.x = coords.x;
    neigh_coords.y = coords.y + 1;
    if(RET_SUCCESS == labGetCell(neigh_coords, &neigh))
    { cb(coords, neigh_coords, neigh); }
  }

  if(!(cell & LAB_SOUTH))
  {
    neigh_coords.x = coords.x;
    neigh_coords.y = coords.y - 1;
    if(RET_SUCCESS == labGetCell(neigh_coords, &neigh))
    { cb(coords, neigh_coords, neigh); }
  }

  if(!(cell & LAB_EAST))
  {
    neigh_coords.x = coords.x + 1;
    neigh_coords.y = coords.y;
    if(RET_SUCCESS == labGetCell(neigh_coords, &neigh))
    { cb(coords, neigh_coords, neigh); }
  }

  if(!(cell & LAB_WEST))
  {
    neigh_coords.x = coords.x - 1;
    neigh_coords.y = coords.y;
    if(RET_SUCCESS == labGetCell(neigh_coords, &neigh))
    { cb(coords, neigh_coords, neigh); }
  }

  return RET_SUCCESS;
}


bool labWallsKnown(lab_cellxy_t cellxy, lab_cell_t walls)
{
  ret_code_t ret = labCheckXy(cellxy);
  lab_cell_t wallsKnown;

  if(ret == RET_SUCCESS)
  {
    wallsKnown = labKnownWalls[cellxy.x][cellxy.y];
    if((wallsKnown & walls) == walls)
    {
      return true;
    }
  }
  return false;
}

bool labWallsExist(lab_cellxy_t coords, lab_cell_t walls)
{
  ret_code_t ret = labCheckXy(coords);
  lab_cell_t wallsSet;

  if(ret == RET_SUCCESS)
  {
    wallsSet = lab[coords.x][coords.y];
    if((wallsSet & walls) == walls)
    {
      return true;
    }
  }
  return false;
}
