

#include <stdarg.h>
#include <stdio.h>
#include <stdbool.h>

#include "logger.h"

#define LOGGER_INCLUDE_LEVEL_IN_LOGS 0

static put_char_fun putcFun;
static bool inited = false;
static LOGGER_LVL_t level = LOGGER_LVL_ERROR;

void log_initialize(put_char_fun fun, LOGGER_LVL_t lvl)
{
  if(inited)
  {
    log_warn("logger initialized twice!");
  }
  putcFun = fun;
  inited = true;
  level = lvl;
  log_raw("logger initialized\n");
}

void log_set_verbosity_level(LOGGER_LVL_t lvl)
{
  level = lvl;
}

static void log_raw_internal(const char *format, va_list args)
{
  char buf[150];
  bool truncated = false;
  unsigned int len_to_print = 0;

  if (!inited)
  {
    return;
  }

  len_to_print = vsnprintf(buf, sizeof(buf), format, args);
  if (len_to_print > 0)
  {
    if(len_to_print >= sizeof(buf))
    {
      truncated = true;
      len_to_print = sizeof(buf) - 1;
    }

    for (unsigned int i = 0; i < len_to_print; ++i)
    {
      if(buf[i] == 0)
      {
        break;
      }
      else
      {
        putcFun(buf[i]);
      }
    }

    if(truncated)
    {
      putcFun('?');
      putcFun('?');
      putcFun('?');
      putcFun('\r');
      putcFun('\n');
    }
  }
}

void log_internal(const char *tag, const char *format, va_list args)
{
#if LOGGER_INCLUDE_LEVEL_IN_LOGS == 1
  log_raw("[%s] ", tag);
#else
  (void)tag;
#endif
  log_raw_internal(format, args);
  log_raw("\n");
}

void log_raw(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  log_raw_internal(format, args);
  va_end(args);
}

void log_error(const char *format, ...)
{
  if (level >= LOGGER_LVL_ERROR)
  {
    log_raw(LOG_COL_RED);
    va_list args;
    va_start(args, format);
    log_internal("err!", format, args);
    va_end(args);
    log_raw(LOG_COL_RESET);
  }
}

void log_warn(const char *format, ...)
{
  if (level >= LOGGER_LVL_WARN)
  {
    log_raw(LOG_COL_YELLOW);
    va_list args;
    va_start(args, format);
    log_internal("warn", format, args);
    va_end(args);
    log_raw(LOG_COL_RESET);
  }
}

void log_info(const char *format, ...)
{
  if (level >= LOGGER_LVL_INFO)
  {
    va_list args;
    va_start(args, format);
    log_internal("info", format, args);
    va_end(args);
  }
}

void log_debug(const char *format, ...)
{
  if (level >= LOGGER_LVL_DEBUG)
  {
    va_list args;
    va_start(args, format);
    log_internal("dbg ", format, args);
    va_end(args);
  }
}
