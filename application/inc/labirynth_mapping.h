#pragma once

/**
 * @brief This is API for notifying new walls in labirynth
 * This module will then decide if and when to add them to known labirynth
 */

#include <stdbool.h>

#include "ret_codes.h"
#include "point.h"
#include "labirynth_utils.h"

// how many times wall must be noticed to acknowledge that it is there
#define LAB_WALL_NOTICE_TO_SAVE 3u
// how many eldest notices must be removed from buffer, when
// there is no space for new notices
#define LAB_FREE_ELDEST_NOTICES (LAB_WALL_NOTICE_TO_SAVE * 3u)

/**
 * @brief Notice existence of wall (or absence)
 * After LAB_WALL_NOTICE_TO_SAVE times specific wall is noticed,
 * it will be saved to map
 *
 * @param[in] coords      coords
 * @param[in] wall        wall noticed (only one direction can be set to 1)
 */
ret_code_t labNoticeWall(lab_specific_wall_t wall, bool existence);

/**
 * @brief initialize mapping module
 */
void labMappingInit(void);
