#pragma once

/**
 * @brief This is API for altering (modifying) existing labirynth
 */

#include <stdbool.h>

#include "ret_codes.h"
#include "point.h"
#include "labirynth_utils.h"

/**
 * Prepare initial labirynth - set only walls which are always set
 * So: all the edges plus East wall of the start point
 */
ret_code_t labResetToDefault(void);

/**
 * @brief remove ALL walls from labirynth
 *
 * @return ret_code_t
 */
void labTotalReset(void);

/**
 * Load the labirynth
 */
ret_code_t labLoad(const labirynth_t lab);

/**
 * Explicitly add or remove wall(s)
 * @param cell - when wall bit is SET, it will be added or removed from labirynth
 */
ret_code_t labWallAdd(lab_cellxy_t coords, lab_cell_t cell);
ret_code_t labWallDel(lab_cellxy_t coords, lab_cell_t cell);
ret_code_t labWallChange(lab_cellxy_t coords, lab_cell_t cell, bool add);