
#ifndef POINTS_H__
#define POINTS_H__

#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>

#include "ret_codes.h"


struct point_struct{
  double x;
  double y;
};
typedef struct point_struct point_t;

/**
 * Print value of the point
 */
void pointDump(const char* description, const point_t p);

/*
 * Rotates point around (0, 0), CCW
 *
 * Example:
 * point(1, 2) after rotation by 90' (M_PI/2) gives (-2, 1)
 */
point_t pointRotateAroundOrigin(point_t toRotate, double angleRad);

/*
 * Rotates point around another point, CCW
 *
 * Example:
 * point(1, 2) after rotation by 90' (M_PI/2 rad) over (0, 0) gives (-2, 1)
 * point(1, 2) after rotation by 180' (M_PI rad) over (1, 1) gives (1, 0)
 */
point_t pointRotateAroundPoint(point_t toRotate, point_t pointAround, double angleRad);

/*
 * Calculates distance between two points
 */
double pointGetDistance(point_t pointA, point_t pointB);

/**
 * @brief Returns true if the points are equal
 */
bool pointEqual(point_t pointA, point_t pointB);


#endif  //POINTS_H__
