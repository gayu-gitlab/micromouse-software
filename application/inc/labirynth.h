#ifndef LABIRYNTH_H__
#define LABIRYNTH_H__

#include <stdbool.h>

#include "ret_codes.h"
#include "point.h"
#include "labirynth_utils.h"

// how many times wall must be noticed to acknowledge that it is there
#define LAB_WALL_NOTICE_TO_SAVE 3u
// how many eldest notices must be removed from buffer, when
// there is no space for new notices
#define LAB_FREE_ELDEST_NOTICES (LAB_WALL_NOTICE_TO_SAVE * 3u)



typedef void (*neighbor_cb)(lab_cellxy_t ref, lab_cellxy_t neighbor, lab_cell_t neigh_cell);

/**
 * Execute function for each neighbor which is connected without wall
 */
ret_code_t labForEachNeighbor(lab_cellxy_t coords, neighbor_cb cb);

/**
 * @brief Check if specific walls in specific cell are known
 *
 * @param[in] cellxy      coords
 * @param[in] cell        walls which are checked
 *
 * @returns true if existence of all provided walls is known
 */
bool labWallsKnown(lab_cellxy_t cellxy, lab_cell_t walls);

/**
 * @brief Check that cell has specific walls
 *
 * @param[in] coords  coords
 * @param[in] walls   wall to check
 * @return true       if all provided walls are set
 * @return false      otherwise
 */
bool labWallsExist(lab_cellxy_t coords, lab_cell_t walls);


#endif // LABIRYNTH_H__
