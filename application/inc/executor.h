#pragma once

#include "ret_codes.h"
#include "state.h"
#include "point.h"


ret_code_t executorInit(const config_t *cfg_);


/**
 * @brief Allows robot to drive smoothly to next point.
 * Uses PID. Returns success as soon as the target point (with some margin) is achieved
 *
 * Returns
 * RET_SUCCESS on success
 * RET_AGAIN - should be repeated
 * other on failure
 */
ret_code_t executorGoToTarget(point_t target, state_t state, double time, motors_speed_t *motors);

/**
 * @brief Similar to executorGoToTarget, but when the target is achieved, it slows down robot
 * (using PID in order not to slip) to zero veloctiy, linear and angular
 */
ret_code_t executorGoToTargetAndStop(point_t aim, state_t state, double time, motors_speed_t *motors);

/**
 * @brief Set raw linear and angular velocities of the robot
 * The min and max values are defined because of the raw <-1, 1> values of motors.
 * So:
 * 1. If linear velocity is equal to 0 (only turning)
 * then angular velocity is in <-radius/track, +radius/track>
 * Example: radius = 0.01m, track = 0.1m, raw angular velocity must be within <-0.1, 0.1>
 *
 * 2. If angular velocity is equal to 0 (going straight)
 * then linear velocity is in <-radius, +radius>
 * Example: radius = 0.01m, raw linear velocity must be within <-0.01, 0.01>
 *
 * @param linear  raw value
 * @param angular raw value
 * @returns raw motor speeds
 */
motors_speed_t executorSetRawVelocities(double raw_linear, double raw_angular);
