#ifndef __POSITION_H__
#define __POSITION_H__


#include "point.h"

struct position_struct{
  double x;
  double y;
  double t;
};
typedef struct position_struct pos_t;



/**
 * Print value of the position
 */
void posDump(const char* description, const pos_t pos);


/**
 * Get point from position.
 * It only loses theta information
 */
point_t posGetPoint(const pos_t pos);

/*
 * Calculates angle in radians from position to given point.
 *
 * Example 1:
 * Robot is at point (1, 1) and it's theta is M_PI/2 (points to North).
 * Wants to go to point (2, 1). This function will return -M_PI/2 (must turn
 * right 90 degrees to look at the destination point)
 *
 * Example 2:
 * Robot is at point (0, 0) and it's theta is M_PI (points to West).
 * Wants to go to point (1, -1). This function will return 3*M_PI/4 (must turn
 * left 135 degrees to look at the destination point)
 *
 * see: tests
 */
double posGetHeadingToPoint(pos_t from, point_t to);

/*
 * Rotates position around a given point, CCW
 *
 * Example:
 * pos(1, 2, M_PI/2) after rotation by 180' (M_PI rad) over (1, 1) gives (1, 0, -M_PI/2)
 */
pos_t posRotateAroundPoint(pos_t toRotate, point_t pointAround, double angleRad);

/**
 * Converts position in local coordinate system
 * to global corrdinate system
 */
pos_t posCoordsLocalToGlobal(
    pos_t local,
    pos_t mouseState);

/**
 * Converts global position
 * to local corrdination system
 */
pos_t posCoordsGlobalToLocal(
    pos_t global,
    pos_t mouseState);


#endif // __POSITION_H__
