#pragma once

#include "ret_codes.h"
#include "labirynth.h"

/**
 * @brief Initialize module
 *
 */
ret_code_t missionInit(void);

/**
 * @brief Execute mission - go to goal
 * 1. Find path to goal
 * 2. Go this path
 *
 * @param goal cell to achieve
 * @returns RET_AGAIN to call it again
 * @returns RET_SUCCESS when finished
 * @returns other on failure
 */
ret_code_t missionAchieveGoal(lab_cellxy_t goal);
