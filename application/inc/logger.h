#pragma once


#define LOG_COL_BLACK    "\033[0;30m"
#define LOG_COL_RED      "\033[0;31m"
#define LOG_COL_GREEN    "\033[0;32m"
#define LOG_COL_YELLOW   "\033[0;33m"
#define LOG_COL_BLUE     "\033[0;34m"
#define LOG_COL_PURPLE   "\033[0;35m"
#define LOG_COL_CYAN     "\033[0;36m"
#define LOG_COL_WHITE    "\033[0;37m"
#define LOG_COL_GRAY     "\033[0;2m"
#define LOG_COL_RESET    "\033[0m"


typedef void (*put_char_fun)(const char c);

typedef enum
{
    LOGGER_LVL_ERROR,
    LOGGER_LVL_WARN,
    LOGGER_LVL_INFO,
    LOGGER_LVL_DEBUG,
} LOGGER_LVL_t;

void log_initialize(put_char_fun fun, LOGGER_LVL_t lvl);
void log_set_verbosity_level(LOGGER_LVL_t lvl);

void log_error(const char* format, ...);
void log_warn(const char* format, ...);
void log_info(const char* format, ...);
void log_debug(const char* format, ...);
void log_raw(const char* format, ...);
