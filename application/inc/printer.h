#ifndef PRINTER_H__
#define PRINTER_H__

#include "vector.h"

/**
 * @brief Print with logger whole map with two routes on it
 *
 * @param[in] pathA vector of lab_cellxy_t
 * @param[in] pathB vector of lab_cellxy_t
 */
void printLabWithTwoRoutes(vector_t pathA, vector_t pathB);

/**
 * @brief Print with logger whole map with two routes on it
 *
 * @param[in] pathA vector of lab_cellxy_t
 */
void printLabWithOneRoute(vector_t pathA);

#endif // PRINTER_H__