#ifndef PATH_PLANNER_H__
#define PATH_PLANNER_H__

#include "useful.h"
#include "ret_codes.h"
#include "labirynth.h"
#include "point.h"
#include "vector.h"

struct pp_cell_text{
  lab_cellxy_t xy;
  const char* text;
};
typedef struct pp_cell_text pp_cell_text_t;

void pp_init(void);

/**
 * @brief Find path between two points (coords)
 * Algorithm uses A*.
 * For now it looks for the shortest route, which does not have to be
 * the fastest one.
 * The path is returned even it the walls are not known - assuming that not known walls do not exist.
 * In such case, RET_MAPPING_NEEDED is returned.
 *
 * @param[in] from    starting point
 * @param[in] to      goal point
 * @param[out] path   vector to store path
 *
 * @return ret_code_t
 * @returns RET_SUCCESS        path found, whole path is known
 * @returns RET_MAPPING_NEEDED path found, but more mapping is needed
 * @returns else               failure
 */
ret_code_t pp_find_path(lab_cellxy_t from, lab_cellxy_t to, vector_t* path);

/**
 * @brief Plan what is the current goal for the robot
 * Used during mapping. Firstly @ref pp_find_path should be used to achieve goal.
 * Then this function should be called to find better solution.
 *
 * @param[in]  pos    current position of robot
 * @param[out] target next target for robot to map
 * @param[in]  path   vector for internal use
 * @param[in]  pathB  vector for internal use
 *
 * @returns RET_SUCCESS if best solution found
 * @returns RET_MAPPING_NEEDED if best solution is not found yet
 * @returns other for failures
 */
ret_code_t pp_plan_mapping(lab_cellxy_t pos, lab_cellxy_t* target, vector_t *path, vector_t *pathB);


#endif // PATH_PLANNER_H__
