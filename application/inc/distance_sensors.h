#pragma once


/**
 * DS = DISTANCE SENSOR
 */

#include "ret_codes.h"
#include "point.h"
#include "vector.h"
#include "state.h"
#include "labirynth_utils.h"

#define MAX_DISTANCE_SENSORS_NUM 5
#define MAX_DISTANCE_SENSOR_RANGE_MM 100
#define MIN_DISTANCE_SENSOR_RANGE_MM 20

typedef struct dsCfgOne{
  pos_t offset;
    // x: offset [m] from middle of the micromouse
    // y: offset [m] from middle of the micromouse
    // t: angle [rad] between sensor's heading and robot's heading CCW.
  // example: 0 - front sensor, pointing stright ahead
  // example: PI/2 - left sensor, pointing perpendicular to left wall
} dsCfgOne_t;

typedef struct dsConfig{
  dsCfgOne_t sensors[MAX_DISTANCE_SENSORS_NUM];
  int numberOfSensors;
} dsConfig_t;

/**
 * Initialize sensors
 *
 * @param dsCfg configuration of sensors
 */
ret_code_t dsInit(const dsConfig_t dsCfg);

/**
 * @brief Getter for config
*/
dsConfig_t* dsGetCfg(void);

/**
 * @brief Calculate distance that ds should return given
 * - position of the ds
 * - max_distance
 * - state of labirynth
 *
 * @param[in] sensor_pos    global position of the sensor
 * @param[in] max_distance  maximal distance of the sensor. Used to optimize function.
 * @param[out] wall         wall that ds is pointing
 * @param[out] distance     distance that the sensor should return
 *
 * @returns RET_SUCCESS for success
 *
 * @see dsWhichClosestWallSensorShouldPoint
 */
ret_code_t dsWhichWallSensorPointsWithDistance(
    pos_t sensor_pos,
    double max_distance,
    lab_specific_wall_t* wall,
    double *distance
);

/**
 * @brief Calculate distance that ds should return if closest wall existed, given
 * position of the DS.
 * If the DS is inside possible wall - return failure
 *
 * @param[in] sensor_pos    global position of the sensor
 * @param[out] wall         wall that ds is pointing
 * @param[out] distance     distance that the sensor should return
 *
 * @returns RET_SUCCESS for success
 *
 * @see dsWhichWallSensorPointsWithDistance
 */
ret_code_t dsWhichClosestWallSensorShouldPoint(
    pos_t sensor_pos,
    lab_specific_wall_t* wall,
    double *distance
);


/**
 * @brief Calculate possible position of one sensor given:
 * - its prior (predicted) position
 * - measurement
 * - wall coords
 *
 * @param[in] sensor_pos  prior (predicted) sensor position
 * @param[out] sensor_pos_updated evaluated position of the sensor
 * @param[in] distance measurement from the sensor in millimeters
 * @param[in] wall_x coordinate of the wall in millimeters
 *
 * @return ret_code_t status of the operation
*/
ret_code_t dsEstimatePositionOfOneSensor(
    pos_t sensor_pos,
    pos_t *sensor_pos_updated,
    double distance,
    double wall_x);

ret_code_t dsEstimatePositionOfOneSensorY(
    pos_t sensor_pos,
    pos_t *sensor_pos_updated,
    double distance,
    double wall_y);

/**
 * @brief Return distance from the sensor to the closes wall
 * no matter if wall exists
 * @note Takes the surface which is closer to sensor, not the middle of the wall
 *
 * @param sensor_pos position of the sensor
 *
 * @return double expected distance
 */
double dsDistanceExpectedToClosestWall(pos_t sensor_pos);