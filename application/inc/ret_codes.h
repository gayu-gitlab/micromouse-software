#ifndef RET_CODES_H__
#define RET_CODES_H__


#include "logger.h"

#include <stdint.h>

#define CHECK_ERR(err) do{ if(err){ \
  log_error("ERROR no. %d in %s: %d\n", err, __FILE__, __LINE__); return err; }} while(0);

#define RET_SUCCESS                   0x00
#define RET_ERR_OTHER                 0x01
#define RET_ERR_OUT_OF_BOUNDS         0x02
#define RET_ERR_INVALID_PARAM         0x03
#define RET_ERR_NOT_IMPLEMENTED       0x04
#define RET_ERR_NOT_INITED            0x05
#define RET_ERR_TIMEOUT               0x06
#define RET_ERR_BAD_CONNECTION        0x07
#define RET_ERR_SERVER_RESPONSE       0x08
#define RET_ERR_NO_MEM                0x09
#define RET_ERR_NOT_FOUND             0x0a
#define RET_AGAIN                     0x0b
#define RET_MAPPING_NEEDED            0x0c


typedef uint8_t ret_code_t;











#endif // RET_CODES_H__
