
#ifndef VECTOR_H__
#define VECTOR_H__

#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>

#include "ret_codes.h"


/**
 * @brief This module provides API to handle structs in vector.
 * Something like vector in C++. But it has also additional features.
 * User must provide buffer for storing items (it can be static or dynamic).
 * Also, user must define size of the items which will be stored.
 *
 */

struct __attribute__((packed, aligned(4))) vector{
  // pointer to allocated memory
  uint8_t *data;
  // current number of items
  unsigned int itemCount;
  // total available number of items
  unsigned int itemTotalAvailable;
  // size of stored items in bytes
  unsigned int itemSize;
  // magic number - checks if vector is inited
  char magic[4];
};
typedef struct vector vector_t;


/*
 * API
 */

/*
 * Initialize vector.
 * You must allocate memory for items (or use static) and provide it as
 * mem pointer and memBytes (in bytes - not in items!).
 * Also itemBytes must be specified.
 *
 * Function will fit there as many items as possible.
 */
ret_code_t vector_init(vector_t* vect, void* mem, uint32_t memBytes, uint32_t itemBytes);

/*
 * Add item to vector
 */
ret_code_t vector_add(vector_t* vect, void* item);

/*
 * Reverse the vector
 */
ret_code_t vector_turnover(vector_t* vect);

/*
 * Delete item under idx index
 */
ret_code_t vector_del_idx(vector_t* vect, uint32_t idx);

/*
 * Delete all items
 */
ret_code_t vector_clear(vector_t* vect);

/*
 * Get item at point
 * Loads to @item item stored under idx in vector_t @vect;
 */
ret_code_t vector_at(vector_t vect, uint32_t idx, void* item);

/*
 * Check idx of the item in vector
 */
ret_code_t vector_get_item_idx(vector_t vect, uint32_t* idx, void* item);

/*
 * Get current number of item in vector
 */
uint32_t vector_item_cnt(vector_t vect);

/**
 * @brief Check if vectors are equal
 *
 * @param a     vector to compare
 * @param b     vector to compare
 *
 * @return true if vectors are equal
 */
bool vector_are_equal(vector_t a, vector_t b);

#endif  //VECTOR_H__
