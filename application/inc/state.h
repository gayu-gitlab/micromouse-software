#ifndef __STATE_H__
#define __STATE_H__


#include "config.h"
#include "motor.h"
#include "labirynth.h"
#include "point.h"
#include "position.h"


typedef struct state{
  double x;         // position x [m]
  double y;         // position y [m]
  double t;         // angle t [rad] (between OX and robot's heading, CCW)
  //example: t = 0 means robot is driving from left to right (West->East)
  //example: t = PI/2 means robot is driving from bottom to top (South->North)
  double v;         // linear velocity v [m/s]
  double w;         // circular velocity w [rad/s]
} state_t;

/**
 * Get point from state
 */
point_t stateGetPoint(state_t state);

/**
 * Get position from state
 */
pos_t stateGetPosition(state_t state);

void stateInitModule(const config_t *cfg);


/**
 * @brief Make one step
 *
 * @param time in seconds
 */
ret_code_t stateStep(double time);

ret_code_t stateNextModel(
    double time,
    state_t st_now,
    state_t *st_next);

ret_code_t stateNextEncoders(
    double time,
    state_t now,
    state_t *next,
    motors_ticks_t ticks);

/**
 * @brief Estimate position of the robot given sensor measurements
 *
 * Function gets as inputs:
 * - estimation of where it is
 * - what is the map
 * - what are the sensor readings
 * and gives position as output
 *
 * @param[in]  prev          previous state of the robot
 * @param[out] next          estimated state of the robot
 * @param[in]  measurements  measurements from sensors
 */
ret_code_t stateNextDistSensors(
    const state_t prev,
    state_t *next,
    double *measurements);

/**
 *
 * @brief TODO
 */
ret_code_t stateDetectWalls(
    const state_t current_state,
    double *measurements);

/**
 * @brief initialize state to starting point
 */
ret_code_t stateInitialize(state_t *st);
state_t stateStartingPoint(void);

void stateDump(char* desc, state_t s);

/**
 * @brief State getter
 */
state_t stateGetCurrentState(void);





#endif // __STATE_H__
