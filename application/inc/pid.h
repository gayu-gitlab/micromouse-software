
#pragma once

#include "ret_codes.h"

#define PID_CONTROLLER_STRUCT_SIZE_BYTES 64

struct pid_controller;
typedef struct pid_controller* pid_controller_t;

ret_code_t pidInit(pid_controller_t pid, double P, double I, double D);
ret_code_t pidStep(pid_controller_t pid, double time, double error, double* output);



