
#pragma once

#include "labirynth.h"

typedef enum
{
  LABIRYNTHS_A,
  LABIRYNTHS_ALL_WALLS,
  LABIRYNTHS_TEST_STRAIGHT_NS,
  LABIRYNTHS_TEST_STRAIGHT_NS_3,
  LABIRYNTHS_TEST_STRAIGHT_EW,
  // WARNING
  // adding here requires to add entry in labirynthNameEnumToString function
  LABIRYNTHS_NUMBER,
} LABIRYNTHS_NAME;

extern const labirynth_t labirynths[LABIRYNTHS_NUMBER];
const char * const labirynthNameEnumToString(LABIRYNTHS_NAME);


