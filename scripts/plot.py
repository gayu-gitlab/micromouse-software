#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt

plt.figure(figsize=(10, 10), dpi=80)

mousePos = [
[0.0960000, 0.0960000],
        ]
sensorPos = [
[0.0560000, 0.0960000],
[0.0613590, 0.0760000],
[0.0660229, 0.0695166],
[0.0718819, 0.0640889],
[0.1160000, 0.0613590],
[0.1306410, 0.1160000],
[0.0760000, 0.1306410],
        ]

dist = [np.sqrt((row[0] - mousePos[0][0])**2 + (row[1] - mousePos[0][1])**2) for row in sensorPos]
print(dist)


plt.scatter([row[0] for row in mousePos], [row[1] for row in mousePos], c='b', marker='o')
plt.scatter([row[0] for row in sensorPos], [row[1] for row in sensorPos], c='r', marker='x')
plt.xlim([0,0.18])
plt.ylim([0,0.18])
plt.show()
