#!/bin/bash
SCRIPT_PATH=`dirname "$(readlink -f "$0")"`

# return on first failure
set -e

# these variables are loaded by cmake
# and later are build into binaries (during compiling)
# SERVER_PORT is also used in runtime in simulator
# between 55000 and 59999
# export SERVER_PORT=$(( $RANDOM%5000 + 55000 ))
# export SERVER_IP="\"127.0.0.1\""

# before making tests, run simulator
# otherwise tests which depend on simulator, will fail
# ${SCRIPT_PATH}/micromouse-gui-simulator/build_scripts/clean_build_ci.sh
# run simulator (it is deteached). Specify TCP port
# ${SCRIPT_PATH}/micromouse-gui-simulator/build_scripts/run_headless_ci.sh --port=${SERVER_PORT}

# build & test this app
# export DOCKER_OTHER_COMMANDS="--env SERVER_PORT --env SERVER_IP"
${SCRIPT_PATH}/builder/docker.sh  ./build_all.sh $@

# stop simulator
# ${SCRIPT_PATH}/micromouse-gui-simulator/build_scripts/stop_headless_ci.sh

echo DONE with SUCCESS
