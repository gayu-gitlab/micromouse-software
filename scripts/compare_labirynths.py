#!/usr/bin/python3

import os
import glob

path = "../micromouse-gui-simulator/mazefiles/classic"


# this is the first row (one above the start point)
Row1= "|   |   |   |   |   |       |   |   |       |   |   |       |   |"

for root, dirs, files in os.walk(path, topdown=False):
    for filename in files:
        with open(os.path.join(path, filename), 'r') as f:
            l = f.readlines()
            # print(l[29])
            if(Row1 in l[29]):
                print(os.path.join(root, filename))


