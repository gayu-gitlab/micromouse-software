#!/bin/bash

cmake -DCMAKE_BUILD_TYPE=Debug .. && make -j $@ && ctest --output-on-failure -R $@

