
# Some assumptions that allows to makes shortcuts
## Main loop time interval

Other naming: Time interval, step time.

Let's assume we have around 500 encoder tics per one revolution. \
Wheel diameter = 2*pi*radius = 10.5cm = 0.105m. \
This gives one tick per 0,21mm.

Let's assume that typical velocity of the robot will be around 0.3m/s.
If main loop works 100 times per second (must execute in less than 0.01s),
then it will execute every 0.3/100 meters, meaning every 3mm.
So one main loop spin will have around 3mm/0.21mm=14 encoder ticks. That's sounds reasonable.


