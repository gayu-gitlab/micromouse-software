#!/bin/bash

# exit when error occurs
set -ex

SCRIPT_PATH=`dirname "$(readlink -f "$0")"`

BUILD_DIR="${SCRIPT_PATH}/build_host"
BUILD_DIR_TARGET="${SCRIPT_PATH}/build_target"
BIN_DIR="${SCRIPT_PATH}/bin"

rm -rf ${BIN_DIR} ${BUILD_DIR}

mkdir ${BUILD_DIR} && cd ${BUILD_DIR}
cmake ..
cmake --build . -j
# allow everyone to RWX build directories
chmod 777 ${BIN_DIR} ${BUILD_DIR}

# run only tests that do not require any HW
ctest --output-on-failure --schedule-random --tests-regex app


# do not remove bin, only build
rm -rf ${BUILD_DIR_TARGET}

mkdir ${BUILD_DIR_TARGET} && cd ${BUILD_DIR_TARGET}
cmake -DCMAKE_TOOLCHAIN_FILE=../platform/ukmarsbot-NUCLEO-G431KB/toolchain.cmake ..
cmake --build . -j
# allow everyone to RWX build directories
chmod 777 ${BIN_DIR} ${BUILD_DIR_TARGET}


echo build_all DONE!
