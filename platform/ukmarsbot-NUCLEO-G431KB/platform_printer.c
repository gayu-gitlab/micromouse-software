
#include "platform_printer.h"

ret_code_t printPathPlannerPoints(vector_t points)
{
  (void) points;
  return RET_ERR_NOT_IMPLEMENTED;
}

ret_code_t printPathPlannerCells(vector_t lab_cells)
{
  (void) lab_cells;
  // nothing to do
  return RET_ERR_NOT_IMPLEMENTED;
}
