

#include "platform_state.h"



ret_code_t statePlatformSet(state_t st)
{
  stateDump("should be set to", st);
  return RET_SUCCESS;
}

ret_code_t statePlatformGet(state_t *st)
{
  (void) st;
  return RET_ERR_NOT_IMPLEMENTED;
}

ret_code_t statePlatformNotify(state_t st)
{
  stateDump("", st);
  return RET_SUCCESS;
}

