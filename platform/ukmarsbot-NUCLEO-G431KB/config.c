

#include <math.h>
#include <stddef.h>
#include <stdbool.h>

#include "config.h"

// #define RADIUS 0.01584
#define RADIUS 17.77

static const config_t cfg =
{
  .whlTrack        = 80,
  .whlTicksPerTurn = 600,

  .whlRadius       = RADIUS,
  .whlCirc         = RADIUS * 2 * M_PI,

  .linearPIDp = 0.0022,
  .linearPIDi = 0.04,
  .linearPIDd = 0.000008,

  .angularPIDp = 0.10,
  .angularPIDi = 1.1,
  .angularPIDd = 0.000,
};


const config_t* configGet(void)
{
  return &cfg;
}
