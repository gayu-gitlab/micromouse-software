/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define DISTANCE_SENSORS_NUMBER 3

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
extern TIM_HandleTypeDef htim1;
extern UART_HandleTypeDef huart1;

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MOTOR_L_DIR_Pin GPIO_PIN_0
#define MOTOR_L_DIR_GPIO_Port GPIOF
#define MOTOR_R_DIR_Pin GPIO_PIN_1
#define MOTOR_R_DIR_GPIO_Port GPIOF
#define DS_RIGHT_Pin GPIO_PIN_0
#define DS_RIGHT_GPIO_Port GPIOA
#define DS_MID_Pin GPIO_PIN_1
#define DS_MID_GPIO_Port GPIOA
#define BATTERY_VOLTS_Pin GPIO_PIN_2
#define BATTERY_VOLTS_GPIO_Port GPIOA
#define DS_LEFT_Pin GPIO_PIN_4
#define DS_LEFT_GPIO_Port GPIOA
#define ENCODER_R_CLK_Pin GPIO_PIN_0
#define ENCODER_R_CLK_GPIO_Port GPIOB
#define ENCODER_R_CLK_EXTI_IRQn EXTI0_IRQn
#define MOTOR_L_PWM_Pin GPIO_PIN_8
#define MOTOR_L_PWM_GPIO_Port GPIOA
#define MOTOR_R_PWM_Pin GPIO_PIN_11
#define MOTOR_R_PWM_GPIO_Port GPIOA
#define ENCODER_L_CLK_Pin GPIO_PIN_12
#define ENCODER_L_CLK_GPIO_Port GPIOA
#define ENCODER_L_CLK_EXTI_IRQn EXTI15_10_IRQn
#define T_SWDIO_Pin GPIO_PIN_13
#define T_SWDIO_GPIO_Port GPIOA
#define T_SWCLK_Pin GPIO_PIN_14
#define T_SWCLK_GPIO_Port GPIOA
#define ENCODER_R_B_Pin GPIO_PIN_15
#define ENCODER_R_B_GPIO_Port GPIOA
#define LED_SENSOR_DIST_Pin GPIO_PIN_4
#define LED_SENSOR_DIST_GPIO_Port GPIOB
#define LED_FRONT_1_Pin GPIO_PIN_5
#define LED_FRONT_1_GPIO_Port GPIOB
#define LED_FRONT_2_Pin GPIO_PIN_6
#define LED_FRONT_2_GPIO_Port GPIOB
#define ENCODER_L_B_Pin GPIO_PIN_7
#define ENCODER_L_B_GPIO_Port GPIOB
#define LED_NUCLEO_Pin GPIO_PIN_8
#define LED_NUCLEO_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
