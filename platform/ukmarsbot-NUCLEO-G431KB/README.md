

# Getting started
- install STM32 Cube Programmer
- cmake .. -DCMAKE_TOOLCHAIN_FILE=../platform/ukmarsbot-NUCLEO-G431KB/toolchain.cmake
- make -j
- STM32_Programmer_CLI --connect port=swd --write ../bin/mm.elf

# Notes that might be useful in the future
## PINs
I do not know why, but ADC1_IN4 did not work for me. So the wirings from sensors to main PCB are switched (A2<->A3) and the sensor readings are done on PA0, PA1 and PA4.
Probably it was because of wrong SB configuration
SB1 is OFF

## HW workarounds
changed R7 from 15k to 43k.


## Timers
### Timer1 motor PWM
Timer1 for motor control
170Mhz on entry (from APB2)
remember that prescaler and ARR must be set to "x-1"
prescaler 170 -> 1MHz
ARR = 100 -> 10kHz
0 <= CCR < 100

### Timer2 - timings
170Mhz / 170 (prescaler) / 1 (ARR) = 1Mhz.
It is 32 bit counter, so it will roll over in 4 294,967295 seconds (71minutes)

## BT troubleshooting
https://alberand.com/hc-05-linux.html
```
BT connection code: 1234
$ sudo rfcomm bind rfcomm0 FC:A8:9A:00:3C:F6
```

### prolific windows drivers

https://www.mediafire.com/file/982x6iyk89v95dp/Prolific_PL2303_driver_v3.3.2.102_%25282008-24-09%2529_Win8_x64_x86.7z/file


https://answers.microsoft.com/en-us/windows/forum/all/pl2303hxa-phased-out-since-2012-version-38310/2ef1d8fa-59fc-421e-9510-b1f63c68d4f9


### HC-05 settings:
- both: VERSION:3.0-20170601
- both: +UART:115200,0,0
- +ADDR:FCA8:9A:3CF6 +NAME:HC-05-SKIN
- +ADDR:FCA8:9A:1D5D +NAME:HC-05-NAKED
Important. To enter AT mode: push button, then connect to power, then you can release button.
Baudrate in ATmode: 38400. Remember to send \r\n after each AT command! In windows Putty it is ctr+j & ctrl+m.


# Robot driving notes
error on encoders:
in straight line: ~1% (3s)
in angle: ~7% (2s)


