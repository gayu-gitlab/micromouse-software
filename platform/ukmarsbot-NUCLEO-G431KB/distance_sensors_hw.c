
#include "stm32g4xx_hal.h"

#include "ret_codes.h"
#include "main.h"
#include "platform_base.h"
#include "math.h"
#include "distance_sensors.h"

static volatile bool ConversionDone = false;

static void TurnIrOn(void)
{
  HAL_GPIO_WritePin(LED_SENSOR_DIST_GPIO_Port, LED_SENSOR_DIST_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(LED_NUCLEO_GPIO_Port, LED_NUCLEO_Pin, GPIO_PIN_SET);
}

static void TurnIrOff(void)
{
  HAL_GPIO_WritePin(LED_SENSOR_DIST_GPIO_Port, LED_SENSOR_DIST_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED_NUCLEO_GPIO_Port, LED_NUCLEO_Pin, GPIO_PIN_RESET);
}

static ret_code_t getSensorReadings(uint16_t buf[], uint8_t numberOfSamples)
{
  HAL_StatusTypeDef hal_status;

  hal_status = HAL_ADC_Start_DMA(&hadc2, (uint32_t*) buf, numberOfSamples);
  if(hal_status != HAL_OK)
  {
    log_warn("DS: start problem = %3d ", hal_status);
    return RET_ERR_OTHER;
  }

  while(ConversionDone == false)
  {
    // This implementation is quite stupid, but it is "good enough" and works
  }
  ConversionDone = 0;

  return RET_SUCCESS;
}

ret_code_t dsGetDistances(double *dists, int len)
{
  ret_code_t ret = RET_SUCCESS;
  uint16_t ReadingsBackground[DISTANCE_SENSORS_NUMBER];
  uint16_t ReadingsWithIR[DISTANCE_SENSORS_NUMBER];
  uint16_t ReadingsDiff[DISTANCE_SENSORS_NUMBER];
  // const double linear_coefficiences[DISTANCE_SENSORS_NUMBER][2] =
  //     {
  //         {-22.26, 312.95},
  //         {-45.00, 556.75},
  //         {-23.50, 342.50},
  //     };
  const double exp_coefficiences[DISTANCE_SENSORS_NUMBER][2] =
      {
          {411, -0.143},
          {786, -0.179},
          {436, -0.133},
      };

  if(len < DISTANCE_SENSORS_NUMBER)
  {
    ret = RET_ERR_NO_MEM;
  }
  if(dists == NULL)
  {
    ret = RET_ERR_INVALID_PARAM;
  }

  if(ret == RET_SUCCESS)
  {
    ret = getSensorReadings(ReadingsBackground, DISTANCE_SENSORS_NUMBER);
  }

  if(ret == RET_SUCCESS)
  {
    TurnIrOn();
    ret = getSensorReadings(ReadingsWithIR, DISTANCE_SENSORS_NUMBER);
    TurnIrOff();
  }

  if(ret == RET_SUCCESS)
  {
    for(uint8_t i = 0; i<DISTANCE_SENSORS_NUMBER; i++)
    {
      ReadingsDiff[i] = ReadingsWithIR[i] - ReadingsBackground[i];
      // dists[i] = (ReadingsDiff[i] - linear_coefficiences[i][1])/linear_coefficiences[i][0];
      dists[i] = log((double)ReadingsDiff[i]/exp_coefficiences[i][0])/exp_coefficiences[i][1];
      log_debug("DS: %d w: %5d b: %5d d: %5d. Dist=%4.1f",
                i,
                ReadingsWithIR[i],
                ReadingsBackground[i],
                ReadingsDiff[i],
                dists[i]);
    }
  }
  log_debug("DS: L=%3.1fcm  M=%3.1fcm  R=%3.1fcm",
            dists[0],
            dists[1],
            dists[2]
            );
  // log_debug("------%d", ret);
  return ret;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
  (void)hadc;
  ConversionDone = true;
}


dsConfig_t dsGetCfgPlatformDefault(void)
{
  static dsConfig_t defaultCfg =
  {
    .numberOfSensors = 3,
    .sensors =
    {
      {.offset = {-38, 50, +M_PI/3}},
      {.offset = {     0, 62, 0}},
      {.offset = { 38, 50, -M_PI/3}},
    },
  };

  return defaultCfg;
}