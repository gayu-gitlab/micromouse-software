set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1.0)

set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER arm-none-eabi-gcc)

set(PLATFORM_NAME "ukmarsbot-NUCLEO-G431KB")
set(CPU cortex-m4)
set(FPU fpv4-sp-d16)

set(LDSCRIPT ${CMAKE_CURRENT_LIST_DIR}/STM32G431KBTx_FLASH.ld)
add_link_options(-T ${LDSCRIPT})
add_link_options(-Wl,--print-memory-usage)
add_link_options(-Wl,--gc-sections)         # garbage collector: get rid of unused code on target


set(CMAKE_C_FLAGS_INIT "   -mcpu=${CPU} \
    -mfpu=${FPU} \
    -mfloat-abi=hard \
    -mthumb \
    --specs=nosys.specs \
    -fdata-sections -ffunction-sections \
    "
    )

set(CMAKE_CXX_FLAGS_INIT
    --specs=nosys.specs
    )


add_definitions(
    -DUSE_HAL_DRIVER
    -DSTM32G431xx
    -DUNITY_OUTPUT_CHAR=platformPutChar
    -DUNITY_OUTPUT_CHAR_HEADER_DECLARATION=platformPutChar\(char\)
    -DUNITY_OUTPUT_FLUSH=platformLoggerFlush
    -DUNITY_OUTPUT_FLUSH_HEADER_DECLARATION=platformLoggerFlush\(void\)
    )

set(CMAKE_EXECUTABLE_SUFFIX_C   .elf)
set(CMAKE_EXECUTABLE_SUFFIX_CXX .elf)
set(CMAKE_EXECUTABLE_SUFFIX_ASM .elf)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)