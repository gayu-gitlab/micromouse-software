#ifndef PLATFORM_H__
#define PLATFORM_H__

#include <stdint.h>
#include <stdbool.h>

/*
 * Initialize platform
 */
void platformInit(void);

/*
 * Delay milliseconds
 */
void platformDelayMs(uint32_t ms);

/*
 * On this function relies all logger module
 */
void platformPutChar(const char c);

/*
 * Turn on or off specific led
 * This API tries to be platform independent, so idexes are specified by platform
*/
void platformSetLedState(uint8_t led_idx, bool on);

/**
 * @brief Check if battery is empty.
 *
 * Return true if the battery is empty
 */
bool platformCheckIfBatteryIsEmpty(void);

/**
 * @brief Show result of the test
 */
void platformShowTestResult(int failures);

/**
 * @brief Start timer in order to measure elapsed time
 */
void platformStartTimingMeasurement(void);

/**
 * @brief Stop timer and return elapsed microseconds
 */
uint32_t platformStopTimingMeasurementUs(void);

#endif // PLATFORM_H__