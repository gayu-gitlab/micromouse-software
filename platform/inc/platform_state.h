#pragma once


#include "state.h"

/*
 * Platform-specific setting the state.
 *
 * For example:
 * [simulator]: set simulator to that state
 * [real life]: print on console where should the mouse be
 * placed and wait for user pressing to "ready" button etc...
 */
ret_code_t statePlatformSet(state_t st);

/*
 * This works only with simulator.
 */
ret_code_t statePlatformGet(state_t *st);



/*
 * Platform-specific notifying the state
 *
 * For example:
 * [simulator]: notify simulator what is the current state
 * [real life]: print on console where the mouse is
 */
ret_code_t statePlatformNotify(state_t st);
