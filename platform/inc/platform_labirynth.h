#pragma once


#include "ret_codes.h"
#include "labirynths.h"

/*
 * Platform-specific setting the labirynth
 *
 * For example:
 * [simulator]: load labirynth from file
 * [real life]: setup the labirynth manually
 */
ret_code_t labirynthSetup(LABIRYNTHS_NAME name);
