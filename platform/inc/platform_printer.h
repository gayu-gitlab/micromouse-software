#pragma once

#include "ret_codes.h"
#include "vector.h"

/**
 * @brief Show the path
 * depends on the system / bsp
 *
 * @param[in] points vector of point_t
 */
ret_code_t printPathPlannerPoints(vector_t points);

/**
 * @brief Show the path
 * @param[in] lab_cells vector of lab_cellxy_t
 */
ret_code_t printPathPlannerCells(vector_t lab_cells);
