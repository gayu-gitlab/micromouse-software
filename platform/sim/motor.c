



#include "simulator_broker.h"
#include "motor.h"


double normalizeSpeed(double speed)
{
  if(speed > MAX_MOTOR_SPEED)
  {
    log_warn("MOTOR: wrong speed! %f > %f", speed, MAX_MOTOR_SPEED);
    speed = MAX_MOTOR_SPEED;
  }
  if(speed < -MAX_MOTOR_SPEED)
  {
    log_warn("MOTOR: wrong speed! %f < %f", speed, -MAX_MOTOR_SPEED);
    speed = -MAX_MOTOR_SPEED;
  }
  return speed;
}

ret_code_t motorSetSpeed(motors_speed_t values)
{
  values.left = normalizeSpeed(values.left);
  values.right = normalizeSpeed(values.right);

  return sim_set_param(SIM_PARAM_MOTOR_SPEED, &values);
}

ret_code_t motorGetTicks(motors_ticks_t *ticks)
{
  return sim_get_param(SIM_PARAM_MOTOR_TICKS, ticks);
}
