

#include <math.h>
#include <stddef.h>
#include <stdbool.h>

#include "config.h"

#define RADIUS 10

static const config_t cfg =
{
  .whlTrack        = 100,
  .whlTicksPerTurn = 1000,

  .whlRadius       = RADIUS,
  .whlCirc         = RADIUS * 2 * M_PI,

  .linearPIDp = 0.0022,
  .linearPIDi = 0.0400,
  .linearPIDd = 0.000008,

  .angularPIDp = 0.10,
  .angularPIDi = 1.1,
  .angularPIDd = 0.000,
};


const config_t* configGet(void)
{
  return &cfg;
}
