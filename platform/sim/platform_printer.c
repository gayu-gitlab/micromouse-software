
#include "simulator_broker.h"
#include "platform_printer.h"
#include "vector.h"
#include "point.h"
#include "labirynth.h"

ret_code_t printPathPlannerPoints(vector_t points)
{
  return sim_set_param(SIM_PARAM_PATH, (void*) &points);
}

ret_code_t printPathPlannerCells(vector_t lab_cells)
{
  vector_t points;
  char pointsMem[5000];
  point_t point;
  lab_cellxy_t tmp;

  CHECK_ERR(vector_init(&points, pointsMem, sizeof(pointsMem), sizeof(point_t)));

  for(uint32_t i=0; i < vector_item_cnt(lab_cells); i++)
  {
    CHECK_ERR(vector_at(lab_cells, i, &tmp));
    CHECK_ERR(labGetMidCell(tmp, &point));
    CHECK_ERR(vector_add(&points, &point));
  }
  return printPathPlannerPoints(points);
}
