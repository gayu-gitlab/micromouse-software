
#include <math.h>
#include <stdbool.h>

#include "distance_sensors_hw.h"
#include "useful.h"
#include "simulator_broker.h"
#include "ret_codes.h"


ret_code_t dsGetDistances(double* dists, int len)
{
  dsConfig_t *cfg = dsGetCfg();
  if(len < cfg->numberOfSensors)
  {
    return RET_ERR_NO_MEM;
  }
  return sim_get_param(SIM_PARAM_DIST_SENSORS, (void*) dists);
}

dsConfig_t dsGetCfgPlatformDefault(void)
{
  dsConfig_t defaultCfg =
  {
    .numberOfSensors = 3,
    .sensors =
    {
      {.offset = {-38, 50, +M_PI/3}},
      {.offset = {     0, 62, 0}},
      {.offset = { 38, 50, -M_PI/3}},
    },
  };

  return defaultCfg;
}