


#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>

#include "platform_base.h"
#include "platform_logger.h"
#include "simulator_broker.h"
#include "config.h"
#include "distance_sensors.h"
#include "distance_sensors_hw.h"

static uint64_t simulated_time_ms = 0;
static uint64_t timing_start;

void platformInit(void)
{
  simulated_time_ms = 0;
  platformLoggerInit();

  sim_set_param(SIM_PARAM_CONFIG, configGet());
  dsInit(dsGetCfgPlatformDefault());
  sim_set_param(SIM_PARAM_CONFIG_DISTANCE_SENSORS, dsGetCfg());
}

void platformDelayMs(uint32_t ms)
{
  simulated_time_ms += (uint64_t)ms;
  double time = ms;
  sim_set_param(SIM_PARAM_EXECUTE, &time);
  // usleep(ms*1000);
}

void platformSetLedState(uint8_t led_idx, bool on)
{
  log_debug("LED %d set to %d", (int)led_idx, (int)on);
}

bool platformCheckIfBatteryIsEmpty(void)
{
  return false;
}

void platformShowTestResult(int failures)
{
  //nothing to do
  (void)failures;
}

void platformStartTimingMeasurement(void)
{
  timing_start = simulated_time_ms;
}

uint32_t platformStopTimingMeasurementUs(void)
{
  uint32_t elapsed_ms = simulated_time_ms - timing_start;

  // log_debug("Time taken %d seconds %d milliseconds", elapsed_ms/1000, elapsed_ms%1000);
  return elapsed_ms*1000;
}