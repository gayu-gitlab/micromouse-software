#include <stdio.h>

#include "platform_base.h"
#include "logger.h"


void platformLoggerInit(void)
{
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
  log_raw("\n\n\n\n\n****************\n");
}

void platformPutChar(const char c)
{
  printf("%c", c);
}

void platformLoggerFlush(void)
{
}
