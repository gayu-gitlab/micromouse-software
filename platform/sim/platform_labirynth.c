

#include "ret_codes.h"
#include "labirynths.h"
#include "platform_labirynth.h"
#include "simulator_broker.h"

ret_code_t labirynthSetup(LABIRYNTHS_NAME name)
{
  sim_set_param(SIM_PARAM_MAP_NAME, (void *)labirynthNameEnumToString(name));
  sim_show_current_lab_knowledge();
  return RET_SUCCESS;
}
