
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>


#include "simulator_broker.h"
#include "ret_codes.h"
#include "json.h"
#include "path_planner.h"
#include "motor.h"
#include "state.h"
#include "labirynth.h"
#include "config.h"
#include "distance_sensors.h"



#define BUF_MAX_LEN 2000

#ifndef SERVER_IP
#define SERVER_IP "127.0.0.1"
#endif

#ifndef SERVER_PORT
#define SERVER_PORT 57233
#endif

#define MOD_NAME "SIM: "

static void sig_handler(int signo);
static const char * sim_param2string(sim_param_t param);
static ret_code_t receive_json(json_object ** json_recv, size_t * len);
static ret_code_t sim_parse_got_params(json_object *recv, sim_param_t param, void* dest);


static bool inited = false;
static int sockfd;
static struct json_tokener * tok;
static char recv_buf[BUF_MAX_LEN];

ret_code_t sim_get_param(sim_param_t param, void* dest)
{
  json_object *jsend, *jrecv, *jrecvpars;
  size_t jlen;
  ret_code_t ret;
  const char *msg;

  if(!inited) { sim_init(); }
  if(!inited) {
    log_error(MOD_NAME"cannot init");
    return RET_ERR_NOT_INITED;
  }

  jsend = json_object_new_object();

  json_object_object_add(jsend, "type", json_object_new_string("get"));
  json_object_object_add(jsend, "param", json_object_new_string(sim_param2string(param)));

  msg = json_object_to_json_string_length(jsend, 0, &jlen);
  // log_debug(MOD_NAME"send %zuB: %s", jlen, msg);

  ssize_t size = write(sockfd, msg, jlen);
  (void) size;

  //**************************************************************
  ret = receive_json(&jrecv, &jlen);
  if(ret)
  {
    sim_deinit();
    return ret;
  }

  if(!json_object_equal(
        json_object_object_get(jrecv, "type"),
        json_object_new_string("resp_get"))
    )
  {
    log_error(MOD_NAME"Server returned invalid type");
    return RET_ERR_SERVER_RESPONSE;
  }

  jrecvpars = json_object_object_get(jrecv, "value");
  CHECK_ERR(sim_parse_got_params(jrecvpars, param, dest));

  return RET_SUCCESS;
}


ret_code_t sim_parse_got_params(json_object *recv, sim_param_t param, void* dest)
{
  if(!recv) { return RET_ERR_INVALID_PARAM; }
  if(!dest) { return RET_ERR_INVALID_PARAM; }

  json_object *tmp;
  switch(param)
  {
    case SIM_PARAM_ALIVE:
      * (bool*) dest = json_object_get_boolean(recv);
      break;

    case SIM_PARAM_MOTOR_TICKS:
      tmp = json_object_array_get_idx(recv, 0);
      ((motors_ticks_t*) dest)->left = json_object_get_int(tmp);
      tmp = json_object_array_get_idx(recv, 1);
      ((motors_ticks_t*) dest)->right = json_object_get_int(tmp);
      break;

    case SIM_PARAM_STATE:
      ((state_t*) dest)->x = json_object_get_double(json_object_object_get(recv, "x"));
      ((state_t*) dest)->y = json_object_get_double(json_object_object_get(recv, "y"));
      ((state_t*) dest)->t = json_object_get_double(json_object_object_get(recv, "t"));
      ((state_t*) dest)->v = json_object_get_double(json_object_object_get(recv, "v"));
      ((state_t*) dest)->w = json_object_get_double(json_object_object_get(recv, "w"));
      break;

    case SIM_PARAM_CONFIG:
      ((config_t*) dest)->whlRadius = json_object_get_double(json_object_object_get(recv, "radius"));
      ((config_t*) dest)->whlTicksPerTurn = json_object_get_double(json_object_object_get(recv, "ticksPerTurn"));
      ((config_t*) dest)->whlTrack = json_object_get_double(json_object_object_get(recv, "track"));
      ((config_t*) dest)->whlCirc = ((config_t*) dest)->whlRadius * 2 * M_PI;
      break;

    case SIM_PARAM_CONFIG_DISTANCE_SENSORS:
      if((json_object_object_get(recv, "distanceSensors") == NULL) ||
      (json_object_is_type(json_object_object_get(recv, "distanceSensors"), json_type_array) == 0))
      {
        log_error("Cannot get distance sensors config");
        return RET_ERR_SERVER_RESPONSE;
      }
      int number = json_object_array_length(json_object_object_get(recv, "distanceSensors"));
      if(number > MAX_DISTANCE_SENSORS_NUM)
      {
        number = MAX_DISTANCE_SENSORS_NUM;
      }
      ((dsConfig_t *)dest)->numberOfSensors = number;
      for(uint8_t i = 0; i<number; i++)
      {
        tmp = json_object_array_get_idx(json_object_object_get(recv, "distanceSensors"), i);
        ((dsConfig_t *)dest)->sensors[i].offset.x = json_object_get_double(json_object_object_get(tmp, "x"));
        ((dsConfig_t *)dest)->sensors[i].offset.y = json_object_get_double(json_object_object_get(tmp, "y"));
        ((dsConfig_t *)dest)->sensors[i].offset.t = json_object_get_double(json_object_object_get(tmp, "t"));
      }
      break;

    case SIM_PARAM_DIST_SENSORS:
      for(uint8_t i = 0; i<json_object_array_length(recv); i++)
      {
        ((double*) dest)[i] = json_object_get_double(json_object_array_get_idx(recv, i));
      }
      break;

    default:
      log_error("invalid param get");
      return RET_ERR_INVALID_PARAM;
  }

  return RET_SUCCESS;
}

static ret_code_t prepare_set_value(json_object *j, sim_param_t param, const void* value)
{
  json_object *jtmp, *jarr, *jvalue;
  ret_code_t ret;

  json_object_object_add(j, "param", json_object_new_string(sim_param2string(param)));

  switch(param)
  {
    case SIM_PARAM_MAP_NAME:
      jvalue = json_object_new_string((char*) value);
      break;

    case SIM_PARAM_CONFIG:
      if(!value){ return RET_ERR_INVALID_PARAM; }

      config_t *cfg = (config_t *)value;
      jtmp = json_object_new_object();
      json_object_object_add(jtmp, "track", json_object_new_double(cfg->whlTrack));
      json_object_object_add(jtmp, "radius", json_object_new_double(cfg->whlRadius));
      json_object_object_add(jtmp, "ticksPerTurn", json_object_new_double(cfg->whlTicksPerTurn));
      jvalue = jtmp;
      break;

    case SIM_PARAM_CONFIG_DISTANCE_SENSORS:
      if(!value){ return RET_ERR_INVALID_PARAM; }

      dsConfig_t *dsCfg = (dsConfig_t *)value;
      jtmp = json_object_new_object();
      jarr = json_object_new_array();

      for(uint8_t i = 0; i<dsCfg->numberOfSensors; i++)
      {
        json_object *cfg_json = json_object_new_object();
        json_object_object_add(cfg_json, "x", json_object_new_double(dsCfg->sensors[i].offset.x));
        json_object_object_add(cfg_json, "y", json_object_new_double(dsCfg->sensors[i].offset.y));
        json_object_object_add(cfg_json, "t", json_object_new_double(dsCfg->sensors[i].offset.t));
        json_object_object_add(cfg_json, "min", json_object_new_double(MIN_DISTANCE_SENSOR_RANGE_MM));
        json_object_object_add(cfg_json, "max", json_object_new_double(MAX_DISTANCE_SENSOR_RANGE_MM));
        json_object_array_add(jarr, cfg_json);
      }
      json_object_object_add(jtmp, "distanceSensors", jarr);
      jvalue = jtmp;
      break;

    case SIM_PARAM_PATH:
      jarr = json_object_new_array();
      if(!value){ return RET_ERR_INVALID_PARAM; }

      vector_t *path = value;
      point_t point;
      for(unsigned int i = 0; i < vector_item_cnt(*path); i++)
      {
        jtmp = json_object_new_object();
        ret = vector_at(*path, i, &point); if(ret) { return ret; }
        json_object_object_add(jtmp, "x", json_object_new_double(point.x));
        json_object_object_add(jtmp, "y", json_object_new_double(point.y));
        json_object_array_add(jarr, jtmp);
      }
      jvalue = jarr;
      break;

    case SIM_PARAM_CELL_TEXT:
      if(!value){ return RET_ERR_INVALID_PARAM; }
      pp_cell_text_t *ct = value;
      jtmp = json_object_new_object();
      json_object_object_add(jtmp, "x", json_object_new_int(ct->xy.x));
      json_object_object_add(jtmp, "y", json_object_new_int(ct->xy.x));
      json_object_object_add(jtmp, "text", json_object_new_string(ct->text));
      jvalue = jtmp;
      break;

    case SIM_PARAM_CLEAR_CELL_TEXTS:
      //value is not needed
      jvalue = json_object_new_string((char*) "");
      break;

    case SIM_PARAM_MOTOR_SPEED:
      if(!value){ return RET_ERR_INVALID_PARAM; }
      motors_speed_t *values = value;

      jtmp = json_object_new_object();
      json_object_object_add(jtmp, "left", json_object_new_double(values->left));
      json_object_object_add(jtmp, "right", json_object_new_double(values->right));
      jvalue = jtmp;
      break;

    case SIM_PARAM_EXECUTE:
      if(!value){ return RET_ERR_INVALID_PARAM; }
      jvalue = json_object_new_double(*(double*)value);
      break;

    case SIM_PARAM_STATE:
    case SIM_PARAM_STATE_NOTIFY:
      if(!value){ return RET_ERR_INVALID_PARAM; }
      jvalue = json_object_new_object();
      json_object_object_add(jvalue, "x",
          json_object_new_double(((state_t*)value)->x));
      json_object_object_add(jvalue, "y",
          json_object_new_double(((state_t*)value)->y));
      json_object_object_add(jvalue, "t",
          json_object_new_double(((state_t*)value)->t));
      json_object_object_add(jvalue, "v",
          json_object_new_double(((state_t*)value)->v));
      json_object_object_add(jvalue, "w",
          json_object_new_double(((state_t*)value)->w));
      break;

    case SIM_PARAM_NOTIFY_WALL:
      jvalue = json_object_new_object();
      if(!value){ return RET_ERR_INVALID_PARAM; }
      SIM_WALL_NOTIFY_t *notify = (SIM_WALL_NOTIFY_t*) value;
      json_object_object_add(jvalue, "x",
          json_object_new_double(notify->x));
      json_object_object_add(jvalue, "y",
          json_object_new_double(notify->y));
      json_object_object_add(jvalue, "NSEW",
          json_object_new_double(notify->mask_walls));
      json_object_object_add(jvalue, "exists",
          json_object_new_int(notify->exists));
      break;

    case SIM_PARAM_NOTIFY_WALL_CLEAR:
      jvalue = json_object_new_string((char*) "");
      break;

    default:
      log_error("not handled case %d", param);
      return RET_ERR_INVALID_PARAM;
  }

  json_object_object_add(j, "value", jvalue);
  return RET_SUCCESS;
}

ret_code_t sim_set_param(sim_param_t param, const void* value)
{
  json_object *jsend, *jrecv;
  size_t jlen;
  ret_code_t ret;
  const char *msg;

  if(!inited) { sim_init(); }
  if(!inited) {
    log_error(MOD_NAME"cannot init");
    return RET_ERR_NOT_INITED;
  }

  jsend = json_object_new_object();
  json_object_object_add(jsend, "type", json_object_new_string("set"));
  ret = prepare_set_value(jsend, param, value); if(ret) { return ret; }
  msg = json_object_to_json_string_length(jsend, 0, &jlen);
  // log_debug(MOD_NAME"send %zuB: %s", jlen, msg);
  ssize_t size = write(sockfd, msg, jlen);
  (void) size;

  //**************************************************************

  ret = receive_json(&jrecv, &jlen); if(ret) { return ret; }


  if(!json_object_equal(
        json_object_object_get(jrecv, "type"),
        json_object_new_string("resp_set")))
  {
    log_error(MOD_NAME"Server returned invalid type");
    return RET_ERR_SERVER_RESPONSE;
  }

  if(!json_object_equal(
        json_object_object_get(jrecv, "result"),
        json_object_new_string("success")))
  {
    log_warn(MOD_NAME"Server returned json error code");
    msg = json_object_to_json_string_length(json_object_object_get(jrecv, "result"), 0, &jlen);
    log_warn(MOD_NAME"received error: %zuB: %s", jlen, msg);
    return RET_ERR_SERVER_RESPONSE;
  }

  return RET_SUCCESS;
}

ret_code_t receive_json(json_object ** json_recv, size_t * len)
{
  int read_len;
  enum json_tokener_error jerr;
  unsigned int got;

  //get response
  memset(recv_buf, 0, sizeof(recv_buf));
  got = 0;
  json_tokener_reset(tok);

  do
  {
    read_len = read(sockfd, &recv_buf[got], sizeof(recv_buf) - got);
    if(read_len < 0)
    {
      log_error(MOD_NAME"bad connection %d: %s", read_len, strerror(read_len));
      return RET_ERR_BAD_CONNECTION;
    }
    else if(read_len == 0)
    {
      log_error(MOD_NAME"timeout");
      return RET_ERR_TIMEOUT;
    }
    else{
      got += read_len;
      *json_recv = json_tokener_parse_ex(tok, recv_buf, got);
    }
  } while ((jerr = json_tokener_get_error(tok)) == json_tokener_continue);

  if (jerr != json_tokener_success)
  {
    log_error(MOD_NAME"Error: %s", json_tokener_error_desc(jerr));
    return RET_ERR_SERVER_RESPONSE;
  }
  if (json_tokener_get_parse_end(tok) < got)
  {
    log_warn(MOD_NAME"warning, received more than expected");
  }
  // log_debug(MOD_NAME"received: %.*s", got, json_object_to_json_string_length(*json_recv, 0, len));

  return RET_SUCCESS;
}

void sim_init(void)
{
  struct sockaddr_in servaddr;
  struct timeval tv;
  ret_code_t ret = RET_SUCCESS;

  inited = false;
  tok = json_tokener_new();

  //register signal hander, so that we can close socket securely
  if(signal(SIGINT, sig_handler) == SIG_ERR)
  {
    log_error(MOD_NAME"can't catch SIGINT");
    ret = RET_ERR_OTHER;
  }

  if(ret == RET_SUCCESS)
  {
    // socket create and varification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
      log_error(MOD_NAME"socket creation failed");
      ret = RET_ERR_OTHER;
    }
  }

  if(ret == RET_SUCCESS)
  {
    // socket timeout
    tv.tv_sec = 10;
    tv.tv_usec = 1;
    ret = setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
  }

  if(ret == RET_SUCCESS)
  {
    // assign IP, PORT
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(SERVER_IP);
    servaddr.sin_port = htons((uint16_t)SERVER_PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) != 0)
    {
      log_error(MOD_NAME "connection with the server failed...");
      log_error(MOD_NAME "check if ip and port are properly set");
      log_error(MOD_NAME "IP: %s, port: %d", SERVER_IP, (uint16_t)SERVER_PORT);
      ret = RET_ERR_OTHER;
    }
  }

  if(ret == RET_SUCCESS)
  {
    json_c_set_serialization_double_format("%.6g", JSON_C_OPTION_GLOBAL);
    inited = true;

    // this status does not determine if module is initialized
    ret = sim_set_param(SIM_PARAM_CONFIG, (void*)configGet());
    if(ret != RET_SUCCESS)
    {
      log_error(MOD_NAME"cannot properly set config");
    }
    log_debug(MOD_NAME"initialized");
  }
  else
  {
    log_error(MOD_NAME"Cannot initialize");
  }
}



void sig_handler(int signo)
{
  if (signo == SIGINT)
  {
    log_info(MOD_NAME"received SIGINT");
    sim_deinit();
  }
  else{
    log_debug(MOD_NAME"received signal %d", signo);
  }
}

void sim_deinit(void)
{
  log_debug(MOD_NAME"sim_deinit\n\n");
  shutdown(sockfd, 2);
  json_tokener_free(tok);
}

const char * sim_param2string(sim_param_t param)
{
  int par = param;
  switch(par)
  {
    case SIM_PARAM_ALIVE: return "alive";
    case SIM_PARAM_CONFIG: return "config";
    case SIM_PARAM_CONFIG_DISTANCE_SENSORS: return "config";
    case SIM_PARAM_MAP_NAME: return "map_name";
    case SIM_PARAM_PATH: return "path_plan";
    case SIM_PARAM_CELL_TEXT: return "cell_text";
    case SIM_PARAM_CLEAR_CELL_TEXTS: return "clear_cell_texts";
    case SIM_PARAM_MOTOR_SPEED: return "motor_speed";
    case SIM_PARAM_MOTOR_TICKS: return "motor_ticks";
    case SIM_PARAM_EXECUTE: return "execute";
    case SIM_PARAM_STATE: return "state";
    case SIM_PARAM_STATE_NOTIFY: return "state_notify";
    case SIM_PARAM_DIST_SENSORS: return "dist_sensors";
    case SIM_PARAM_NOTIFY_WALL: return "notify_wall";
    case SIM_PARAM_NOTIFY_WALL_CLEAR: return "notify_wall_clear_all";
  }
  return "invalid_param";
}

ret_code_t sim_show_current_lab_knowledge(void)
{
  lab_cellxy_t cellxy;
  ret_code_t ret;
  SIM_WALL_NOTIFY_t n;

  ret = sim_set_param(SIM_PARAM_NOTIFY_WALL_CLEAR, NULL);
  if(ret != RET_SUCCESS)
  {
    return ret;
  }

  for(uint8_t x = 0; x < LAB_N; x++)
  {
    cellxy.x = x;
    for (uint8_t y = 0; y < LAB_N; y++)
    {
      cellxy.y = y;
      for (uint8_t wall = LAB_NORTH; wall <= LAB_WEST; wall <<= 1)
      {
        if (labWallsKnown(cellxy, wall))
        {
          n.x = x;
          n.y = y;
          n.mask_walls = wall;
          n.exists = false;
          if (labWallsExist(cellxy, wall))
          {
            n.exists = true;
          }

          ret = sim_set_param(SIM_PARAM_NOTIFY_WALL, (void *)&n);
          if (ret != RET_SUCCESS)
          {
            return ret;
          }
        }
      }
    }
  }

  return RET_SUCCESS;
}


