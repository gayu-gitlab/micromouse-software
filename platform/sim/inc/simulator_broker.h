/*
 * Created by: Bazyli Gielniak
 */

#ifndef SIMULATOR_BROKER_H__
#define SIMULATOR_BROKER_H__

#include "ret_codes.h"
#include "stdbool.h"

/*
 * WARNING
 * add also entry to sim_param2string
 */
enum sim_param{
    SIM_PARAM_ALIVE,           //is simulator alive?
    SIM_PARAM_CONFIG,
    SIM_PARAM_CONFIG_DISTANCE_SENSORS,
    SIM_PARAM_MAP_NAME,
    SIM_PARAM_PATH,
    SIM_PARAM_CELL_TEXT,
    SIM_PARAM_CLEAR_CELL_TEXTS,
    SIM_PARAM_MOTOR_SPEED,
    SIM_PARAM_MOTOR_TICKS,
    SIM_PARAM_EXECUTE,
    SIM_PARAM_STATE,
    SIM_PARAM_STATE_NOTIFY,
    SIM_PARAM_DIST_SENSORS,
    SIM_PARAM_NOTIFY_WALL,
    SIM_PARAM_NOTIFY_WALL_CLEAR,
    SIM_PARAM_NUMBER
};
typedef enum sim_param sim_param_t;



ret_code_t sim_get_param(sim_param_t param, void* dest);
ret_code_t sim_set_param(sim_param_t param, const void* value);

void sim_init(void);
void sim_deinit(void);

typedef struct
{
    uint8_t x;
    uint8_t y;
    uint8_t mask_walls;
    bool exists;
} SIM_WALL_NOTIFY_t;


ret_code_t sim_show_current_lab_knowledge(void);












#endif // SIMULATOR_BROKER_H__
